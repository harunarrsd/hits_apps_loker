-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 22 Sep 2019 pada 06.52
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hits_apps`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `applications`
--

CREATE TABLE `applications` (
  `id_applications` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name_surname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `nationality` varchar(20) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `place_of_birth` varchar(50) DEFAULT NULL,
  `photo` text DEFAULT NULL,
  `gender` varchar(12) DEFAULT NULL,
  `id_position` int(11) DEFAULT NULL,
  `accept_lower_rank` tinyint(1) DEFAULT NULL,
  `available_from` varchar(25) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` text DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `name_next_of_kin` varchar(50) DEFAULT NULL,
  `relationship_next_of_kin` varchar(50) DEFAULT NULL,
  `phone_next_of_kin` varchar(15) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `applications`
--

INSERT INTO `applications` (`id_applications`, `first_name`, `middle_name`, `last_name_surname`, `email`, `password`, `nationality`, `date_of_birth`, `place_of_birth`, `photo`, `gender`, `id_position`, `accept_lower_rank`, `available_from`, `address`, `city`, `phone`, `name_next_of_kin`, `relationship_next_of_kin`, `phone_next_of_kin`, `remark`, `role`, `status`, `date_created`) VALUES
(6, 'Harun', 'Arrosid', 'Bagas', 'user@gmail.com', 'ee11cbb19052e40b07aac0ca060c23ee', 'Indonesia', '2019-02-08', 'Bogor', '300_6.jpg', NULL, 9, 1, 'Any Time', 'mantap', 'KOTA DEPOK', '', 'Bagus', 'Adik Kandung', '089616837849', 'mantapp', 2, 1, NULL),
(7, NULL, NULL, NULL, 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(8, 'Harun', '', '', 'daftar@gmail.com', '07aad1df9d8908b63e5e8170b2bcc819', '', '0000-00-00', '', '300_3.jpg', NULL, 0, 0, '', '', '', '', '', '', NULL, NULL, 2, NULL, '2019-02-26 04:00:39'),
(9, NULL, NULL, NULL, 'sctrhndyn@gmail.com', '4297f44b13955235245b2497399d7a93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2019-05-19 22:07:19'),
(10, 'Adetia', '', 'Juliana', 'sctrhndyn+1@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '2000-05-17', '', NULL, NULL, 41, 1, '', 'Jalan Krukur Raya ', 'KOTA JAKARTA SELATAN', '0895607189231', 'Aris', 'Ibu', NULL, '', 2, NULL, '2019-05-19 22:20:16'),
(11, 'Erika', '', 'Cahyani', 'sctrhndyn+2@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '2001-06-01', '', NULL, NULL, 20, 1, '', 'Cagar Alam Krl', 'KOTA DEPOK', '089535621126', 'diane', 'ibu', NULL, NULL, 2, NULL, '2019-05-19 22:32:49'),
(12, 'sulis', 'tiara', '', 'sctrhndyn+3@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '2000-12-03', '', NULL, NULL, 19, 1, '', 'Jalan Margonda Raya', 'KOTA DEPOK', '089672771430', 'Ryan', 'Bapak', NULL, '', 2, NULL, '2019-05-19 22:38:06'),
(13, 'Aulia', '', 'Nurrahmanita', 'sctrhndyn+4@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '2000-02-08', '', NULL, NULL, 34, 1, '', 'Jalan Dewi Sartika', 'KOTA DEPOK', '08952194112', 'ryan', 'Bapak', NULL, '', 2, NULL, '2019-05-19 22:42:22'),
(14, 'Nadya', 'Pramesti', 'Kirana', 'sctrhndyn+5@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '2001-03-30', '', NULL, NULL, 14, 1, '', 'Jalan M.Kahfi', 'KOTA JAKARTA SELATAN', '081290555394', 'kiki', 'kakak', NULL, '', 2, NULL, '2019-05-19 22:47:20'),
(15, 'Melia', '', 'Sari', 'sctrhndyn+6@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '2000-05-20', '', NULL, NULL, 23, 1, '', 'Jalan Villa Santika', 'KOTA DEPOK', '087780862148', 'difa', 'adik', NULL, '', 2, NULL, '2019-05-19 23:11:53'),
(16, 'Ali ', 'Rahman', '', 'sctrhndyn+7@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '1998-08-22', '', NULL, NULL, 9, 1, '', 'Jalan Srengseng Sawah', 'KOTA JAKARTA SELATAN', '0895033396846', 'Nur', 'Istri', NULL, '', 2, NULL, '2019-05-19 23:15:56'),
(17, 'Arif', 'Ramadhan', '', 'sctrhndyn+8@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '1999-04-17', '', NULL, NULL, 10, 1, '', 'Jalan Mangga', 'KOTA DEPOK', '0895344141781', 'Sarti', 'Istri', NULL, '', 2, NULL, '2019-05-19 23:19:48'),
(18, 'Sururi', 'Junita', '', 'sctrhndyn+9@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '2000-06-29', '', NULL, NULL, 22, 1, '', 'Jalan Gg.Hj Umar', 'KOTA BOGOR', '08977773643', 'risa', 'adik', NULL, '', 2, NULL, '2019-05-19 23:23:24'),
(19, 'Sucitra ', 'Handayani', '', 'sctrhndyn+10@gmail.com', '4297f44b13955235245b2497399d7a93', 'Indonesia', '2000-11-03', '', NULL, NULL, 19, 1, '', 'Jalan Pindahan II', 'KOTA DEPOK', '0895344149781', 'omi', 'Ibu', NULL, '', 2, NULL, '2019-05-19 23:29:15'),
(20, NULL, NULL, NULL, 'adeskron51@gmail.com', '52655800aa39fa9b9f700fe46f84783d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2019-06-10 04:01:42'),
(21, 'Ade', '', 'Sutisna', 'endahnurrahman@yahoo.com', '2c66c492299cc7ed119f6aa96907a2f1', 'Indonesian', '1984-01-11', 'Jakarta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2019-06-30 21:29:26'),
(22, NULL, NULL, NULL, 'dwiwahyusu54@gmail.com', '95e8305df54fb65b7943023077bdb8b3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2019-08-12 23:46:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `certificate`
--

CREATE TABLE `certificate` (
  `id_certificate` int(11) NOT NULL,
  `name_certificate` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `certificate`
--

INSERT INTO `certificate` (`id_certificate`, `name_certificate`) VALUES
(1, 'certificate 1'),
(2, 'certificate 2'),
(3, 'certificate 3'),
(4, 'certificate 4'),
(5, 'certificate 5'),
(6, 'certificate 6'),
(7, 'certificate 7');

-- --------------------------------------------------------

--
-- Struktur dari tabel `certification`
--

CREATE TABLE `certification` (
  `id_certification` int(11) NOT NULL,
  `id_certificate` int(11) DEFAULT NULL,
  `number_certification` varchar(25) DEFAULT NULL,
  `place_of_issue_certification` varchar(50) DEFAULT NULL,
  `date_of_issue_certification` date DEFAULT NULL,
  `expired_date_certification` date DEFAULT NULL,
  `id_applications` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `certification`
--

INSERT INTO `certification` (`id_certification`, `id_certificate`, `number_certification`, `place_of_issue_certification`, `date_of_issue_certification`, `expired_date_certification`, `id_applications`) VALUES
(5, 1, NULL, NULL, '2019-04-12', NULL, 6),
(6, 3, NULL, NULL, '0000-00-00', NULL, 10),
(7, 4, NULL, NULL, '0000-00-00', NULL, 12),
(8, 3, NULL, NULL, '0000-00-00', NULL, 13),
(9, 6, NULL, NULL, '0000-00-00', NULL, 14),
(10, 3, NULL, NULL, '0000-00-00', NULL, 15),
(11, 3, NULL, NULL, '0000-00-00', NULL, 16),
(12, 3, NULL, NULL, '0000-00-00', NULL, 17),
(13, 3, NULL, NULL, '0000-00-00', NULL, 18),
(14, 3, NULL, NULL, '0000-00-00', NULL, 19),
(15, 0, NULL, NULL, '0000-00-00', NULL, 6),
(16, 5, '7827382', 'depok', '2019-09-02', '2019-09-05', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `company`
--

CREATE TABLE `company` (
  `id_company` int(11) NOT NULL,
  `name_company` varchar(50) DEFAULT NULL,
  `manager_company` varchar(50) DEFAULT NULL,
  `address_company` text DEFAULT NULL,
  `phone_company` int(15) DEFAULT NULL,
  `nationality_company` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `company`
--

INSERT INTO `company` (`id_company`, `name_company`, `manager_company`, `address_company`, `phone_company`, `nationality_company`) VALUES
(1, 'Company 1', 'Manager 1', 'blabla', 67677, 'Argentina'),
(2, 'Company 2', 'Manager 2', 'blabla', 67677, 'bhbh'),
(3, 'Company 3', 'Manager 3', 'blabla', 67677, 'bhbh'),
(4, 'Company 4', 'Manager 4', 'blabla', 67677, 'bhbh'),
(5, 'Company 5', 'Manager 5', 'ndjsa', 7822, 'Indonesia'),
(6, 'Company 6', 'Manager 6', 'dsgdh', 3728, 'Indonesia'),
(7, 'Company 3', 'Manager 3', 'blabla', 67677, 'bhbh'),
(8, 'Company 4', 'Manager 4', 'blabla', 67677, 'bhbh'),
(9, 'Company 5', 'Manager 5', 'ndjsa', 7822, 'Indonesia'),
(10, 'Company 6', 'Manager 6', 'dsgdh', 3728, 'Indonesia');

-- --------------------------------------------------------

--
-- Struktur dari tabel `documents`
--

CREATE TABLE `documents` (
  `id_documents` int(11) NOT NULL,
  `id_documents_user` int(11) NOT NULL,
  `number_documents` varchar(20) DEFAULT NULL,
  `place_of_issue_documents` varchar(100) DEFAULT NULL,
  `date_of_issue` date DEFAULT NULL,
  `date_of_expire` date DEFAULT NULL,
  `photo_documents` text DEFAULT NULL,
  `id_applications` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `documents`
--

INSERT INTO `documents` (`id_documents`, `id_documents_user`, `number_documents`, `place_of_issue_documents`, `date_of_issue`, `date_of_expire`, `photo_documents`, `id_applications`) VALUES
(43, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 10),
(44, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 12),
(45, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 13),
(46, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 14),
(47, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 15),
(48, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 16),
(49, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 17),
(50, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 18),
(51, 3, '', NULL, '0000-00-00', '0000-00-00', NULL, 19),
(57, 4, '7437483', 'depok', '2019-09-01', '2019-09-03', NULL, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `documents_user`
--

CREATE TABLE `documents_user` (
  `id_documents_user` int(11) NOT NULL,
  `name_documents` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `documents_user`
--

INSERT INTO `documents_user` (`id_documents_user`, `name_documents`) VALUES
(1, 'Document 1'),
(2, 'Document 2'),
(3, 'Document 3'),
(4, 'Document 4'),
(5, 'Document 5'),
(6, 'Document 6'),
(7, 'Document 7');

-- --------------------------------------------------------

--
-- Struktur dari tabel `engine`
--

CREATE TABLE `engine` (
  `id_engine` int(11) NOT NULL,
  `name_engine` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `engine`
--

INSERT INTO `engine` (`id_engine`, `name_engine`) VALUES
(1, 'engine 1'),
(2, 'engine 2'),
(3, 'engine 3'),
(4, 'engine 4'),
(5, 'engine 5'),
(6, 'engine 6'),
(7, 'engine 7');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_applications`
--

CREATE TABLE `history_applications` (
  `id_history` int(11) NOT NULL,
  `id_applications` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name_surname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nationality` varchar(20) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `place_of_birth` varchar(50) DEFAULT NULL,
  `photo` text DEFAULT NULL,
  `gender` varchar(12) DEFAULT NULL,
  `id_position` int(11) DEFAULT NULL,
  `accept_lower_rank` tinyint(1) DEFAULT NULL,
  `available_from` varchar(25) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `next_of_kin` varchar(20) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  `id_vessel` int(11) DEFAULT NULL,
  `id_pool` int(11) DEFAULT NULL,
  `id_tov` int(11) DEFAULT NULL,
  `range_salary_start` int(11) DEFAULT NULL,
  `range_salary_end` int(11) DEFAULT NULL,
  `work_from` date DEFAULT NULL,
  `work_to` date DEFAULT NULL,
  `file_wawancara` text DEFAULT NULL,
  `contract_periode` varchar(5) DEFAULT NULL,
  `salary` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_applications`
--

INSERT INTO `history_applications` (`id_history`, `id_applications`, `first_name`, `middle_name`, `last_name_surname`, `email`, `nationality`, `date_of_birth`, `place_of_birth`, `photo`, `gender`, `id_position`, `accept_lower_rank`, `available_from`, `address`, `city`, `phone`, `next_of_kin`, `relationship`, `remark`, `status`, `date_created`, `id_company`, `id_vessel`, `id_pool`, `id_tov`, `range_salary_start`, `range_salary_end`, `work_from`, `work_to`, `file_wawancara`, `contract_periode`, `salary`) VALUES
(15, 6, 'Harun', 'Arrosid', 'Bagas', 'user@gmail.com', 'Indonesia', '2019-02-08', 'Bogor', '300_6.jpg', NULL, 6, 1, 'Any Time', 'mantap', 'KOTA DEPOK', 896, '', 'Lajang', 'mantapp', 4, '2019-03-14 01:00:34', 3, 1, 3, 3, 5000000, 8000000, '2019-05-16', '2019-05-22', NULL, NULL, NULL),
(16, 9, '', '', '', 'sctrhndyn@gmail.com', '', '0000-00-00', '', '', NULL, 0, 0, '', '', '', 0, '', '', NULL, 1, '2019-05-19 22:17:50', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, NULL, NULL, NULL),
(17, 10, 'Adetia', '', 'Juliana', 'sctrhndyn+1@gmail.com', 'Indonesia', '2000-05-17', '', '', NULL, 41, 1, '', 'Jalan Krukur Raya ', 'KOTA JAKARTA SELATAN', 2147483647, 'Aris', 'Ibu', NULL, 4, '2019-05-19 22:29:45', 3, 10, 1, 3, 5000000, 7500000, '2019-05-14', '2020-06-16', NULL, NULL, NULL),
(18, 11, 'Erika', '', 'Cahyani', 'sctrhndyn+2@gmail.com', 'Indonesia', '2001-06-01', '', '', NULL, 20, 1, '', 'Cagar Alam Krl', 'KOTA DEPOK', 2147483647, 'diane', 'ibu', NULL, 3, '2019-05-19 22:37:28', 2, 1, 7, 3, 5000000, 7500000, '2019-09-01', '2019-12-31', NULL, NULL, NULL),
(19, 12, 'sulis', 'tiara', '', 'sctrhndyn+3@gmail.com', 'Indonesia', '2000-12-03', '', '', NULL, 19, 1, '', 'Jalan Margonda Raya', 'KOTA DEPOK', 2147483647, 'Ryan', 'Bapak', NULL, 3, '2019-05-19 22:41:46', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, NULL, NULL, NULL),
(20, 13, 'Aulia', '', 'Nurrahmanita', 'sctrhndyn+4@gmail.com', 'Indonesia', '2000-02-08', '', '', NULL, 34, 1, '', 'Jalan Dewi Sartika', 'KOTA DEPOK', 2147483647, 'ryan', 'Bapak', NULL, 1, '2019-05-19 22:45:37', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, NULL, NULL, NULL),
(21, 13, 'Aulia', '', 'Nurrahmanita', 'sctrhndyn+4@gmail.com', 'Indonesia', '2000-02-08', '', '', NULL, 34, 1, '', 'Jalan Dewi Sartika', 'KOTA DEPOK', 2147483647, 'ryan', 'Bapak', NULL, 1, '2019-05-19 22:46:15', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, NULL, NULL, NULL),
(22, 14, 'Nadya', 'Pramesti', 'Kirana', 'sctrhndyn+5@gmail.com', 'Indonesia', '2001-03-30', '', '', NULL, 14, 1, '', 'Jalan M.Kahfi', 'KOTA JAKARTA SELATAN', 2147483647, 'kiki', 'kakak', NULL, 3, '2019-05-19 23:11:20', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, '2019-08-31_03:28:44_22.pdf', NULL, NULL),
(23, 15, 'Melia', '', 'Sari', 'sctrhndyn+6@gmail.com', 'Indonesia', '2000-05-20', '', '', NULL, 23, 1, '', 'Jalan Villa Santika', 'KOTA DEPOK', 2147483647, 'difa', 'adik', NULL, 3, '2019-05-19 23:15:27', NULL, NULL, NULL, 3, 5000000, 8000000, NULL, NULL, '2019-08-31 03_23.22_23', NULL, NULL),
(24, 16, 'Ali ', 'Rahman', '', 'sctrhndyn+7@gmail.com', 'Indonesia', '1998-08-22', '', '', NULL, 9, 1, '', 'Jalan Srengseng Sawah', 'KOTA JAKARTA SELATAN', 2147483647, 'Nur', 'Istri', NULL, 3, '2019-05-19 23:19:27', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, '2019-08-31_24.pdf', NULL, NULL),
(25, 17, 'Arif', 'Ramadhan', '', 'sctrhndyn+8@gmail.com', 'Indonesia', '1999-04-17', '', '', NULL, 10, 1, '', 'Jalan Mangga', 'KOTA DEPOK', 2147483647, 'Sarti', 'Istri', NULL, 3, '2019-05-19 23:22:52', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, '2019-08-31_03:20:01_25.pdf', NULL, NULL),
(26, 18, 'Sururi', 'Junita', '', 'sctrhndyn+9@gmail.com', 'Indonesia', '2000-06-29', '', '', NULL, 22, 1, '', 'Jalan Gg.Hj Umar', 'KOTA BOGOR', 2147483647, 'risa', 'adik', NULL, 3, '2019-05-19 23:28:42', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, '2019-08-31_26.pdf', NULL, NULL),
(27, 19, 'Sucitra ', 'Handayani', '', 'sctrhndyn+10@gmail.com', 'Indonesia', '2000-11-03', '', '', NULL, 19, 1, '', 'Jalan Pindahan II', 'KOTA DEPOK', 2147483647, 'omi', 'Ibu', NULL, 3, '2019-05-19 23:34:31', NULL, NULL, NULL, 3, 5000000, 7500000, NULL, NULL, '2019-08-31_033103_27.pdf', NULL, NULL),
(28, 19, 'Sucitra ', 'Handayani', '', 'sctrhndyn+10@gmail.com', 'Indonesia', '2000-11-03', '', '', NULL, 19, 1, '', 'Jalan Pindahan II', 'KOTA DEPOK', 2147483647, 'omi', 'Ibu', NULL, 4, '2019-05-19 23:34:51', 2, 10, 4, 3, 5000000, 7500000, '2019-09-05', '2019-11-05', '2019-09-05_053137_28.pdf', '2', '10000000'),
(29, 6, 'Harun', 'Arrosid', 'Bagas', 'user@gmail.com', 'Indonesia', '2019-02-08', 'Bogor', '300_6.jpg', NULL, 9, 1, 'Any Time', 'mantap', 'KOTA DEPOK', 896, '', 'Lajang', NULL, 3, '2019-06-10 01:37:47', NULL, NULL, NULL, 1, 16654, 564685, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten_kota`
--

CREATE TABLE `kabupaten_kota` (
  `id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi_id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `kabupaten_kota`
--

INSERT INTO `kabupaten_kota` (`id`, `provinsi_id`, `name`) VALUES
('1101', '11', 'KABUPATEN SIMEULUE'),
('1102', '11', 'KABUPATEN ACEH SINGKIL'),
('1103', '11', 'KABUPATEN ACEH SELATAN'),
('1104', '11', 'KABUPATEN ACEH TENGGARA'),
('1105', '11', 'KABUPATEN ACEH TIMUR'),
('1106', '11', 'KABUPATEN ACEH TENGAH'),
('1107', '11', 'KABUPATEN ACEH BARAT'),
('1108', '11', 'KABUPATEN ACEH BESAR'),
('1109', '11', 'KABUPATEN PIDIE'),
('1110', '11', 'KABUPATEN BIREUEN'),
('1111', '11', 'KABUPATEN ACEH UTARA'),
('1112', '11', 'KABUPATEN ACEH BARAT DAYA'),
('1113', '11', 'KABUPATEN GAYO LUES'),
('1114', '11', 'KABUPATEN ACEH TAMIANG'),
('1115', '11', 'KABUPATEN NAGAN RAYA'),
('1116', '11', 'KABUPATEN ACEH JAYA'),
('1117', '11', 'KABUPATEN BENER MERIAH'),
('1118', '11', 'KABUPATEN PIDIE JAYA'),
('1171', '11', 'KOTA BANDA ACEH'),
('1172', '11', 'KOTA SABANG'),
('1173', '11', 'KOTA LANGSA'),
('1174', '11', 'KOTA LHOKSEUMAWE'),
('1175', '11', 'KOTA SUBULUSSALAM'),
('1201', '12', 'KABUPATEN NIAS'),
('1202', '12', 'KABUPATEN MANDAILING NATAL'),
('1203', '12', 'KABUPATEN TAPANULI SELATAN'),
('1204', '12', 'KABUPATEN TAPANULI TENGAH'),
('1205', '12', 'KABUPATEN TAPANULI UTARA'),
('1206', '12', 'KABUPATEN TOBA SAMOSIR'),
('1207', '12', 'KABUPATEN LABUHAN BATU'),
('1208', '12', 'KABUPATEN ASAHAN'),
('1209', '12', 'KABUPATEN SIMALUNGUN'),
('1210', '12', 'KABUPATEN DAIRI'),
('1211', '12', 'KABUPATEN KARO'),
('1212', '12', 'KABUPATEN DELI SERDANG'),
('1213', '12', 'KABUPATEN LANGKAT'),
('1214', '12', 'KABUPATEN NIAS SELATAN'),
('1215', '12', 'KABUPATEN HUMBANG HASUNDUTAN'),
('1216', '12', 'KABUPATEN PAKPAK BHARAT'),
('1217', '12', 'KABUPATEN SAMOSIR'),
('1218', '12', 'KABUPATEN SERDANG BEDAGAI'),
('1219', '12', 'KABUPATEN BATU BARA'),
('1220', '12', 'KABUPATEN PADANG LAWAS UTARA'),
('1221', '12', 'KABUPATEN PADANG LAWAS'),
('1222', '12', 'KABUPATEN LABUHAN BATU SELATAN'),
('1223', '12', 'KABUPATEN LABUHAN BATU UTARA'),
('1224', '12', 'KABUPATEN NIAS UTARA'),
('1225', '12', 'KABUPATEN NIAS BARAT'),
('1271', '12', 'KOTA SIBOLGA'),
('1272', '12', 'KOTA TANJUNG BALAI'),
('1273', '12', 'KOTA PEMATANG SIANTAR'),
('1274', '12', 'KOTA TEBING TINGGI'),
('1275', '12', 'KOTA MEDAN'),
('1276', '12', 'KOTA BINJAI'),
('1277', '12', 'KOTA PADANGSIDIMPUAN'),
('1278', '12', 'KOTA GUNUNGSITOLI'),
('1301', '13', 'KABUPATEN KEPULAUAN MENTAWAI'),
('1302', '13', 'KABUPATEN PESISIR SELATAN'),
('1303', '13', 'KABUPATEN SOLOK'),
('1304', '13', 'KABUPATEN SIJUNJUNG'),
('1305', '13', 'KABUPATEN TANAH DATAR'),
('1306', '13', 'KABUPATEN PADANG PARIAMAN'),
('1307', '13', 'KABUPATEN AGAM'),
('1308', '13', 'KABUPATEN LIMA PULUH KOTA'),
('1309', '13', 'KABUPATEN PASAMAN'),
('1310', '13', 'KABUPATEN SOLOK SELATAN'),
('1311', '13', 'KABUPATEN DHARMASRAYA'),
('1312', '13', 'KABUPATEN PASAMAN BARAT'),
('1371', '13', 'KOTA PADANG'),
('1372', '13', 'KOTA SOLOK'),
('1373', '13', 'KOTA SAWAH LUNTO'),
('1374', '13', 'KOTA PADANG PANJANG'),
('1375', '13', 'KOTA BUKITTINGGI'),
('1376', '13', 'KOTA PAYAKUMBUH'),
('1377', '13', 'KOTA PARIAMAN'),
('1401', '14', 'KABUPATEN KUANTAN SINGINGI'),
('1402', '14', 'KABUPATEN INDRAGIRI HULU'),
('1403', '14', 'KABUPATEN INDRAGIRI HILIR'),
('1404', '14', 'KABUPATEN PELALAWAN'),
('1405', '14', 'KABUPATEN S I A K'),
('1406', '14', 'KABUPATEN KAMPAR'),
('1407', '14', 'KABUPATEN ROKAN HULU'),
('1408', '14', 'KABUPATEN BENGKALIS'),
('1409', '14', 'KABUPATEN ROKAN HILIR'),
('1410', '14', 'KABUPATEN KEPULAUAN MERANTI'),
('1471', '14', 'KOTA PEKANBARU'),
('1473', '14', 'KOTA D U M A I'),
('1501', '15', 'KABUPATEN KERINCI'),
('1502', '15', 'KABUPATEN MERANGIN'),
('1503', '15', 'KABUPATEN SAROLANGUN'),
('1504', '15', 'KABUPATEN BATANG HARI'),
('1505', '15', 'KABUPATEN MUARO JAMBI'),
('1506', '15', 'KABUPATEN TANJUNG JABUNG TIMUR'),
('1507', '15', 'KABUPATEN TANJUNG JABUNG BARAT'),
('1508', '15', 'KABUPATEN TEBO'),
('1509', '15', 'KABUPATEN BUNGO'),
('1571', '15', 'KOTA JAMBI'),
('1572', '15', 'KOTA SUNGAI PENUH'),
('1601', '16', 'KABUPATEN OGAN KOMERING ULU'),
('1602', '16', 'KABUPATEN OGAN KOMERING ILIR'),
('1603', '16', 'KABUPATEN MUARA ENIM'),
('1604', '16', 'KABUPATEN LAHAT'),
('1605', '16', 'KABUPATEN MUSI RAWAS'),
('1606', '16', 'KABUPATEN MUSI BANYUASIN'),
('1607', '16', 'KABUPATEN BANYU ASIN'),
('1608', '16', 'KABUPATEN OGAN KOMERING ULU SELATAN'),
('1609', '16', 'KABUPATEN OGAN KOMERING ULU TIMUR'),
('1610', '16', 'KABUPATEN OGAN ILIR'),
('1611', '16', 'KABUPATEN EMPAT LAWANG'),
('1612', '16', 'KABUPATEN PENUKAL ABAB LEMATANG ILIR'),
('1613', '16', 'KABUPATEN MUSI RAWAS UTARA'),
('1671', '16', 'KOTA PALEMBANG'),
('1672', '16', 'KOTA PRABUMULIH'),
('1673', '16', 'KOTA PAGAR ALAM'),
('1674', '16', 'KOTA LUBUKLINGGAU'),
('1701', '17', 'KABUPATEN BENGKULU SELATAN'),
('1702', '17', 'KABUPATEN REJANG LEBONG'),
('1703', '17', 'KABUPATEN BENGKULU UTARA'),
('1704', '17', 'KABUPATEN KAUR'),
('1705', '17', 'KABUPATEN SELUMA'),
('1706', '17', 'KABUPATEN MUKOMUKO'),
('1707', '17', 'KABUPATEN LEBONG'),
('1708', '17', 'KABUPATEN KEPAHIANG'),
('1709', '17', 'KABUPATEN BENGKULU TENGAH'),
('1771', '17', 'KOTA BENGKULU'),
('1801', '18', 'KABUPATEN LAMPUNG BARAT'),
('1802', '18', 'KABUPATEN TANGGAMUS'),
('1803', '18', 'KABUPATEN LAMPUNG SELATAN'),
('1804', '18', 'KABUPATEN LAMPUNG TIMUR'),
('1805', '18', 'KABUPATEN LAMPUNG TENGAH'),
('1806', '18', 'KABUPATEN LAMPUNG UTARA'),
('1807', '18', 'KABUPATEN WAY KANAN'),
('1808', '18', 'KABUPATEN TULANGBAWANG'),
('1809', '18', 'KABUPATEN PESAWARAN'),
('1810', '18', 'KABUPATEN PRINGSEWU'),
('1811', '18', 'KABUPATEN MESUJI'),
('1812', '18', 'KABUPATEN TULANG BAWANG BARAT'),
('1813', '18', 'KABUPATEN PESISIR BARAT'),
('1871', '18', 'KOTA BANDAR LAMPUNG'),
('1872', '18', 'KOTA METRO'),
('1901', '19', 'KABUPATEN BANGKA'),
('1902', '19', 'KABUPATEN BELITUNG'),
('1903', '19', 'KABUPATEN BANGKA BARAT'),
('1904', '19', 'KABUPATEN BANGKA TENGAH'),
('1905', '19', 'KABUPATEN BANGKA SELATAN'),
('1906', '19', 'KABUPATEN BELITUNG TIMUR'),
('1971', '19', 'KOTA PANGKAL PINANG'),
('2101', '21', 'KABUPATEN KARIMUN'),
('2102', '21', 'KABUPATEN BINTAN'),
('2103', '21', 'KABUPATEN NATUNA'),
('2104', '21', 'KABUPATEN LINGGA'),
('2105', '21', 'KABUPATEN KEPULAUAN ANAMBAS'),
('2171', '21', 'KOTA B A T A M'),
('2172', '21', 'KOTA TANJUNG PINANG'),
('3101', '31', 'KABUPATEN KEPULAUAN SERIBU'),
('3171', '31', 'KOTA JAKARTA SELATAN'),
('3172', '31', 'KOTA JAKARTA TIMUR'),
('3173', '31', 'KOTA JAKARTA PUSAT'),
('3174', '31', 'KOTA JAKARTA BARAT'),
('3175', '31', 'KOTA JAKARTA UTARA'),
('3201', '32', 'KABUPATEN BOGOR'),
('3202', '32', 'KABUPATEN SUKABUMI'),
('3203', '32', 'KABUPATEN CIANJUR'),
('3204', '32', 'KABUPATEN BANDUNG'),
('3205', '32', 'KABUPATEN GARUT'),
('3206', '32', 'KABUPATEN TASIKMALAYA'),
('3207', '32', 'KABUPATEN CIAMIS'),
('3208', '32', 'KABUPATEN KUNINGAN'),
('3209', '32', 'KABUPATEN CIREBON'),
('3210', '32', 'KABUPATEN MAJALENGKA'),
('3211', '32', 'KABUPATEN SUMEDANG'),
('3212', '32', 'KABUPATEN INDRAMAYU'),
('3213', '32', 'KABUPATEN SUBANG'),
('3214', '32', 'KABUPATEN PURWAKARTA'),
('3215', '32', 'KABUPATEN KARAWANG'),
('3216', '32', 'KABUPATEN BEKASI'),
('3217', '32', 'KABUPATEN BANDUNG BARAT'),
('3218', '32', 'KABUPATEN PANGANDARAN'),
('3271', '32', 'KOTA BOGOR'),
('3272', '32', 'KOTA SUKABUMI'),
('3273', '32', 'KOTA BANDUNG'),
('3274', '32', 'KOTA CIREBON'),
('3275', '32', 'KOTA BEKASI'),
('3276', '32', 'KOTA DEPOK'),
('3277', '32', 'KOTA CIMAHI'),
('3278', '32', 'KOTA TASIKMALAYA'),
('3279', '32', 'KOTA BANJAR'),
('3301', '33', 'KABUPATEN CILACAP'),
('3302', '33', 'KABUPATEN BANYUMAS'),
('3303', '33', 'KABUPATEN PURBALINGGA'),
('3304', '33', 'KABUPATEN BANJARNEGARA'),
('3305', '33', 'KABUPATEN KEBUMEN'),
('3306', '33', 'KABUPATEN PURWOREJO'),
('3307', '33', 'KABUPATEN WONOSOBO'),
('3308', '33', 'KABUPATEN MAGELANG'),
('3309', '33', 'KABUPATEN BOYOLALI'),
('3310', '33', 'KABUPATEN KLATEN'),
('3311', '33', 'KABUPATEN SUKOHARJO'),
('3312', '33', 'KABUPATEN WONOGIRI'),
('3313', '33', 'KABUPATEN KARANGANYAR'),
('3314', '33', 'KABUPATEN SRAGEN'),
('3315', '33', 'KABUPATEN GROBOGAN'),
('3316', '33', 'KABUPATEN BLORA'),
('3317', '33', 'KABUPATEN REMBANG'),
('3318', '33', 'KABUPATEN PATI'),
('3319', '33', 'KABUPATEN KUDUS'),
('3320', '33', 'KABUPATEN JEPARA'),
('3321', '33', 'KABUPATEN DEMAK'),
('3322', '33', 'KABUPATEN SEMARANG'),
('3323', '33', 'KABUPATEN TEMANGGUNG'),
('3324', '33', 'KABUPATEN KENDAL'),
('3325', '33', 'KABUPATEN BATANG'),
('3326', '33', 'KABUPATEN PEKALONGAN'),
('3327', '33', 'KABUPATEN PEMALANG'),
('3328', '33', 'KABUPATEN TEGAL'),
('3329', '33', 'KABUPATEN BREBES'),
('3371', '33', 'KOTA MAGELANG'),
('3372', '33', 'KOTA SURAKARTA'),
('3373', '33', 'KOTA SALATIGA'),
('3374', '33', 'KOTA SEMARANG'),
('3375', '33', 'KOTA PEKALONGAN'),
('3376', '33', 'KOTA TEGAL'),
('3401', '34', 'KABUPATEN KULON PROGO'),
('3402', '34', 'KABUPATEN BANTUL'),
('3403', '34', 'KABUPATEN GUNUNG KIDUL'),
('3404', '34', 'KABUPATEN SLEMAN'),
('3471', '34', 'KOTA YOGYAKARTA'),
('3501', '35', 'KABUPATEN PACITAN'),
('3502', '35', 'KABUPATEN PONOROGO'),
('3503', '35', 'KABUPATEN TRENGGALEK'),
('3504', '35', 'KABUPATEN TULUNGAGUNG'),
('3505', '35', 'KABUPATEN BLITAR'),
('3506', '35', 'KABUPATEN KEDIRI'),
('3507', '35', 'KABUPATEN MALANG'),
('3508', '35', 'KABUPATEN LUMAJANG'),
('3509', '35', 'KABUPATEN JEMBER'),
('3510', '35', 'KABUPATEN BANYUWANGI'),
('3511', '35', 'KABUPATEN BONDOWOSO'),
('3512', '35', 'KABUPATEN SITUBONDO'),
('3513', '35', 'KABUPATEN PROBOLINGGO'),
('3514', '35', 'KABUPATEN PASURUAN'),
('3515', '35', 'KABUPATEN SIDOARJO'),
('3516', '35', 'KABUPATEN MOJOKERTO'),
('3517', '35', 'KABUPATEN JOMBANG'),
('3518', '35', 'KABUPATEN NGANJUK'),
('3519', '35', 'KABUPATEN MADIUN'),
('3520', '35', 'KABUPATEN MAGETAN'),
('3521', '35', 'KABUPATEN NGAWI'),
('3522', '35', 'KABUPATEN BOJONEGORO'),
('3523', '35', 'KABUPATEN TUBAN'),
('3524', '35', 'KABUPATEN LAMONGAN'),
('3525', '35', 'KABUPATEN GRESIK'),
('3526', '35', 'KABUPATEN BANGKALAN'),
('3527', '35', 'KABUPATEN SAMPANG'),
('3528', '35', 'KABUPATEN PAMEKASAN'),
('3529', '35', 'KABUPATEN SUMENEP'),
('3571', '35', 'KOTA KEDIRI'),
('3572', '35', 'KOTA BLITAR'),
('3573', '35', 'KOTA MALANG'),
('3574', '35', 'KOTA PROBOLINGGO'),
('3575', '35', 'KOTA PASURUAN'),
('3576', '35', 'KOTA MOJOKERTO'),
('3577', '35', 'KOTA MADIUN'),
('3578', '35', 'KOTA SURABAYA'),
('3579', '35', 'KOTA BATU'),
('3601', '36', 'KABUPATEN PANDEGLANG'),
('3602', '36', 'KABUPATEN LEBAK'),
('3603', '36', 'KABUPATEN TANGERANG'),
('3604', '36', 'KABUPATEN SERANG'),
('3671', '36', 'KOTA TANGERANG'),
('3672', '36', 'KOTA CILEGON'),
('3673', '36', 'KOTA SERANG'),
('3674', '36', 'KOTA TANGERANG SELATAN'),
('5101', '51', 'KABUPATEN JEMBRANA'),
('5102', '51', 'KABUPATEN TABANAN'),
('5103', '51', 'KABUPATEN BADUNG'),
('5104', '51', 'KABUPATEN GIANYAR'),
('5105', '51', 'KABUPATEN KLUNGKUNG'),
('5106', '51', 'KABUPATEN BANGLI'),
('5107', '51', 'KABUPATEN KARANG ASEM'),
('5108', '51', 'KABUPATEN BULELENG'),
('5171', '51', 'KOTA DENPASAR'),
('5201', '52', 'KABUPATEN LOMBOK BARAT'),
('5202', '52', 'KABUPATEN LOMBOK TENGAH'),
('5203', '52', 'KABUPATEN LOMBOK TIMUR'),
('5204', '52', 'KABUPATEN SUMBAWA'),
('5205', '52', 'KABUPATEN DOMPU'),
('5206', '52', 'KABUPATEN BIMA'),
('5207', '52', 'KABUPATEN SUMBAWA BARAT'),
('5208', '52', 'KABUPATEN LOMBOK UTARA'),
('5271', '52', 'KOTA MATARAM'),
('5272', '52', 'KOTA BIMA'),
('5301', '53', 'KABUPATEN SUMBA BARAT'),
('5302', '53', 'KABUPATEN SUMBA TIMUR'),
('5303', '53', 'KABUPATEN KUPANG'),
('5304', '53', 'KABUPATEN TIMOR TENGAH SELATAN'),
('5305', '53', 'KABUPATEN TIMOR TENGAH UTARA'),
('5306', '53', 'KABUPATEN BELU'),
('5307', '53', 'KABUPATEN ALOR'),
('5308', '53', 'KABUPATEN LEMBATA'),
('5309', '53', 'KABUPATEN FLORES TIMUR'),
('5310', '53', 'KABUPATEN SIKKA'),
('5311', '53', 'KABUPATEN ENDE'),
('5312', '53', 'KABUPATEN NGADA'),
('5313', '53', 'KABUPATEN MANGGARAI'),
('5314', '53', 'KABUPATEN ROTE NDAO'),
('5315', '53', 'KABUPATEN MANGGARAI BARAT'),
('5316', '53', 'KABUPATEN SUMBA TENGAH'),
('5317', '53', 'KABUPATEN SUMBA BARAT DAYA'),
('5318', '53', 'KABUPATEN NAGEKEO'),
('5319', '53', 'KABUPATEN MANGGARAI TIMUR'),
('5320', '53', 'KABUPATEN SABU RAIJUA'),
('5321', '53', 'KABUPATEN MALAKA'),
('5371', '53', 'KOTA KUPANG'),
('6101', '61', 'KABUPATEN SAMBAS'),
('6102', '61', 'KABUPATEN BENGKAYANG'),
('6103', '61', 'KABUPATEN LANDAK'),
('6104', '61', 'KABUPATEN MEMPAWAH'),
('6105', '61', 'KABUPATEN SANGGAU'),
('6106', '61', 'KABUPATEN KETAPANG'),
('6107', '61', 'KABUPATEN SINTANG'),
('6108', '61', 'KABUPATEN KAPUAS HULU'),
('6109', '61', 'KABUPATEN SEKADAU'),
('6110', '61', 'KABUPATEN MELAWI'),
('6111', '61', 'KABUPATEN KAYONG UTARA'),
('6112', '61', 'KABUPATEN KUBU RAYA'),
('6171', '61', 'KOTA PONTIANAK'),
('6172', '61', 'KOTA SINGKAWANG'),
('6201', '62', 'KABUPATEN KOTAWARINGIN BARAT'),
('6202', '62', 'KABUPATEN KOTAWARINGIN TIMUR'),
('6203', '62', 'KABUPATEN KAPUAS'),
('6204', '62', 'KABUPATEN BARITO SELATAN'),
('6205', '62', 'KABUPATEN BARITO UTARA'),
('6206', '62', 'KABUPATEN SUKAMARA'),
('6207', '62', 'KABUPATEN LAMANDAU'),
('6208', '62', 'KABUPATEN SERUYAN'),
('6209', '62', 'KABUPATEN KATINGAN'),
('6210', '62', 'KABUPATEN PULANG PISAU'),
('6211', '62', 'KABUPATEN GUNUNG MAS'),
('6212', '62', 'KABUPATEN BARITO TIMUR'),
('6213', '62', 'KABUPATEN MURUNG RAYA'),
('6271', '62', 'KOTA PALANGKA RAYA'),
('6301', '63', 'KABUPATEN TANAH LAUT'),
('6302', '63', 'KABUPATEN KOTA BARU'),
('6303', '63', 'KABUPATEN BANJAR'),
('6304', '63', 'KABUPATEN BARITO KUALA'),
('6305', '63', 'KABUPATEN TAPIN'),
('6306', '63', 'KABUPATEN HULU SUNGAI SELATAN'),
('6307', '63', 'KABUPATEN HULU SUNGAI TENGAH'),
('6308', '63', 'KABUPATEN HULU SUNGAI UTARA'),
('6309', '63', 'KABUPATEN TABALONG'),
('6310', '63', 'KABUPATEN TANAH BUMBU'),
('6311', '63', 'KABUPATEN BALANGAN'),
('6371', '63', 'KOTA BANJARMASIN'),
('6372', '63', 'KOTA BANJAR BARU'),
('6401', '64', 'KABUPATEN PASER'),
('6402', '64', 'KABUPATEN KUTAI BARAT'),
('6403', '64', 'KABUPATEN KUTAI KARTANEGARA'),
('6404', '64', 'KABUPATEN KUTAI TIMUR'),
('6405', '64', 'KABUPATEN BERAU'),
('6409', '64', 'KABUPATEN PENAJAM PASER UTARA'),
('6411', '64', 'KABUPATEN MAHAKAM HULU'),
('6471', '64', 'KOTA BALIKPAPAN'),
('6472', '64', 'KOTA SAMARINDA'),
('6474', '64', 'KOTA BONTANG'),
('6501', '65', 'KABUPATEN MALINAU'),
('6502', '65', 'KABUPATEN BULUNGAN'),
('6503', '65', 'KABUPATEN TANA TIDUNG'),
('6504', '65', 'KABUPATEN NUNUKAN'),
('6571', '65', 'KOTA TARAKAN'),
('7101', '71', 'KABUPATEN BOLAANG MONGONDOW'),
('7102', '71', 'KABUPATEN MINAHASA'),
('7103', '71', 'KABUPATEN KEPULAUAN SANGIHE'),
('7104', '71', 'KABUPATEN KEPULAUAN TALAUD'),
('7105', '71', 'KABUPATEN MINAHASA SELATAN'),
('7106', '71', 'KABUPATEN MINAHASA UTARA'),
('7107', '71', 'KABUPATEN BOLAANG MONGONDOW UTARA'),
('7108', '71', 'KABUPATEN SIAU TAGULANDANG BIARO'),
('7109', '71', 'KABUPATEN MINAHASA TENGGARA'),
('7110', '71', 'KABUPATEN BOLAANG MONGONDOW SELATAN'),
('7111', '71', 'KABUPATEN BOLAANG MONGONDOW TIMUR'),
('7171', '71', 'KOTA MANADO'),
('7172', '71', 'KOTA BITUNG'),
('7173', '71', 'KOTA TOMOHON'),
('7174', '71', 'KOTA KOTAMOBAGU'),
('7201', '72', 'KABUPATEN BANGGAI KEPULAUAN'),
('7202', '72', 'KABUPATEN BANGGAI'),
('7203', '72', 'KABUPATEN MOROWALI'),
('7204', '72', 'KABUPATEN POSO'),
('7205', '72', 'KABUPATEN DONGGALA'),
('7206', '72', 'KABUPATEN TOLI-TOLI'),
('7207', '72', 'KABUPATEN BUOL'),
('7208', '72', 'KABUPATEN PARIGI MOUTONG'),
('7209', '72', 'KABUPATEN TOJO UNA-UNA'),
('7210', '72', 'KABUPATEN SIGI'),
('7211', '72', 'KABUPATEN BANGGAI LAUT'),
('7212', '72', 'KABUPATEN MOROWALI UTARA'),
('7271', '72', 'KOTA PALU'),
('7301', '73', 'KABUPATEN KEPULAUAN SELAYAR'),
('7302', '73', 'KABUPATEN BULUKUMBA'),
('7303', '73', 'KABUPATEN BANTAENG'),
('7304', '73', 'KABUPATEN JENEPONTO'),
('7305', '73', 'KABUPATEN TAKALAR'),
('7306', '73', 'KABUPATEN GOWA'),
('7307', '73', 'KABUPATEN SINJAI'),
('7308', '73', 'KABUPATEN MAROS'),
('7309', '73', 'KABUPATEN PANGKAJENE DAN KEPULAUAN'),
('7310', '73', 'KABUPATEN BARRU'),
('7311', '73', 'KABUPATEN BONE'),
('7312', '73', 'KABUPATEN SOPPENG'),
('7313', '73', 'KABUPATEN WAJO'),
('7314', '73', 'KABUPATEN SIDENRENG RAPPANG'),
('7315', '73', 'KABUPATEN PINRANG'),
('7316', '73', 'KABUPATEN ENREKANG'),
('7317', '73', 'KABUPATEN LUWU'),
('7318', '73', 'KABUPATEN TANA TORAJA'),
('7322', '73', 'KABUPATEN LUWU UTARA'),
('7325', '73', 'KABUPATEN LUWU TIMUR'),
('7326', '73', 'KABUPATEN TORAJA UTARA'),
('7371', '73', 'KOTA MAKASSAR'),
('7372', '73', 'KOTA PAREPARE'),
('7373', '73', 'KOTA PALOPO'),
('7401', '74', 'KABUPATEN BUTON'),
('7402', '74', 'KABUPATEN MUNA'),
('7403', '74', 'KABUPATEN KONAWE'),
('7404', '74', 'KABUPATEN KOLAKA'),
('7405', '74', 'KABUPATEN KONAWE SELATAN'),
('7406', '74', 'KABUPATEN BOMBANA'),
('7407', '74', 'KABUPATEN WAKATOBI'),
('7408', '74', 'KABUPATEN KOLAKA UTARA'),
('7409', '74', 'KABUPATEN BUTON UTARA'),
('7410', '74', 'KABUPATEN KONAWE UTARA'),
('7411', '74', 'KABUPATEN KOLAKA TIMUR'),
('7412', '74', 'KABUPATEN KONAWE KEPULAUAN'),
('7413', '74', 'KABUPATEN MUNA BARAT'),
('7414', '74', 'KABUPATEN BUTON TENGAH'),
('7415', '74', 'KABUPATEN BUTON SELATAN'),
('7471', '74', 'KOTA KENDARI'),
('7472', '74', 'KOTA BAUBAU'),
('7501', '75', 'KABUPATEN BOALEMO'),
('7502', '75', 'KABUPATEN GORONTALO'),
('7503', '75', 'KABUPATEN POHUWATO'),
('7504', '75', 'KABUPATEN BONE BOLANGO'),
('7505', '75', 'KABUPATEN GORONTALO UTARA'),
('7571', '75', 'KOTA GORONTALO'),
('7601', '76', 'KABUPATEN MAJENE'),
('7602', '76', 'KABUPATEN POLEWALI MANDAR'),
('7603', '76', 'KABUPATEN MAMASA'),
('7604', '76', 'KABUPATEN MAMUJU'),
('7605', '76', 'KABUPATEN MAMUJU UTARA'),
('7606', '76', 'KABUPATEN MAMUJU TENGAH'),
('8101', '81', 'KABUPATEN MALUKU TENGGARA BARAT'),
('8102', '81', 'KABUPATEN MALUKU TENGGARA'),
('8103', '81', 'KABUPATEN MALUKU TENGAH'),
('8104', '81', 'KABUPATEN BURU'),
('8105', '81', 'KABUPATEN KEPULAUAN ARU'),
('8106', '81', 'KABUPATEN SERAM BAGIAN BARAT'),
('8107', '81', 'KABUPATEN SERAM BAGIAN TIMUR'),
('8108', '81', 'KABUPATEN MALUKU BARAT DAYA'),
('8109', '81', 'KABUPATEN BURU SELATAN'),
('8171', '81', 'KOTA AMBON'),
('8172', '81', 'KOTA TUAL'),
('8201', '82', 'KABUPATEN HALMAHERA BARAT'),
('8202', '82', 'KABUPATEN HALMAHERA TENGAH'),
('8203', '82', 'KABUPATEN KEPULAUAN SULA'),
('8204', '82', 'KABUPATEN HALMAHERA SELATAN'),
('8205', '82', 'KABUPATEN HALMAHERA UTARA'),
('8206', '82', 'KABUPATEN HALMAHERA TIMUR'),
('8207', '82', 'KABUPATEN PULAU MOROTAI'),
('8208', '82', 'KABUPATEN PULAU TALIABU'),
('8271', '82', 'KOTA TERNATE'),
('8272', '82', 'KOTA TIDORE KEPULAUAN'),
('9101', '91', 'KABUPATEN FAKFAK'),
('9102', '91', 'KABUPATEN KAIMANA'),
('9103', '91', 'KABUPATEN TELUK WONDAMA'),
('9104', '91', 'KABUPATEN TELUK BINTUNI'),
('9105', '91', 'KABUPATEN MANOKWARI'),
('9106', '91', 'KABUPATEN SORONG SELATAN'),
('9107', '91', 'KABUPATEN SORONG'),
('9108', '91', 'KABUPATEN RAJA AMPAT'),
('9109', '91', 'KABUPATEN TAMBRAUW'),
('9110', '91', 'KABUPATEN MAYBRAT'),
('9111', '91', 'KABUPATEN MANOKWARI SELATAN'),
('9112', '91', 'KABUPATEN PEGUNUNGAN ARFAK'),
('9171', '91', 'KOTA SORONG'),
('9401', '94', 'KABUPATEN MERAUKE'),
('9402', '94', 'KABUPATEN JAYAWIJAYA'),
('9403', '94', 'KABUPATEN JAYAPURA'),
('9404', '94', 'KABUPATEN NABIRE'),
('9408', '94', 'KABUPATEN KEPULAUAN YAPEN'),
('9409', '94', 'KABUPATEN BIAK NUMFOR'),
('9410', '94', 'KABUPATEN PANIAI'),
('9411', '94', 'KABUPATEN PUNCAK JAYA'),
('9412', '94', 'KABUPATEN MIMIKA'),
('9413', '94', 'KABUPATEN BOVEN DIGOEL'),
('9414', '94', 'KABUPATEN MAPPI'),
('9415', '94', 'KABUPATEN ASMAT'),
('9416', '94', 'KABUPATEN YAHUKIMO'),
('9417', '94', 'KABUPATEN PEGUNUNGAN BINTANG'),
('9418', '94', 'KABUPATEN TOLIKARA'),
('9419', '94', 'KABUPATEN SARMI'),
('9420', '94', 'KABUPATEN KEEROM'),
('9426', '94', 'KABUPATEN WAROPEN'),
('9427', '94', 'KABUPATEN SUPIORI'),
('9428', '94', 'KABUPATEN MAMBERAMO RAYA'),
('9429', '94', 'KABUPATEN NDUGA'),
('9430', '94', 'KABUPATEN LANNY JAYA'),
('9431', '94', 'KABUPATEN MAMBERAMO TENGAH'),
('9432', '94', 'KABUPATEN YALIMO'),
('9433', '94', 'KABUPATEN PUNCAK'),
('9434', '94', 'KABUPATEN DOGIYAI'),
('9435', '94', 'KABUPATEN INTAN JAYA'),
('9436', '94', 'KABUPATEN DEIYAI'),
('9471', '94', 'KOTA JAYAPURA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `license`
--

CREATE TABLE `license` (
  `id_license` int(11) NOT NULL,
  `id_license_user` int(11) NOT NULL,
  `grade_of_license` varchar(50) DEFAULT NULL,
  `number_license` varchar(30) DEFAULT NULL,
  `place_of_issue_license` varchar(50) DEFAULT NULL,
  `date_of_issue_license` date DEFAULT NULL,
  `date_of_expire_license` date DEFAULT NULL,
  `photo_license` text DEFAULT NULL,
  `id_applications` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `license`
--

INSERT INTO `license` (`id_license`, `id_license_user`, `grade_of_license`, `number_license`, `place_of_issue_license`, `date_of_issue_license`, `date_of_expire_license`, `photo_license`, `id_applications`) VALUES
(41, 3, '', '', '', '0000-00-00', '0000-00-00', NULL, 12),
(42, 3, '', '', '', '0000-00-00', '0000-00-00', NULL, 13),
(43, 3, '', '', '', '0000-00-00', '0000-00-00', NULL, 14),
(44, 3, '', '', '', '0000-00-00', '0000-00-00', NULL, 15),
(45, 3, '', '', '', '0000-00-00', '0000-00-00', NULL, 16),
(46, 3, '', '', '', '0000-00-00', '0000-00-00', NULL, 17),
(47, 3, '', '', '', '0000-00-00', '0000-00-00', NULL, 18),
(48, 3, '', '', '', '0000-00-00', '0000-00-00', NULL, 19),
(50, 2, 'Management', '637263', 'Depok', '2019-09-01', '2019-09-03', NULL, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `license_user`
--

CREATE TABLE `license_user` (
  `id_license_user` int(11) NOT NULL,
  `name_license` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `license_user`
--

INSERT INTO `license_user` (`id_license_user`, `name_license`) VALUES
(1, 'license 1'),
(2, 'license 2'),
(3, 'license 3'),
(4, 'license 4'),
(5, 'license 5'),
(6, 'license 6'),
(7, 'license 7');

-- --------------------------------------------------------

--
-- Struktur dari tabel `negara`
--

CREATE TABLE `negara` (
  `id` int(11) NOT NULL,
  `code_negara` varchar(2) NOT NULL DEFAULT '',
  `name_negara` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `negara`
--

INSERT INTO `negara` (`id`, `code_negara`, `name_negara`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People\'s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People\'s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'SS', 'South Sudan'),
(203, 'ES', 'Spain'),
(204, 'LK', 'Sri Lanka'),
(205, 'SH', 'St. Helena'),
(206, 'PM', 'St. Pierre and Miquelon'),
(207, 'SD', 'Sudan'),
(208, 'SR', 'Suriname'),
(209, 'SJ', 'Svalbard and Jan Mayen Islands'),
(210, 'SZ', 'Swaziland'),
(211, 'SE', 'Sweden'),
(212, 'CH', 'Switzerland'),
(213, 'SY', 'Syrian Arab Republic'),
(214, 'TW', 'Taiwan'),
(215, 'TJ', 'Tajikistan'),
(216, 'TZ', 'Tanzania, United Republic of'),
(217, 'TH', 'Thailand'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad and Tobago'),
(222, 'TN', 'Tunisia'),
(223, 'TR', 'Turkey'),
(224, 'TM', 'Turkmenistan'),
(225, 'TC', 'Turks and Caicos Islands'),
(226, 'TV', 'Tuvalu'),
(227, 'UG', 'Uganda'),
(228, 'UA', 'Ukraine'),
(229, 'AE', 'United Arab Emirates'),
(230, 'GB', 'United Kingdom'),
(231, 'US', 'United States'),
(232, 'UM', 'United States minor outlying islands'),
(233, 'UY', 'Uruguay'),
(234, 'UZ', 'Uzbekistan'),
(235, 'VU', 'Vanuatu'),
(236, 'VA', 'Vatican City State'),
(237, 'VE', 'Venezuela'),
(238, 'VN', 'Vietnam'),
(239, 'VG', 'Virgin Islands (British)'),
(240, 'VI', 'Virgin Islands (U.S.)'),
(241, 'WF', 'Wallis and Futuna Islands'),
(242, 'EH', 'Western Sahara'),
(243, 'YE', 'Yemen'),
(244, 'ZR', 'Zaire'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `open_job`
--

CREATE TABLE `open_job` (
  `id_oj` int(11) NOT NULL,
  `id_tov` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status_oj` tinyint(1) DEFAULT NULL,
  `information` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `open_job`
--

INSERT INTO `open_job` (`id_oj`, `id_tov`, `start_date`, `end_date`, `status_oj`, `information`) VALUES
(2, 1, '2019-02-04', '2019-02-27', 0, NULL),
(3, 2, '2019-02-04', '2019-02-07', 0, NULL),
(5, 3, '2019-03-14', '2019-03-16', 1, NULL),
(6, 1, '2019-05-18', '2019-05-19', 1, 'yeyeyyeye');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pool`
--

CREATE TABLE `pool` (
  `id_pool` int(11) NOT NULL,
  `name_pool` varchar(50) NOT NULL,
  `deskripsi_pool` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pool`
--

INSERT INTO `pool` (`id_pool`, `name_pool`, `deskripsi_pool`) VALUES
(1, 'Pool 1', 'ndsjd'),
(2, 'Pool 2', 'ndsjd'),
(3, 'Pool 3', 'ndsjd'),
(4, 'Pool 4', 'ndsjd'),
(5, 'Pool poolan', 'Polllll'),
(6, 'Pool A', 'Pool untuk kapal tanker'),
(7, 'PT. Humpuss Tranportasi Curah', 'Tug Boat and Barge'),
(8, 'PT. Humpuss Transportasi Kimia', 'Crude Tanker, Product Tanker, Chemical Tanker, LPG Tanker'),
(9, 'PT. Jasa Armada Indonesia', 'Harbour Tug, Pilot Boat, Mooring Boat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pool_vessel`
--

CREATE TABLE `pool_vessel` (
  `id_pv` int(11) NOT NULL,
  `id_pool` int(11) NOT NULL,
  `id_vessel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pool_vessel`
--

INSERT INTO `pool_vessel` (`id_pv`, `id_pool`, `id_vessel`) VALUES
(1, 5, 1),
(2, 5, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `position`
--

CREATE TABLE `position` (
  `id_position` int(11) NOT NULL,
  `name_position` varchar(100) NOT NULL,
  `rank` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `position`
--

INSERT INTO `position` (`id_position`, `name_position`, `rank`) VALUES
(1, '1ST ASST ENGINEER', NULL),
(2, '1ST OFFICER', NULL),
(3, '2ND ASST ENGINEER', NULL),
(4, '2ND OFFICER', NULL),
(5, '3RD ASST ENGINEER', NULL),
(6, '3RD OFFICER', NULL),
(7, '4TH ASST ENGINEER', NULL),
(8, '4TH OFFICER', NULL),
(9, 'ABLE BODY SEAMAN', NULL),
(10, 'ADDL 1ST ASST ENGINEER', NULL),
(11, 'ADDL 1ST OFFICER', NULL),
(12, 'ADDL 2ND ASST ENGINEER', NULL),
(13, 'ADDL 2ND OFFICER', NULL),
(14, 'ADDL 3RD ASST ENGINEER', NULL),
(15, 'ADDL CHIEF ENGINEER', NULL),
(16, 'ADDL CHIEF OFFICER', NULL),
(17, 'ADDL GAS ENGINEER', NULL),
(18, 'ADDL MASTER', NULL),
(19, 'ASSISTANT ENGINEER', NULL),
(20, 'ASST. ENGINEER (TUIE)', NULL),
(21, 'BOSUN', NULL),
(22, 'CARPENTER', NULL),
(23, 'CHIEF COOK', NULL),
(24, 'CHIEF ENGINEER', NULL),
(25, 'CHIEF OFFICER', NULL),
(26, 'DECK BOY', NULL),
(27, 'DECK CADET', NULL),
(28, 'DECK CADET (LING)', NULL),
(29, 'DECK CADET (OFF)', NULL),
(30, 'DECK CADET (TUID)', NULL),
(31, 'DECK GAS MAN', NULL),
(32, 'DECK TRAINEE', NULL),
(33, 'ELECTRICIAN', NULL),
(34, 'ENG. TRAINEE', NULL),
(35, 'ENGINE BOY', NULL),
(36, 'ENGINE CADET', NULL),
(37, 'ENGINE CADET (LING)', NULL),
(38, 'ENGINE CADET (OFF)', NULL),
(39, 'ENGINE GAS MAN', NULL),
(40, 'GAS ENGINEER', NULL),
(41, 'MASTER', NULL),
(42, 'MESSMAN', NULL),
(43, 'OILER', NULL),
(44, 'OILER#1', NULL),
(45, 'ORDINARY SEAMAN', NULL),
(46, 'PUMP MAN', NULL),
(47, 'SECOND COOK', NULL),
(48, 'SENIOR 1ST ASST ENGINEER', NULL),
(49, 'SENIOR 2ND ASST ENGINEER', NULL),
(50, 'SENIOR 2ND OFFICER', NULL),
(51, 'SENIOR CHIEF OFFICER', NULL),
(52, 'STEWARDESS C', NULL),
(53, 'TRAINEE 1ST ASST.ENG', NULL),
(54, 'TRAINEE 1ST OFFICER', NULL),
(55, 'TRAINEE 2ND ASST.ENG', NULL),
(56, 'TRAINEE 2ND OFFICER', NULL),
(57, 'TRAINEE 3RD ASST.ENG', NULL),
(58, 'TRAINEE 3RD OFFICER', NULL),
(59, 'TRAINEE CHIEF ENGINEER', NULL),
(60, 'TRAINEE CHIEF OFFICER', NULL),
(61, 'TRAINEE GAS ENGINEER', NULL),
(62, 'TRAINEE MASTER', NULL),
(63, 'WIPER', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE `provinsi` (
  `id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`id`, `name`) VALUES
('11', 'ACEH'),
('12', 'SUMATERA UTARA'),
('13', 'SUMATERA BARAT'),
('14', 'RIAU'),
('15', 'JAMBI'),
('16', 'SUMATERA SELATAN'),
('17', 'BENGKULU'),
('18', 'LAMPUNG'),
('19', 'KEPULAUAN BANGKA BELITUNG'),
('21', 'KEPULAUAN RIAU'),
('31', 'DKI JAKARTA'),
('32', 'JAWA BARAT'),
('33', 'JAWA TENGAH'),
('34', 'DI YOGYAKARTA'),
('35', 'JAWA TIMUR'),
('36', 'BANTEN'),
('51', 'BALI'),
('52', 'NUSA TENGGARA BARAT'),
('53', 'NUSA TENGGARA TIMUR'),
('61', 'KALIMANTAN BARAT'),
('62', 'KALIMANTAN TENGAH'),
('63', 'KALIMANTAN SELATAN'),
('64', 'KALIMANTAN TIMUR'),
('65', 'KALIMANTAN UTARA'),
('71', 'SULAWESI UTARA'),
('72', 'SULAWESI TENGAH'),
('73', 'SULAWESI SELATAN'),
('74', 'SULAWESI TENGGARA'),
('75', 'GORONTALO'),
('76', 'SULAWESI BARAT'),
('81', 'MALUKU'),
('82', 'MALUKU UTARA'),
('91', 'PAPUA BARAT'),
('94', 'PAPUA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sea_experience`
--

CREATE TABLE `sea_experience` (
  `id_sea_experience` int(11) NOT NULL,
  `name_company_sea_experience` varchar(50) DEFAULT NULL,
  `name_vessel_sea_experience` varchar(50) DEFAULT NULL,
  `id_tov` int(11) DEFAULT NULL,
  `gt_sea_experience` varchar(10) DEFAULT NULL,
  `id_engine` int(11) DEFAULT NULL,
  `bhp_sea_experience` varchar(10) DEFAULT NULL,
  `id_position` int(11) DEFAULT NULL,
  `flag_of_vessel_sea_experience` text DEFAULT NULL,
  `other_crew_nationality_sea_experience` varchar(100) DEFAULT NULL,
  `from_sea_experience` date DEFAULT NULL,
  `to_sea_experience` date DEFAULT NULL,
  `id_applications` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sea_experience`
--

INSERT INTO `sea_experience` (`id_sea_experience`, `name_company_sea_experience`, `name_vessel_sea_experience`, `id_tov`, `gt_sea_experience`, `id_engine`, `bhp_sea_experience`, `id_position`, `flag_of_vessel_sea_experience`, `other_crew_nationality_sea_experience`, `from_sea_experience`, `to_sea_experience`, `id_applications`) VALUES
(7, 'PT Huhuhu', 'Huhuhu', 2, '7337', 4, '726', 3, 'Huhuhu', 'Huhuhu', '2019-05-13', '2019-05-17', 6),
(10, 'asd', '', 1, '', 0, '', 0, '', '', '0000-00-00', '0000-00-00', 6),
(12, 'PT hehe', 'hehe', 2, '800', 4, '800', 2, 'wwkwk', 'American Samoa', '2019-09-18', '2019-09-21', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `type_of_vessel`
--

CREATE TABLE `type_of_vessel` (
  `id_tov` int(11) NOT NULL,
  `name_tov` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `type_of_vessel`
--

INSERT INTO `type_of_vessel` (`id_tov`, `name_tov`) VALUES
(1, 'Pilot Boat'),
(2, 'Feri'),
(3, 'Kargo'),
(4, 'Crude Tanker'),
(5, 'Product Tanker'),
(6, 'General Cargo'),
(7, 'Chemical Tanker'),
(8, 'Bulk Carrier');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vessel`
--

CREATE TABLE `vessel` (
  `id_vessel` int(11) NOT NULL,
  `name_vessel` varchar(50) DEFAULT NULL,
  `gt_vessel` int(10) DEFAULT NULL,
  `id_engine` int(11) DEFAULT NULL,
  `bhp_vessel` int(10) DEFAULT NULL,
  `flag_vessel` text DEFAULT NULL,
  `dwt_vessel` text DEFAULT NULL,
  `id_tov` int(11) NOT NULL,
  `id_company` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `vessel`
--

INSERT INTO `vessel` (`id_vessel`, `name_vessel`, `gt_vessel`, `id_engine`, `bhp_vessel`, `flag_vessel`, `dwt_vessel`, `id_tov`, `id_company`) VALUES
(1, 'Vessel 1', 7847, 1, 873, 'dsdsdsa', '3232', 2, 9),
(10, 'Vessel 10', 34343, 2, 4545, 'sdsdasda dsd sa', '3232', 1, 7);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id_applications`);

--
-- Indeks untuk tabel `certificate`
--
ALTER TABLE `certificate`
  ADD PRIMARY KEY (`id_certificate`);

--
-- Indeks untuk tabel `certification`
--
ALTER TABLE `certification`
  ADD PRIMARY KEY (`id_certification`);

--
-- Indeks untuk tabel `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id_company`);

--
-- Indeks untuk tabel `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id_documents`);

--
-- Indeks untuk tabel `documents_user`
--
ALTER TABLE `documents_user`
  ADD PRIMARY KEY (`id_documents_user`);

--
-- Indeks untuk tabel `engine`
--
ALTER TABLE `engine`
  ADD PRIMARY KEY (`id_engine`);

--
-- Indeks untuk tabel `history_applications`
--
ALTER TABLE `history_applications`
  ADD PRIMARY KEY (`id_history`);

--
-- Indeks untuk tabel `kabupaten_kota`
--
ALTER TABLE `kabupaten_kota`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kabupatenkota_provinsi_id_index` (`provinsi_id`);

--
-- Indeks untuk tabel `license`
--
ALTER TABLE `license`
  ADD PRIMARY KEY (`id_license`);

--
-- Indeks untuk tabel `license_user`
--
ALTER TABLE `license_user`
  ADD PRIMARY KEY (`id_license_user`);

--
-- Indeks untuk tabel `negara`
--
ALTER TABLE `negara`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `open_job`
--
ALTER TABLE `open_job`
  ADD PRIMARY KEY (`id_oj`);

--
-- Indeks untuk tabel `pool`
--
ALTER TABLE `pool`
  ADD PRIMARY KEY (`id_pool`);

--
-- Indeks untuk tabel `pool_vessel`
--
ALTER TABLE `pool_vessel`
  ADD PRIMARY KEY (`id_pv`);

--
-- Indeks untuk tabel `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id_position`);

--
-- Indeks untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sea_experience`
--
ALTER TABLE `sea_experience`
  ADD PRIMARY KEY (`id_sea_experience`);

--
-- Indeks untuk tabel `type_of_vessel`
--
ALTER TABLE `type_of_vessel`
  ADD PRIMARY KEY (`id_tov`);

--
-- Indeks untuk tabel `vessel`
--
ALTER TABLE `vessel`
  ADD PRIMARY KEY (`id_vessel`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `applications`
--
ALTER TABLE `applications`
  MODIFY `id_applications` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `certificate`
--
ALTER TABLE `certificate`
  MODIFY `id_certificate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `certification`
--
ALTER TABLE `certification`
  MODIFY `id_certification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `company`
--
ALTER TABLE `company`
  MODIFY `id_company` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `documents`
--
ALTER TABLE `documents`
  MODIFY `id_documents` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT untuk tabel `documents_user`
--
ALTER TABLE `documents_user`
  MODIFY `id_documents_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `engine`
--
ALTER TABLE `engine`
  MODIFY `id_engine` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `history_applications`
--
ALTER TABLE `history_applications`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `license`
--
ALTER TABLE `license`
  MODIFY `id_license` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT untuk tabel `license_user`
--
ALTER TABLE `license_user`
  MODIFY `id_license_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `negara`
--
ALTER TABLE `negara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT untuk tabel `open_job`
--
ALTER TABLE `open_job`
  MODIFY `id_oj` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pool`
--
ALTER TABLE `pool`
  MODIFY `id_pool` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `pool_vessel`
--
ALTER TABLE `pool_vessel`
  MODIFY `id_pv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `position`
--
ALTER TABLE `position`
  MODIFY `id_position` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT untuk tabel `sea_experience`
--
ALTER TABLE `sea_experience`
  MODIFY `id_sea_experience` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `type_of_vessel`
--
ALTER TABLE `type_of_vessel`
  MODIFY `id_tov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `vessel`
--
ALTER TABLE `vessel`
  MODIFY `id_vessel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kabupaten_kota`
--
ALTER TABLE `kabupaten_kota`
  ADD CONSTRAINT `kabupatenkota_provinsi_id_foreign` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
