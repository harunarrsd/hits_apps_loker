<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends CI_Controller {

    function __construct()
	{
        parent::__construct();
		$this->load->model('m_hits');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('home');
		}        
    }
    
    public function index()
	{
        $data['title'] = 'Seafarer Management System';
        $data['sql'] = $this->m_hits->read_documents_user();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/document/index',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function add()
	{
        $data['title'] = 'Seafarer Management System';
        $data['op'] = 'add';
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/document/add',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    function create_documents_user(){
        $op = $this->input->post('op');
        $id = $this->input->post('id');
        $data = array(
            'name_documents' => $this->input->post('name_documents'),
            'type_documents' => $this->input->post('type_documents')
        );
        if($op=='add'){
            $this->m_hits->create_documents_user($data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('document/add');
        }else{
            $this->m_hits->update_documents_user($id,$data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('document');
        }
    }
        
    public function edit_documents_user($id){
        $data['title'] = 'Seafarer Management System';
        $data['op'] = 'edit';
        $data['sql'] = $this->m_hits->edit_documents_user($id);
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/document/add',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }
        
    public function delete_documents_user($id){
        $this->m_hits->delete_documents_user($id);
        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('document');
    }
}
