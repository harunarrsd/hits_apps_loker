<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

   function __construct() {
      parent::__construct();
      $this->load->model('m_hits');
      if ($this->session->userdata('udhmasuk')==false) {
         redirect('home');
      }        
   }

   public function index() {
      $data['title'] = 'Seafarer Management System';
      $data['header'] = $this->load->view('layout/header','',true);

      if ($this->session->userdata('role') == 1) {
         $data['sidebar'] = $this->load->view('layout/sidebar','',true);
      } else {
         $data['sidebar'] = $this->load->view('layout/sidebar_user','',true);
      }
      
      $data['pages'] = $this->load->view('pages/user/setting/index',array('main'=>$data),true);
      $this->load->view('master',array('main'=>$data));
   }
}