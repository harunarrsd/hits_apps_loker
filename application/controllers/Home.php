<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index(){
		$data['title'] = 'Seafarer Management System';
		$this->load->view('pages/home',array('main'=>$data));
	}

	function create_applications(){
		$data = array(
			'email' => $this->input->post('email_daftar'), 
			'password' => md5($this->input->post('password_daftar')),
			'role' => 2,
			'created_at' => date("Y-m-d H:i:s")
		);

		$this->m_hits->create_applications($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Pendaftaran Berhasil !</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

		redirect('home','refresh');
	}

	function ceklogin(){
		if (isset($_POST['login'])) {
			$email = $this->input->post('email');
			$pass = md5($this->input->post('password'));
			$cek = $this->m_hits->proseslogin($email, $pass);

			if ($cek) {
				$data = array('udhmasuk' => true,
					'id'=>$cek->id_user,
					'email'=>$cek->email,
					'role' => $cek->role,
					'first_name' => $cek->first_name,
					'surname' => $cek->last_name_surname);
				$this->session->set_userdata($data);
				redirect('dashboard');
			} else {
				$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Maaf Email atau Password Anda Salah !</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('home','refresh');
			}
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('home');
	}
}