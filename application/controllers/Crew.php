<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crew extends CI_Controller {

    function __construct()
	{
        parent::__construct();
		$this->load->model('m_hits');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('home');
		}        
	}

	public function index()
	{
        $data['title'] = 'Seaferer Management System';
        $data['sql1'] = $this->m_hits->read_on_board_crew();
        $data['sql2'] = $this->m_hits->read_principal();
        $data['sql3'] = $this->m_hits->read_vessel();
        $data['sql4'] = $this->m_hits->read_pool();
        $data['sql5'] = $this->m_hits->read_type_of_vessel();
        $data['sql6'] = $this->m_hits->read_position();
        $data['sql7'] = $this->m_hits->read_history_applications2();
        $data['sql8'] = $this->m_hits->read_documents_user();
        $data['sql9'] = $this->m_hits->read_sertifikat();
        $data['sql10'] = $this->m_hits->read_license_user();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/crew/index',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function all()
    {
        $data['title'] = 'Seaferer Management System';
        $data['sql0'] = $this->m_hits->read_crew();
        $data['sql2'] = $this->m_hits->read_principal();
        $data['sql3'] = $this->m_hits->read_vessel();
        $data['sql4'] = $this->m_hits->read_pool();
        $data['sql5'] = $this->m_hits->read_type_of_vessel();
        $data['sql6'] = $this->m_hits->read_position();
        $data['sql7'] = $this->m_hits->read_history_applications2();
        $data['sql8'] = $this->m_hits->read_documents_user();
        $data['sql9'] = $this->m_hits->read_sertifikat();
        $data['sql10'] = $this->m_hits->read_license_user();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/crew/all',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function filter(){
        $data['title'] = 'Seaferer Management System';
        $data['sql0'] = $this->m_hits->read_applications_approved();
        $data['sql'] = $this->m_hits->read_applications_approved2();
        $data['sql2'] = $this->m_hits->read_principal();
        $data['sql3'] = $this->m_hits->read_vessel();
        $data['sql4'] = $this->m_hits->read_pool();
        $data['sql5'] = $this->m_hits->read_type_of_vessel();
        $data['sql6'] = $this->m_hits->read_position();
        $data['sql7'] = $this->m_hits->read_history_applications2();
        $data['sql8'] = $this->m_hits->read_documents_user();
        $data['sql9'] = $this->m_hits->read_sertifikat();
        $data['sql10'] = $this->m_hits->read_license_user();
        $filter_rank = $this->input->post('filter_rank');
        $data['filter_rank'] = $filter_rank;
        $filter_pool = $this->input->post('filter_pool');
        $data['filter_pool'] = $filter_pool;
        $filter_company = $this->input->post('filter_company');
        $data['filter_company'] = $filter_company;
        $filter_vessel = $this->input->post('filter_vessel');
        $data['filter_vessel'] = $filter_vessel;
        $filter_tov = $this->input->post('filter_tov');
        $data['filter_tov'] = $filter_tov;
        $filter_address = $this->input->post('filter_address');
        $data['filter_address'] = $filter_address;
        $filter_documents = $this->input->post('filter_documents');
        $data['filter_documents'] = $filter_documents;
        $filter_certificate = $this->input->post('filter_certificate');
        $data['filter_certificate'] = $filter_certificate;
        $filter_license = $this->input->post('filter_license');
        $data['filter_license'] = $filter_license;
        // $data['filter'] = $this->db->query("SELECT * FROM history_applications a JOIN position using(id_position) JOIN type_of_vessel using(id_tov) JOIN company using(id_company) JOIN vessel using(id_vessel) JOIN pool using(id_pool) WHERE a.id_pool LIKE '".$filter_pool."' OR a.id_company LIKE '".$filter_company."' OR a.id_position LIKE '".$filter_rank."' OR a.id_vessel LIKE '".$filter_vessel."' OR a.id_tov LIKE '".$filter_tov."' OR a.city LIKE '".$filter_address."' AND a.status=4");
        $data['filter'] = $this->db->query("SELECT * FROM history_applications a JOIN position using(id_position) JOIN type_of_vessel using(id_tov) JOIN company using(id_company) JOIN vessel using(id_vessel) JOIN pool using(id_pool) JOIN documents d using(id_applications) JOIN certification c using(id_applications) JOIN license l using(id_applications) WHERE a.id_pool LIKE '".$filter_pool."' OR a.id_company LIKE '".$filter_company."' OR a.id_position LIKE '".$filter_rank."' OR a.id_vessel LIKE '".$filter_vessel."' OR a.id_tov LIKE '".$filter_tov."' OR a.city LIKE '".$filter_address."' OR d.id_documents_user LIKE '".$filter_documents."' OR c.id_certificate LIKE '".$filter_certificate."' OR l.id_license_user LIKE '".$filter_license."' AND a.status=4 GROUP BY a.id_applications");
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/crew/filter',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    function update_wait(){
        $id = $this->input->post('id');
        $data = array(
            'status'	=> $this->input->post('status'),
            'id_company'=> $this->input->post('id_company'),
            'id_vessel'	=> $this->input->post('id_vessel'),
            'id_pool'	=> $this->input->post('id_pool'),
            'work_from'	=> $this->input->post('work_from'),
            'work_to'	=> $this->input->post('work_to')
        );
        $this->m_hits->update_history_applications($id,$data);
        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('crew');
    }

    public function data_cv($id)
    {
        $data['crew'] = 'applications';
        $data['read_position'] = $this->m_hits->read_position();
        $data['read_kabupaten_kota'] = $this->m_hits->read_kabupaten_kota();
        $data['read_sertifikat'] = $this->m_hits->read_sertifikat();
        $data['read_documents_user'] = $this->m_hits->read_documents_user();
        $data['read_license_user'] = $this->m_hits->read_license_user();
        $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
        $data['read_engine'] = $this->m_hits->read_engine();
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['sql'] =  $this->m_hits->get_user($id);
        $data['title'] = 'Seafarer Management System';
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/crew/data_cv',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function data_cv_applications($id)
    {
        $data['crew'] = 'applications';
        $data['read_position'] = $this->m_hits->read_position();
        $data['read_kabupaten_kota'] = $this->m_hits->read_kabupaten_kota();
        $data['read_sertifikat'] = $this->m_hits->read_sertifikat();
        $data['read_documents_user'] = $this->m_hits->read_documents_user();
        $data['read_license_user'] = $this->m_hits->read_license_user();
        $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
        $data['read_engine'] = $this->m_hits->read_engine();
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['sql'] =  $this->m_hits->get_user($id);
        $data['title'] = 'Seafarer Management System';
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/crew/data_cv_applications',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function cetak($idhis){
        $data['id'] = $idhis;
        $data['read_position'] = $this->m_hits->read_position();
        $data['read_kabupaten_kota'] = $this->m_hits->read_kabupaten_kota();
        $data['read_provinsi'] = $this->m_hits->read_provinsi();
        $data['read_sertifikat'] = $this->m_hits->read_sertifikat();
        $data['read_documents_user'] = $this->m_hits->read_documents_user();
        $data['read_license_user'] = $this->m_hits->read_license_user();
        $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
        $data['read_engine'] = $this->m_hits->read_engine();
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['title'] = 'Seafarer Management System';
        $this->load->view('pages/crew/cetak',array('main'=>$data));
    }

    public function cetak_eksternal($idhis){
        $data['id'] = $idhis;
        $data['read_position'] = $this->m_hits->read_position();
        $data['title'] = 'Seafarer Management System';
        $this->load->view('pages/crew/cetak_eksternal',array('main'=>$data));
    }

    public function cetak_aplications($idap){
        $data['id'] = $idap;
        $data['read_position'] = $this->m_hits->read_position();
        $data['read_kabupaten_kota'] = $this->m_hits->read_kabupaten_kota();
        $data['read_sertifikat'] = $this->m_hits->read_sertifikat();
        $data['read_documents_user'] = $this->m_hits->read_documents_user();
        $data['read_license_user'] = $this->m_hits->read_license_user();
        $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
        $data['read_engine'] = $this->m_hits->read_engine();
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['title'] = 'Seafarer Management System';
        $this->load->view('pages/crew/cetak_aplications',array('main'=>$data));
    }

    public function cetak_eksternal_aplications($idap){
        $data['id'] = $idap;
        $data['read_position'] = $this->m_hits->read_position();
        $data['read_kabupaten_kota'] = $this->m_hits->read_kabupaten_kota();
        $data['read_sertifikat'] = $this->m_hits->read_sertifikat();
        $data['read_documents_user'] = $this->m_hits->read_documents_user();
        $data['read_license_user'] = $this->m_hits->read_license_user();
        $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
        $data['read_engine'] = $this->m_hits->read_engine();
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['title'] = 'Seafarer Management System';
        $this->load->view('pages/crew/cetak_eksternal_aplications',array('main'=>$data));
    }

    public function update_replacement() {
        $id = $this->input->post('id');
        $data = array(
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'active'  => $this->input->post('status'),
            'updated_at' => date("Y-m-d H:i:s"),
            'updated_by' => $this->session->userdata('id')
        );
        $this->m_hits->update_replacement($id,$data);

        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('crew');
    }
}
