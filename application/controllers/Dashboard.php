<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

   function __construct() { 
      parent::__construct();
      $this->load->model('m_hits');
      if ($this->session->userdata('udhmasuk')==false) {
         redirect('home');
      }        
   }

   public function index() {
      $data['title'] = 'Seafarer Management System';

      if($this->session->userdata('role') == 1) {
         $data['sql'] = $this->m_hits->read_applications()->num_rows();
         $data['sql1'] = $this->m_hits->read_crew()->num_rows();
         $data['sql2'] = $this->m_hits->read_principal()->num_rows();
         $data['sql3'] = $this->m_hits->read_vessel2()->num_rows();
         $data['header'] = $this->load->view('layout/header','',true);
         $data['sidebar'] = $this->load->view('layout/sidebar','',true);
         $data['pages'] = $this->load->view('pages/dashboard',array('main'=>$data),true);

         $this->load->view('master',array('main'=>$data));
      } else {
         $data['header'] = $this->load->view('layout/header','',true);
         $data['sidebar'] = $this->load->view('layout/sidebar_user','',true);
         $data['pages'] = $this->load->view('pages/user/dashboard',array('main'=>$data),true);

         $this->load->view('master',array('main'=>$data));
      }
   }
}
