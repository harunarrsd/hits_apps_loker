<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

        function __construct()
	{
        parent::__construct();
		$this->load->model('m_hits');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('home');
		}        
	}

	public function index()
	{
        $data['title'] = 'Seaferer Management System';
        $data['sql'] = $this->m_hits->read_principal();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/principal/index',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
        }
    
        public function add()
	{
        $data['title'] = 'Seaferer Management System';
        $data['op'] = 'add';
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/principal/add',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
        }
        
        function create_principal(){
                $op = $this->input->post('op');
                $id = $this->input->post('id');
		$data = array(
			'name_company' => $this->input->post('nama_principal'), 
			'manager_company' => $this->input->post('penanggungjawab_principal'),
			'address_company' => $this->input->post('lokasi_principal'),
			'phone_company' => $this->input->post('no_telp_principal'),
			'nationality_company' => $this->input->post('negara_principal')
                );
                if($op=='add'){
                        $this->m_hits->create_principal($data);
                        $this->session->set_flashdata('notif_add','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        redirect('principal/add');
                }else{
                        $this->m_hits->update_principal($id,$data);
                        $this->session->set_flashdata('notif_edit','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        redirect('principal');
                }
        }
        
        public function edit_principal($id)
	{
		$data['title'] = 'Seafarer Management System';
		$data['op'] = 'edit';
                $data['sql'] = $this->m_hits->edit_principal($id);
                $data['read_negara'] = $this->m_hits->read_negara();
                $data['header'] = $this->load->view('layout/header','',true);
                $data['sidebar'] = $this->load->view('layout/sidebar','',true);
		$data['pages'] = $this->load->view('pages/principal/add',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
        }
        
        public function delete_principal($id){
		$this->m_hits->delete_principal($id);
		$this->session->set_flashdata('notif_delete','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		redirect('principal');
	}
}
