<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
        $data['title'] = 'Seafarer Management System';
        $this->load->view('pages/main',array('main'=>$data));
	}

	function create_signup(){
		$data = array(
			'nama_lengkap' => $this->input->post('nama_lengkap'), 
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tanggal_lahir' => $this->input->post('tanggal_lahir'),
			'alamat' => $this->input->post('alamat'),
			'no_ktp' => $this->input->post('no_ktp'),
			'seaferer_code' => $this->input->post('seaferer_code'),
			'posisi' => $this->input->post('posisi'),
			'no_hp' => $this->input->post('no_hp'),
			'email' => $this->input->post('email'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'usia' => $this->input->post('usia'),
			'status' => 'New'
		);
		$this->m_hits->create_signup($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Pendaftaran Berhasil. Harap periksa email Anda.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		redirect('.');
	}
}
