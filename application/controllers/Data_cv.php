<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_cv extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('m_hits');
        if ($this->session->userdata('udhmasuk')==false) {
            redirect('home');
        }        
    }

    public function index() {
        $data['read_position'] = $this->m_hits->read_position();
        $data['read_kabupaten_kota'] = $this->m_hits->read_kabupaten_kota();
        $data['read_provinsi'] = $this->m_hits->read_provinsi();
        $data['read_sertifikat'] = $this->m_hits->read_sertifikat();
        $data['read_documents_user'] = $this->m_hits->read_documents_user();
        $data['read_license_user'] = $this->m_hits->read_license_user();
        $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
        $data['read_engine'] = $this->m_hits->read_engine();
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['title'] = 'Seafarer Management System';
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar_user','',true);
        $data['pages'] = $this->load->view('pages/user/data_cv/index',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function cetak() {
        $data['read_position'] = $this->m_hits->read_position();
        $data['read_kabupaten_kota'] = $this->m_hits->read_kabupaten_kota();
        $data['read_provinsi'] = $this->m_hits->read_provinsi();
        $data['read_sertifikat'] = $this->m_hits->read_sertifikat();
        $data['read_documents_user'] = $this->m_hits->read_documents_user();
        $data['read_license_user'] = $this->m_hits->read_license_user();
        $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
        $data['read_engine'] = $this->m_hits->read_engine();
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['title'] = 'Seafarer Management System';
        $this->load->view('pages/user/data_cv/cetak',array('main'=>$data));
    }

    function update_data_cv(){
        $id = $this->input->post('id');
        $config = array(
            'upload_path'=>'upload/photo_user',
            'allowed_types'=>'pdf|jpg|png|jpeg',
            'max_size'=>2086
        );

        $phone = array();
        if($this->input->post('phone')) {
            array_push($phone, $this->input->post('phone'));
        }
        if($this->input->post('phone_1')) {
            array_push($phone, $this->input->post('phone_1'));
        }
        if($this->input->post('phone_2')) {
            array_push($phone, $this->input->post('phone_2'));
        }

        $this->load->library('upload',$config);
        if($this->upload->do_upload('photo')){
            $finfo = $this->upload->data();
            $photo = $finfo['file_name'];

            $data = array(
                'first_name' => $this->input->post('first_name'),
                'middle_name' => $this->input->post('middle_name'),
                'last_name_surname' => $this->input->post('last_name'),
                'nationality' => $this->input->post('nationality'),
                'date_of_birth' => $this->input->post('date_of_birth'),
                'place_of_birth' => $this->input->post('place_of_birth'),
                'id_position' => $this->input->post('id_position'),
                'accept_lower_rank' => $this->input->post('accept_lower_rank'),
                'available_from' => $this->input->post('available_from'),
                'address' => $this->input->post('address'),
                'province' => $this->input->post('province'),
                'city' => $this->input->post('city'),
                'phone' => implode(",", $phone),
                'name_next_of_kin' => $this->input->post('name_next_of_kin'),
                'phone_next_of_kin' => $this->input->post('phone_next_of_kin'),
                'relationship_next_of_kin' => $this->input->post('relationship_next_of_kin'),
                'photo' => $photo,
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->session->userdata('id')
            );

            $kode_id = array('id_applications'=>$id);
            $photo_db = $this->db->get_where('applications',$kode_id);
            if($photo_db->num_rows()>0){
                $pros=$photo_db->row();
                $name_photo=$pros->photo;

                if(file_exists($lok=FCPATH.'upload/photo_user/'.$name_photo)){
                    unlink($lok);
                }
            }
        } else {
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'middle_name' => $this->input->post('middle_name'),
                'last_name_surname' => $this->input->post('last_name'),
                'nationality' => $this->input->post('nationality'),
                'date_of_birth' => $this->input->post('date_of_birth'),
                'place_of_birth' => $this->input->post('place_of_birth'),
                'id_position' => $this->input->post('id_position'),
                'accept_lower_rank' => $this->input->post('accept_lower_rank'),
                'available_from' => $this->input->post('available_from'),
                'address' => $this->input->post('address'),
                'province' => $this->input->post('province'),
                'city' => $this->input->post('city'),
                'phone' => implode(",", $phone),
                'name_next_of_kin' => $this->input->post('name_next_of_kin'),
                'phone_next_of_kin' => $this->input->post('phone_next_of_kin'),
                'relationship_next_of_kin' => $this->input->post('relationship_next_of_kin'),
                'remark' => $this->input->post('remark'),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->session->userdata('id')
            );
        }
        $this->m_hits->update_data_cv($id,$data);

        // documents
        $name_documents = $this->input->post('name_documents');
        $number_documents = $this->input->post('number_documents');
        $place_of_issue_documents = $this->input->post('place_of_issue_documents');
        $date_of_issue = $this->input->post('date_of_issue');
        $date_of_expire = $this->input->post('date_of_expire');

        $config2 = array(
            'upload_path'=>'upload/photo_documents',
            'allowed_types'=>'pdf|jpg|png|jpeg',
            'max_size'=>2086
        );
        $this->upload->initialize($config2);

        if(is_array($name_documents) && count($name_documents) > 0) {
            $data_documents = array();

            for ($idx=0; $idx < count($name_documents); $idx++) { 
                $temp =  array(
                    'id_documents_user' => $name_documents[$idx],
                    'number_documents' => $number_documents[$idx],
                    'place_of_issue_documents' => $place_of_issue_documents[$idx],
                    'date_of_issue' => $date_of_issue[$idx],
                    'date_of_expire' => $date_of_expire[$idx],
                    'photo_documents' => NULL,
                    'id_applications' => $id,
                );

                if($this->upload->do_upload('photo_documents_' . ($idx + 1))) {
                    $finfo = $this->upload->data();
                    $temp['photo_documents'] = $finfo['file_name'];
                }

                array_push($data_documents, $temp);
            }

            $this->m_hits->create_documents($data_documents);
        }

        // license
        $name_license = $this->input->post('name_license');
        $grade_of_license = $this->input->post('grade_of_license');
        $number_license = $this->input->post('number_license');
        $place_of_issue_license = $this->input->post('place_of_issue_license');
        $date_of_issue_license = $this->input->post('date_of_issue_license');
        $date_of_expire_license = $this->input->post('date_of_expire_license');

        $config2 = array(
            'upload_path'=>'upload/photo_license',
            'allowed_types'=>'pdf|jpg|png|jpeg',
            'max_size'=>2086
        );
        $this->upload->initialize($config2);

        if(is_array($name_license) && count($name_license) > 0) {
            $data_license = array();

            for ($idx=0; $idx < count($name_license); $idx++) { 
                $temp = array(
                    'id_license_user' => $name_license[$idx],
                    'grade_of_license' => $grade_of_license[$idx],
                    'number_license' => $number_license[$idx],
                    'place_of_issue_license' => $place_of_issue_license[$idx],
                    'date_of_issue_license' => $date_of_issue_license[$idx],
                    'date_of_expire_license' => $date_of_expire_license[$idx],
                    'photo_license' => NULL,
                    'id_applications' => $id,
                );

                if($this->upload->do_upload('photo_license_' . ($idx + 1))) {
                    $finfo = $this->upload->data();
                    $temp['photo_license'] = $finfo['file_name'];
                }

                array_push($data_license, $temp);
            }

            $this->m_hits->create_license($data_license);
        }

        //certification
        $id_certificate = $this->input->post('id_certificate');
        $number_certification = $this->input->post('number_certification');
        $place_of_issue_certification = $this->input->post('place_of_issue_certification');
        $date_of_issue_certification = $this->input->post('date_of_issue_certification');
        $expired_date_certification = $this->input->post('expired_date_certification');

        $config2 = array(
            'upload_path'=>'upload/photo_certification',
            'allowed_types'=>'pdf|jpg|png|jpeg',
            'max_size'=>2086
        );
        $this->upload->initialize($config2);

        if(is_array($id_certificate) && count($id_certificate) > 0) {
            $data_certification = array();

            for ($idx=0; $idx < count($id_certificate); $idx++) { 
                $temp = array(
                    'id_certificate' => $id_certificate[$idx],
                    'number_certification' => $number_certification[$idx],
                    'place_of_issue_certification' => $place_of_issue_certification[$idx],
                    'date_of_issue_certification' => $date_of_issue_certification[$idx],
                    'expired_date_certification' => $expired_date_certification[$idx],
                    'photo_certification' => NULL,
                    'id_applications' => $id,
                );

                if($this->upload->do_upload('photo_certification_' . ($idx + 1))) {
                    $finfo = $this->upload->data();
                    $temp['photo_certification'] = $finfo['file_name'];
                }

                array_push($data_certification, $temp);
            }

            $this->m_hits->create_certification($data_certification);
        }

        //sea_experience
        $name_company_se = $this->input->post('name_company_se');
        $name_vessel_se = $this->input->post('name_vessel_se');
        $name_tov_se = $this->input->post('name_tov_se');
        $gt_se = $this->input->post('gt_se');
        $engine_type_se = $this->input->post('engine_type_se');
        $bhp_se = $this->input->post('bhp_se');
        $rank_se = $this->input->post('rank_se');
        $from_se = $this->input->post('from_se');
        $to_se = $this->input->post('to_se');
        $nationality_se = $this->input->post('nationality_se');
        $flag_of_vessel_se = $this->input->post('flag_of_vessel_se');

        if(is_array($name_company_se) && count($name_company_se) > 0) {
            $data_se = array();

            for ($idx=0; $idx < count($name_company_se); $idx++) { 
                $temp = array(
                    'name_company_sea_experience' => $name_company_se[$idx],
                    'other_crew_nationality_sea_experience' => implode(", ", $this->input->post('nationality_se_' . ($idx + 1))),
                    'name_vessel_sea_experience' => $name_vessel_se[$idx],
                    'flag_of_vessel_sea_experience' => $flag_of_vessel_se[$idx],
                    'id_tov' => $name_tov_se[$idx],
                    'gt_sea_experience' => $gt_se[$idx],
                    'id_engine' => $engine_type_se[$idx],
                    'bhp_sea_experience' => $bhp_se[$idx],
                    'id_position' => $rank_se[$idx],
                    'from_sea_experience' => $from_se[$idx],
                    'to_sea_experience' => $to_se[$idx],
                    'id_applications' => $id,
                );

                array_push($data_se, $temp);
            }

            $this->m_hits->create_sea_experience($data_se);
        }

        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data berhasil disimpan' .print_r($nationality_se). '</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('data_cv');
    }

    public function delete_documents($id){
        $this->m_hits->delete_documents($id);
        // $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data documents dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        // redirect('data_cv');

        $res = array(
            'status' => 'SUCCESS', 
            'msg' => 'Data berhasil dihapus');

        header("Content-Type: application/json");
        echo json_encode($res);
    }

    public function delete_license($id){
        $this->m_hits->delete_license($id);
        // $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data license dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        // redirect('data_cv');

        $res = array(
            'status' => 'SUCCESS', 
            'msg' => 'Data berhasil dihapus');

        header("Content-Type: application/json");
        echo json_encode($res);
    }

    public function delete_certification($id){
        $this->m_hits->delete_certification($id);
        // $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data license dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        // redirect('data_cv');

        $res = array(
            'status' => 'SUCCESS', 
            'msg' => 'Data berhasil dihapus');

        header("Content-Type: application/json");
        echo json_encode($res);
    }

    public function delete_sea_experience($id){
        $this->m_hits->delete_sea_experience($id);
        // $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data license dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        // redirect('data_cv');

        $res = array(
            'status' => 'SUCCESS', 
            'msg' => 'Data berhasil dihapus');

        header("Content-Type: application/json");
        echo json_encode($res);
    }
}