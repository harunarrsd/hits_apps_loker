<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_profile extends CI_Controller {

    function __construct()
	{
        parent::__construct();
		$this->load->model('m_hits');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('home');
		}        
    }

    public function index()
	{
        $data['title'] = 'Seafarer Management System';
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar_user','',true);
        $data['pages'] = $this->load->view('pages/my_profile/index',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    function update_my_profile(){
        $id = $this->input->post('id');
        $config = array(
            'upload_path'=>'upload/photo_user',
            'allowed_types'=>'jpg|png|jpeg',
            'max_size'=>2086
            );
        $this->load->library('upload',$config);
        if($this->upload->do_upload('photo')){
            $finfo = $this->upload->data();
            $photo = $finfo['file_name'];

            $data = array(
                'first_name' => $this->input->post('first_name'),
                'middle_name' => $this->input->post('middle_name'),
                'last_name_surname' => $this->input->post('last_name'),
                'nationality' => $this->input->post('nationality'),
                'date_of_birth' => $this->input->post('date_of_birth'),
                'place_of_birth' => $this->input->post('place_of_birth'),
                'email' => $this->input->post('email'),
                'photo' => $photo
            );
            $kode_id = array('id_applications'=>$id);
            $photo_db = $this->db->get_where('applications',$kode_id);
            if($photo_db->num_rows()>0){
                $pros=$photo_db->row();
                $name_photo=$pros->photo;

                if(file_exists($lok=FCPATH.'upload/photo_user/'.$name_photo)){
                    unlink($lok);
                }
            }
        }else{
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'middle_name' => $this->input->post('middle_name'),
                'last_name_surname' => $this->input->post('last_name'),
                'nationality' => $this->input->post('nationality'),
                'date_of_birth' => $this->input->post('date_of_birth'),
                'place_of_birth' => $this->input->post('place_of_birth'),
                'email' => $this->input->post('email')
            );
        }
        $this->m_hits->update_data_cv($id,$data);

        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data berhasil disimpan</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('my_profile');
    }
}
