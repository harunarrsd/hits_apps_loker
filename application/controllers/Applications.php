<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Applications extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('m_hits');
        if ($this->session->userdata('udhmasuk')==false) {
            redirect('home');
        }        
    }

    public function index() {
        $data['title'] = 'Seaferer Management System';
        if($this->session->userdata('role')==1){
            $position = NULL;
            $tov = NULL;
            if($this->input->get('filter_rank')) {
                $position = $this->input->get('filter_rank');
            }
            if($this->input->get('filter_tov')) {
                $tov = $this->input->get('filter_tov');
            }
            $data['position'] = $position;
            $data['tov'] = $tov;
            $data['sql'] = $this->m_hits->read_history_applications($position, $tov);
            $data['sql2'] = $this->m_hits->read_principal();
            $data['sql3'] = $this->m_hits->read_vessel();
            $data['sql4'] = $this->m_hits->read_pool();
            $data['sql5'] = $this->m_hits->read_type_of_vessel();
            $data['sql6'] = $this->m_hits->read_position();

            $data['header'] = $this->load->view('layout/header','',true);
            $data['sidebar'] = $this->load->view('layout/sidebar','',true);
            $data['pages'] = $this->load->view('pages/applications/index',array('main'=>$data),true);
            $this->load->view('master',array('main'=>$data));
        }else{
            $data['header'] = $this->load->view('layout/header','',true);
            $data['sidebar'] = $this->load->view('layout/sidebar_user','',true);
            $data['pages'] = $this->load->view('pages/user/applications/index',array('main'=>$data),true);
            $this->load->view('master',array('main'=>$data));
        }
    }

    public function daftar($id) {
        $data['title'] = 'Seaferer Management System';
        $data['sql2'] = $this->db->query("SELECT * FROM type_of_vessel WHERE id_tov = '".$id."'");
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar_user','',true);
        $data['pages'] = $this->load->view('pages/user/applications/daftar',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    function create_history_applications(){
        $id = $this->input->post('id');
        $data = array(
            'id_applicant' => $id,
            'id_tov' => $this->input->post('id_tov'),
            'range_salary_start' => $this->input->post('salary_start'),
            'range_salary_end' => $this->input->post('salary_end'),
            'status_applicant' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'created_by' => $id
        );
        $this->m_hits->create_history_applications($data);

        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data berhasil dikirim.</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications');
    }

    function update_data_cv(){
        $id = $this->input->post('id');
        $config = array(
            'upload_path'=>'upload/photo_user',
            'allowed_types'=>'jpg|png|jpeg',
            'max_size'=>2086
        );
        $this->load->library('upload',$config);
        if($this->upload->do_upload('photo')){
            $finfo = $this->upload->data();
            $photo = $finfo['file_name'];

            $data = array(
                'first_name' => $this->input->post('first_name'),
                'middle_name' => $this->input->post('middle_name'),
                'last_name_surname' => $this->input->post('last_name'),
                'nationality' => $this->input->post('nationality'),
                'date_of_birth' => $this->input->post('date_of_birth'),
                'place_of_birth' => $this->input->post('place_of_birth'),
                'id_position' => $this->input->post('id_position'),
                'accept_lower_rank' => $this->input->post('accept_lower_rank'),
                'available_from' => $this->input->post('available_from'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'phone' => $this->input->post('phone'),
                'name_next_of_kin' => $this->input->post('name_next_of_kin'),
                'relationship_next_of_kin' => $this->input->post('relationship_next_of_kin'),
                'phone_next_of_kin' => $this->input->post('phone_next_of_kin'),
                'photo' => $photo
            );
            $kode_id = array('id_applications'=>$id);
            $photo_db = $this->db->get_where('applications',$kode_id);
            if($photo_db->num_rows()>0){
                $pros=$photo_db->row();
                $name_photo=$pros->photo;

                if(file_exists($lok=FCPATH.'upload/photo_user/'.$name_photo)){
                    unlink($lok);
                }
            }
        }else{
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'middle_name' => $this->input->post('middle_name'),
                'last_name_surname' => $this->input->post('last_name'),
                'nationality' => $this->input->post('nationality'),
                'date_of_birth' => $this->input->post('date_of_birth'),
                'place_of_birth' => $this->input->post('place_of_birth'),
                'id_position' => $this->input->post('id_position'),
                'accept_lower_rank' => $this->input->post('accept_lower_rank'),
                'available_from' => $this->input->post('available_from'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'phone' => $this->input->post('phone'),
                'name_next_of_kin' => $this->input->post('name_next_of_kin'),
                'relationship_next_of_kin' => $this->input->post('relationship_next_of_kin'),
                'phone_next_of_kin' => $this->input->post('phone_next_of_kin'),
            );
        }
        $this->m_hits->update_data_cv($id,$data);

        // documents
        $a = $this->input->post('name_documents');
        $b = $this->input->post('number_documents');
        $c = $this->input->post('date_of_issue');
        $d = $this->input->post('date_of_expire');
        if($a=='' AND $b=='' AND $c=='' AND $d==''){

        }else{
            $name_documents = $_POST['name_documents'];
            $number_documents = $_POST['number_documents'];
            $date_of_issue = $_POST['date_of_issue'];
            $date_of_expire = $_POST['date_of_expire'];
            $data_documents = array();
            $index = 0;
            foreach($name_documents as $datadocuments){
                array_push($data_documents, array(
                    'name_documents' => $datadocuments,
                    'number_documents' => $number_documents[$index],
                    'date_of_issue' => $date_of_issue[$index],
                    'date_of_expire' => $date_of_expire[$index],
                    'id_applications' => $id,
                ));
                $index++;
            }
            $this->m_hits->create_documents($data_documents);
        }

        // license
        $e = $this->input->post('name_license');
        $f = $this->input->post('grade_of_license');
        $g = $this->input->post('number_license');
        $h = $this->input->post('place_of_issue_license');
        $i = $this->input->post('date_of_issue_license');
        $j = $this->input->post('date_of_expire_license');
        if($e=='' AND $f=='' AND $g=='' AND $h=='' AND $i=='' AND $j==''){

        }else{
            $name_license = $_POST['name_license'];
            $grade_of_license = $_POST['grade_of_license'];
            $number_license = $_POST['number_license'];
            $place_of_issue_license = $_POST['place_of_issue_license'];
            $date_of_issue_license = $_POST['date_of_issue_license'];
            $date_of_expire_license = $_POST['date_of_expire_license'];
            $data_license = array();
            $index = 0;
            foreach($name_license as $datalicense){
                array_push($data_license, array(
                    'name_license' => $datalicense,
                    'grade_of_license' => $grade_of_license[$index],
                    'number_license' => $number_license[$index],
                    'place_of_issue_license' => $place_of_issue_license[$index],
                    'date_of_issue_license' => $date_of_issue_license[$index],
                    'date_of_expire_license' => $date_of_expire_license[$index],
                    'id_applications' => $id,
                ));
                $index++;
            }
            $this->m_hits->create_license($data_license);
        }
        $id_t = $this->input->post('id_tov');

        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications/daftar/'.$id_t);
    }

    public function delete_documents($id){
        $this->m_hits->delete_documents($id);
        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data documents dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications/daftar');
    }

    public function delete_license($id){
        $this->m_hits->delete_license($id);
        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data license dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications/daftar');
    }

    public function wait() {
        $position = NULL;
        $tov = NULL;
        if($this->input->get('filter_rank')) {
            $position = $this->input->get('filter_rank');
        }
        if($this->input->get('filter_tov')) {
            $tov = $this->input->get('filter_tov');
        }
        $data['position'] = $position;
        $data['tov'] = $tov;
        $data['title'] = 'Seaferer Management System';
        $data['sql'] = $this->m_hits->read_applications_waiting($position, $tov);
        $data['sql2'] = $this->m_hits->read_principal();
        $data['sql3'] = $this->m_hits->read_vessel();
        $data['sql4'] = $this->m_hits->read_pool();
        $data['sql5'] = $this->m_hits->read_type_of_vessel();
        $data['sql6'] = $this->m_hits->read_position();

        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/applications/waiting',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function trash() {
        $position = NULL;
        $tov = NULL;
        if($this->input->get('filter_rank')) {
            $position = $this->input->get('filter_rank');
        }
        if($this->input->get('filter_tov')) {
            $tov = $this->input->get('filter_tov');
        }
        $data['position'] = $position;
        $data['tov'] = $tov;
        $data['title'] = 'Seaferer Management System';
        $data['sql'] = $this->m_hits->read_applications_trash($position, $tov);
        $data['sql5'] = $this->m_hits->read_type_of_vessel();
        $data['sql6'] = $this->m_hits->read_position();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/applications/trash',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function settings() {
        $data['title'] = 'Seaferer Management System';
        $data['pages'] = $this->load->view('pages/applications/settings',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function delete_applications($id){
        $this->m_hits->delete_applications($id);
        echo "<script type='text/javascript'>alert ('Sukses!');</script>";
        redirect('applications','refresh');
    }

    function update(){
        $id = $this->input->post('id');
        $file_wawancara = NULL;
        $filename = date("Y-m-d His")."_".$id;
        $config = array(
            'upload_path'=>'upload/file_wawancara/',
            'allowed_types'=>'pdf',
            'file_name'=>$filename
        );            
        $this->upload->initialize($config);
        if ($this->upload->do_upload('file_wawancara')) {
            $finfo = $this->upload->data();
            $file_wawancara = $finfo['file_name'];
        }

        $data = array(
            'status_applicant'    => $this->input->post('status'),
            'file_wawancara'	=> $file_wawancara,
            'updated_at' => date("Y-m-d H:i:s"),
            'updated_by' => $this->session->userdata('id')
        );
        $this->m_hits->update_history_applications($id,$data);
        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications');
    }

    function update_wait(){
        $id = $this->input->post('id');
        $data = array(
            'status_applicant'	=> $this->input->post('status'),
            'updated_at' => date("Y-m-d H:i:s"),
            'updated_by' => $this->session->userdata('id')
        );
        $this->m_hits->update_history_applications($id,$data);

        if($this->input->post('status') == '4') {
            $crew = array(
                'status_crew' => 1,
                'crew_join_date' =>  $this->input->post('join_date'),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->session->userdata('id')
            );
            $this->m_hits->update_data_cv($this->input->post('id_user'),$crew);

            $start_date = DateTime::createFromFormat('Y-m-d', $this->input->post('start_date'));
            if($this->input->post('contract_type') == 'Day(s)') {
                $start_date->add(new DateInterval('P'.$this->input->post('contract_length').'D'));
            } else {
                $start_date->add(new DateInterval('P'.$this->input->post('contract_length').'M'));
            }
            $replacement = array(
                'id_crew'=> $this->input->post('id_user'),
                'id_company'=> $this->input->post('id_company'),
                'id_vessel' => $this->input->post('id_vessel'),
                'id_pool' => $this->input->post('id_pool'),
                'salary' => $this->input->post('salary'),
                'contract_length' => $this->input->post('contract_length'),
                'contract_type' => $this->input->post('contract_type'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $start_date->format('Y-m-d'),
                'active' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => $this->session->userdata('id')
            );
            $this->m_hits->create_replacement($replacement);
        }
        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications/wait');
    }

    function update_trash(){
        $id = $this->input->post('id');
        $data = array(
            'status_applicant'  => $this->input->post('status'),
            'updated_at' => date("Y-m-d H:i:s"),
            'updated_by' => $this->session->userdata('id')
        );
        $this->m_hits->update_history_applications($id,$data);
        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications/trash');
    }

    function upload_surat_kesehatan() {
        $image=basename($_FILES['foto_surat_kesehatan']['name']);
        $image=str_replace(' ','|',$image);
        $type = explode(".",$image);
        $type = $type[count($type)-1];
        if (in_array($type,array('jpg','jpeg','png','gif','PNG')))
        {
            $tmppath="./upload/surat_kesehatan/".$image;
            if(is_uploaded_file($_FILES["foto_surat_kesehatan"]["tmp_name"]))
            {
                move_uploaded_file($_FILES['foto_surat_kesehatan']['tmp_name'],$tmppath);
                return $tmppath;
            }
        }
        else
        {
            echo "<script type='text/javascript'>alert ('Maaf format gambar tidak mendukung!');document.location='../applications/wait';</script>";
        }
    }

    function upload_surat_kck() {
        $image=basename($_FILES['foto_surat_kck']['name']);
        $image=str_replace(' ','|',$image);
        $type = explode(".",$image);
        $type = $type[count($type)-1];
        if (in_array($type,array('jpg','jpeg','png','gif','PNG')))
        {
            $tmppath="./upload/surat_kck/".$image;
            if(is_uploaded_file($_FILES["foto_surat_kck"]["tmp_name"]))
            {
                move_uploaded_file($_FILES['foto_surat_kck']['tmp_name'],$tmppath);
                return $tmppath;
            }
        }
        else
        {
            echo "<script type='text/javascript'>alert ('Maaf format gambar tidak mendukung!');document.location='../applications/wait';</script>";
        }
    }

    function upload_sertifikat() {
        $image=basename($_FILES['foto_sertifikat']['name']);
        $image=str_replace(' ','|',$image);
        $type = explode(".",$image);
        $type = $type[count($type)-1];
        if (in_array($type,array('jpg','jpeg','png','gif','PNG')))
        {
            $tmppath="./upload/sertifikat/".$image;
            if(is_uploaded_file($_FILES["foto_sertifikat"]["tmp_name"]))
            {
                move_uploaded_file($_FILES['foto_sertifikat']['tmp_name'],$tmppath);
                return $tmppath;
            }
        }
        else
        {
            echo "<script type='text/javascript'>alert ('Maaf format gambar tidak mendukung!');document.location='../applications/wait';</script>";
        }
    }

    function create_berkas(){
        $data = array(
            'id_applications' => $this->input->post('id'),
            'foto_surat_kesehatan'	=> $this->upload_surat_kesehatan(),
            'foto_surat_kck'	=> $this->upload_surat_kck()
        );
        $this->m_hits->create_berkas($data);
        $this->session->set_flashdata('notif_berkas','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Upload Surat Kesehatan dan SKCK Berhasil</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications/wait');
    }

    function create_berkas_sertifikat(){
        $id = $this->input->post('id');
        $ser = $this->input->post('nama_sertifikat');
        $sertif = implode(",",$ser);
        $data = array(
            'id_applications' => $id,
            'nama_sertifikat' => $sertif
        );
        $this->m_hits->create_berkas_sertifikat($data);
        $this->session->set_flashdata('notif_berkas_sertifikat','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Serifikat Berhasil</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications/wait');
    }

    function create_applications_approved(){
        $id = $this->input->post('id');
        $data = array(
            'id_applications' => $id,
            'id_vessel' => $this->input->post('id_vessel')
        );
        $this->m_hits->create_applications_approved($data);
        $this->session->set_flashdata('notif_approved','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('applications/wait');
    }

    public function data_cv($id){
        $data['read_position'] = $this->m_hits->read_position();
        $data['read_kabupaten_kota'] = $this->m_hits->read_kabupaten_kota();
        $data['read_provinsi'] = $this->m_hits->read_provinsi();
        $data['read_sertifikat'] = $this->m_hits->read_sertifikat();
        $data['read_documents_user'] = $this->m_hits->read_documents_user();
        $data['read_license_user'] = $this->m_hits->read_license_user();
        $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
        $data['read_engine'] = $this->m_hits->read_engine();
        $data['read_negara'] = $this->m_hits->read_negara();
        $data['sql'] = $this->m_hits->get_applicant($id);
        $data['title'] = 'Seafarer Management System';
        // $this->load->view('pages/applications/data_cv',array('main'=>$data));
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/applications/data_cv',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function filter() {
        $data['filter'] = 'new';
        $data['title'] = 'Seaferer Management System';
        $data['sql'] = $this->m_hits->read_history_applications();
        $data['sql2'] = $this->m_hits->read_principal();
        $data['sql3'] = $this->m_hits->read_vessel();
        $data['sql4'] = $this->m_hits->read_pool();
        $data['sql5'] = $this->m_hits->read_type_of_vessel();
        $data['sql6'] = $this->m_hits->read_position();
        $filter_rank = $this->input->post('filter_rank');
        $data['filter_rank'] = $filter_rank;
        $filter_tov = $this->input->post('filter_tov');
        $data['filter_tov'] = $filter_tov;
        $data['filter_new'] = $this->db->query("SELECT * FROM history_applications a JOIN position using(id_position) JOIN type_of_vessel using(id_tov) WHERE a.id_position LIKE '".$filter_rank."' OR a.id_tov LIKE '".$filter_tov."' AND a.status=1 OR a.status=2");
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/applications/filter',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function filter_wait() {
        $data['filter'] = 'wait';
        $data['title'] = 'Seaferer Management System';
        $data['sql'] = $this->m_hits->read_history_applications();
        $data['sql2'] = $this->m_hits->read_principal();
        $data['sql3'] = $this->m_hits->read_vessel();
        $data['sql4'] = $this->m_hits->read_pool();
        $data['sql5'] = $this->m_hits->read_type_of_vessel();
        $data['sql6'] = $this->m_hits->read_position();
        $filter_rank = $this->input->post('filter_rank');
        $data['filter_rank'] = $filter_rank;
        $filter_tov = $this->input->post('filter_tov');
        $data['filter_tov'] = $filter_tov;
        $data['filter_wait'] = $this->db->query("SELECT * FROM history_applications a JOIN position using(id_position) JOIN type_of_vessel using(id_tov) WHERE a.id_position LIKE '".$filter_rank."' OR a.id_tov LIKE '".$filter_tov."' AND a.status=3");
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/applications/filter',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function filter_trash() {
        $data['filter'] = 'trash';
        $data['title'] = 'Seaferer Management System';
        $data['sql'] = $this->m_hits->read_history_applications();
        $data['sql2'] = $this->m_hits->read_principal();
        $data['sql3'] = $this->m_hits->read_vessel();
        $data['sql4'] = $this->m_hits->read_pool();
        $data['sql5'] = $this->m_hits->read_type_of_vessel();
        $data['sql6'] = $this->m_hits->read_position();
        $filter_rank = $this->input->post('filter_rank');
        $data['filter_rank'] = $filter_rank;
        $filter_tov = $this->input->post('filter_tov');
        $data['filter_tov'] = $filter_tov;
        $data['filter_trash'] = $this->db->query("SELECT * FROM history_applications a JOIN position using(id_position) JOIN type_of_vessel using(id_tov) WHERE a.id_position LIKE '".$filter_rank."' OR a.id_tov LIKE '".$filter_tov."' AND a.status=5");
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/applications/filter',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }
}
