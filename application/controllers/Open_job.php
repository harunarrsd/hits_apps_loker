<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Open_job extends CI_Controller {

    function __construct()
	{
        parent::__construct();
		$this->load->model('m_hits');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('home');
		}        
    }
    
    public function index()
	{
        $data['title'] = 'Seafarer Management System';
        $data['sql'] = $this->m_hits->read_open_job();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/open_job/history',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function add()
	{
        $data['title'] = 'Seafarer Management System';
        $data['op'] = 'add';
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/open_job/history_edit',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    // function create_open_job(){
    //     $op = $this->input->post('op');
    //     $id = $this->input->post('id');
    //     $data = array(
    //         'name_tov' => $this->input->post('name_tov')
    //     );
    //     if($op=='add'){
    //             $this->m_hits->create_open_job($data);
    //             $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
    //             redirect('open_job/add');
    //     }else{
    //             $this->m_hits->update_open_job($id,$data);
    //             $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
    //             redirect('open_job');
    //     }
    // }

    // function create_open_job2(){
    //     // $op = $this->input->post('op');
    //     $id = $this->input->post('id');
    //     $data = array(
    //         'id_tov' => $id,
    //         'start_date' => $this->input->post('start_date'),
    //         'end_date' => $this->input->post('end_date'),
    //         'status_oj' => $this->input->post('status_oj')
    //     );
    //     // if($op=='add'){
    //             $this->m_hits->create_open_job2($data);
    //             $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
    //             redirect('open_job');
    //     // }
    //     // else{
    //     //         $this->m_hits->update_open_job2($id,$data);
    //     //         $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
    //     //         redirect('open_job');
    //     // }
    // }

    public function edit_open_job($id)
	{
		$data['title'] = 'Seafarer Management System';
		$data['op'] = 'edit';
        $data['sql'] = $this->m_hits->edit_open_job($id);
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
		$data['pages'] = $this->load->view('pages/open_job/history_edit',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
    }

    // public function open_job($id)
	// {
	// 	$data['title'] = 'Seafarer Management System';
	// 	$data['op'] = 'edit';
    //     $data['sql'] = $this->m_hits->edit_open_job($id);
    //     $data['header'] = $this->load->view('layout/header','',true);
    //     $data['sidebar'] = $this->load->view('layout/sidebar','',true);
	// 	$data['pages'] = $this->load->view('pages/open_job/open',array('main'=>$data),true);
	// 	$this->load->view('master',array('main'=>$data));
    // }
        
    public function delete_open_job($id){
		$this->m_hits->delete_open_job($id);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		redirect('open_job');
    }
    
    // public function history()
	// {
    //     $data['title'] = 'Seafarer Management System';
    //     // $data['op'] = 'add';
    //     $data['header'] = $this->load->view('layout/header','',true);
    //     $data['sidebar'] = $this->load->view('layout/sidebar','',true);
    //     $data['pages'] = $this->load->view('pages/open_job/history',array('main'=>$data),true);
    //     $this->load->view('master',array('main'=>$data));
    // }

    // public function edit_open_job2($id)
	// {
	// 	$data['title'] = 'Seafarer Management System';
	// 	// $data['op'] = 'edit';
    //     $data['sql'] = $this->db->query("SELECT * FROM open_job JOIN type_of_vessel using(id_tov) WHERE id_oj = '".$id."'");
    //     $data['header'] = $this->load->view('layout/header','',true);
    //     $data['sidebar'] = $this->load->view('layout/sidebar','',true);
	// 	$data['pages'] = $this->load->view('pages/open_job/history_edit',array('main'=>$data),true);
	// 	$this->load->view('master',array('main'=>$data));
    // }

    function update_open_job2(){
        $op = $this->input->post('op');
        $id = $this->input->post('id');
        if($op=='add'){
            $data = array(
                'id_tov' => $this->input->post('id_tov'),
                'start_date' => date("Y-m-d"),
                'end_date' => $this->input->post('end_date'),
                'status_oj' => 1,
                'information' => $this->input->post('information')
            );
            $this->m_hits->create_open_job2($data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('open_job/add');
        }else{
            $data = array(
                'end_date' => $this->input->post('end_date'),
                'information' => $this->input->post('information')
            );
            $this->m_hits->update_open_job2($id,$data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('open_job');
        }
    }

    function update_open_job3($id){
            $data = array(
                'status_oj' => 1,
            );
            $this->m_hits->update_open_job2($id,$data);
            $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('open_job');
    }

    function update_open_job4($id){
        $data = array(
            'status_oj' => 0,
        );
        $this->m_hits->update_open_job2($id,$data);
        $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('open_job');
}
}
