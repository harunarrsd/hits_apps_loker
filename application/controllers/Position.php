<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position extends CI_Controller {

        function __construct()
	{
        parent::__construct();
		$this->load->model('m_hits');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('home');
		}        
	}

	public function index()
	{
        $data['title'] = 'Seaferer Management System';
        $data['sql'] = $this->m_hits->read_position();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/position/index',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }
    
    public function add()
	{
        $data['title'] = 'Seaferer Management System';
        $data['op'] = 'add';
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/position/add',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
        }
        
        function create_position(){
                $op = $this->input->post('op');
                $id = $this->input->post('id');
		$data = array(
            'name_position' => $this->input->post('nama_posisi'),
			'department' => $this->input->post('department')
        );
                if($op=='add'){
                        $this->m_hits->create_position($data);
                        $this->session->set_flashdata('notif_add','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        redirect('position/add');
                }else{
                        $this->m_hits->update_position($id,$data);
                        $this->session->set_flashdata('notif_edit','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        redirect('position');
                }
        }
        
        public function edit_position($id)
	{
		$data['title'] = 'Seafarer Management System';
		$data['op'] = 'edit';
                $data['sql'] = $this->m_hits->edit_position($id);
                $data['header'] = $this->load->view('layout/header','',true);
                $data['sidebar'] = $this->load->view('layout/sidebar','',true);
		$data['pages'] = $this->load->view('pages/position/add',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
        }
        
        public function delete_position($id){
		$this->m_hits->delete_position($id);
		$this->session->set_flashdata('notif_delete','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect('position');
        }
}
