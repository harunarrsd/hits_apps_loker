<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pool extends CI_Controller {

        function __construct()
	{
        parent::__construct();
		$this->load->model('m_hits');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('home');
		}        
	}

        public function index()
	{
                $data['title'] = 'Seafarer Management System';
                $data['sql'] = $this->m_hits->read_pool();
                $data['read_vessel'] = $this->m_hits->read_vessel2();
                $data['header'] = $this->load->view('layout/header','',true);
                $data['sidebar'] = $this->load->view('layout/sidebar','',true);
                $data['pages'] = $this->load->view('pages/pool/index',array('main'=>$data),true);
                $this->load->view('master',array('main'=>$data));
        }
    
        public function add()
	{
                $data['title'] = 'Seafarer Management System';
                $data['op'] = 'add';
                $data['read_vessel'] = $this->m_hits->read_vessel2();
                $data['header'] = $this->load->view('layout/header','',true);
                $data['sidebar'] = $this->load->view('layout/sidebar','',true);
                $data['pages'] = $this->load->view('pages/pool/add',array('main'=>$data),true);
                $this->load->view('master',array('main'=>$data));
        }

        function create_pool(){
                $op = $this->input->post('op');
                $id = $this->input->post('id');
                $vessel = $this->input->post('vessel');
		$data = array(
			'name_pool' => $this->input->post('nama_pool'), 
			'deskripsi_pool' => $this->input->post('deskripsi_pool')
                );
                if($op=='add'){
                        $this->m_hits->create_pool($data);

                        $id_pool = $this->db->insert_id();
                        foreach($vessel as $row){
                                $data = array(
                                                'id_pool' => $id_pool,
                                                'id_vessel' => $row
                                        );
                                $this->db->insert('pool_vessel',$data);
                        }
                        $this->session->set_flashdata('notif_add','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        redirect('pool/add');
                }else{
                        $this->m_hits->update_pool($id,$data);
                        $this->session->set_flashdata('notif_edit','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        redirect('pool');
                }
        }
        
        public function edit_pool($id)
	{
		$data['title'] = 'Seafarer Management System';
                $data['op'] = 'edit';
                $data['read_vessel'] = $this->m_hits->read_vessel2();
                $data['sql'] = $this->m_hits->edit_pool($id);
                // $data['sql'] = $this->db->query('SELECT * FROM pool_vessel b JOIN pool a USING(id_pool) JOIN vessel c USING(id_vessel) WHERE a.id_pool = "'.$id.'"');
                $data['header'] = $this->load->view('layout/header','',true);
                $data['sidebar'] = $this->load->view('layout/sidebar','',true);
		$data['pages'] = $this->load->view('pages/pool/add',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
        }
        
        public function delete_pool($id){
		$this->m_hits->delete_pool($id);
		$this->session->set_flashdata('notif_delete','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		redirect('pool');
	}
}
