<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type_of_vessel extends CI_Controller {

    function __construct()
	{
        parent::__construct();
		$this->load->model('m_hits');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('home');
		}        
    }
    
    public function index()
	{
        $data['title'] = 'Seafarer Management System';
        $data['sql'] = $this->m_hits->read_open_job();
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/type_of_vessel/index',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    public function add()
	{
        $data['title'] = 'Seafarer Management System';
        $data['op'] = 'add';
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
        $data['pages'] = $this->load->view('pages/type_of_vessel/add',array('main'=>$data),true);
        $this->load->view('master',array('main'=>$data));
    }

    function create_open_job(){
        $op = $this->input->post('op');
        $id = $this->input->post('id');
        $data = array(
            'name_tov' => $this->input->post('name_tov')
        );
        if($op=='add'){
                $this->m_hits->create_open_job($data);
                $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect('type_of_vessel/add');
        }else{
                $this->m_hits->update_open_job($id,$data);
                $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                redirect('type_of_vessel');
        }
    }

    public function edit_open_job($id)
	{
		$data['title'] = 'Seafarer Management System';
		$data['op'] = 'edit';
        $data['sql'] = $this->m_hits->edit_open_job($id);
        $data['header'] = $this->load->view('layout/header','',true);
        $data['sidebar'] = $this->load->view('layout/sidebar','',true);
		$data['pages'] = $this->load->view('pages/type_of_vessel/add',array('main'=>$data),true);
		$this->load->view('master',array('main'=>$data));
    }

    
        
    public function delete_open_job($id){
		$this->m_hits->delete_open_job($id);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		redirect('type_of_vessel');
    }
}
