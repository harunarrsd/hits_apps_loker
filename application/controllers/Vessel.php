<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vessel extends CI_Controller {

   function __construct()
   {
      parent::__construct();
      $this->load->model('m_hits');
      if ($this->session->userdata('udhmasuk')==false) {
         redirect('home');
      }        
   }

   public function index()
   {
      $data['title'] = 'Seaferer Management System';
      $data['sql'] = $this->m_hits->read_vessel();
      $data['header'] = $this->load->view('layout/header','',true);
      $data['sidebar'] = $this->load->view('layout/sidebar','',true);
      $data['pages'] = $this->load->view('pages/vessel/index',array('main'=>$data),true);
      $this->load->view('master',array('main'=>$data));
   }

   public function add()
   {
      $data['title'] = 'Seaferer Management System';
      $data['op'] = 'add';
      $data['sql2'] = $this->m_hits->read_principal();
      $data['sql3'] = $this->m_hits->read_pool();
      $data['read_engine'] = $this->m_hits->read_engine();
      $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
      $data['read_principal'] = $this->m_hits->read_principal();
      $data['read_negara'] = $this->m_hits->read_negara();
      $data['header'] = $this->load->view('layout/header','',true);
      $data['sidebar'] = $this->load->view('layout/sidebar','',true);
      $data['pages'] = $this->load->view('pages/vessel/add',array('main'=>$data),true);
      $this->load->view('master',array('main'=>$data));
   }

   function create_vessel(){
      $op = $this->input->post('op');
      $id = $this->input->post('id');
      // $config = array(
      //         'upload_path'=>'upload/flag_vessel',
      //         'allowed_types'=>'jpg|png|jpeg',
      //         'max_size'=>2086
      // );
      // $this->load->library('upload',$config);
      // if($this->upload->do_upload('photo')){
      //         $finfo = $this->upload->data();
      //         $photo = $finfo['file_name'];

      $data = array(
         'name_vessel' => $this->input->post('name_vessel'), 
         'gt_vessel' => $this->input->post('gt_vessel'),
         'id_engine' => $this->input->post('engine_vessel'),
         'bhp_vessel' => $this->input->post('bhp_vessel'),
         'flag_vessel' => $this->input->post('flag_vessel'),
         'id_tov' => $this->input->post('id_tov'),
         'id_company' => $this->input->post('id_company'),
         'dwt_vessel' => $this->input->post('dwt_vessel'),
      );
      // $kode_id = array('id_vessel'=>$id);
      // $photo_db = $this->db->get_where('vessel',$kode_id);
      // if($photo_db->num_rows()>0){
      //         $pros=$photo_db->row();
      //         $name_photo=$pros->flag_vessel;

      //         if(file_exists($lok=FCPATH.'upload/flag_vessel/'.$name_photo)){
      //                 unlink($lok);
      //         }
      // }
      // }else{
      // $data = array(
      //         'name_vessel' => $this->input->post('name_vessel'), 
      //         'gt_vessel' => $this->input->post('gt_vessel'),
      //         'id_engine' => $this->input->post('engine_vessel'),
      //         'bhp_vessel' => $this->input->post('bhp_vessel')
      // );
      // }
      if($op=='add'){
         $this->m_hits->create_vessel($data);
         $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil ditambah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
         redirect('vessel/add');
      }else{
         $this->m_hits->update_vessel($id,$data);
         $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil diubah</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
         redirect('vessel');
      }
   }

   public function edit_vessel($id)
   {
      $data['title'] = 'Seafarer Management System';
      $data['op'] = 'edit';
      $data['sql'] = $this->m_hits->edit_vessel($id);
      $data['sql2'] = $this->m_hits->read_principal();
      $data['sql3'] = $this->m_hits->read_pool();
      $data['read_engine'] = $this->m_hits->read_engine();
      $data['read_type_of_vessel'] = $this->m_hits->read_type_of_vessel();
      $data['read_principal'] = $this->m_hits->read_principal();
      $data['read_negara'] = $this->m_hits->read_negara();
      $data['header'] = $this->load->view('layout/header','',true);
      $data['sidebar'] = $this->load->view('layout/sidebar','',true);
      $data['pages'] = $this->load->view('pages/vessel/add',array('main'=>$data),true);
      $this->load->view('master',array('main'=>$data));
   }

   public function delete_vessel($id){
      $this->m_hits->delete_vessel($id);
      $this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Data Berhasil dihapus</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
      redirect('vessel');
   }
}
