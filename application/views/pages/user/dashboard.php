<?php
    $sql = $this->db->query("SELECT * FROM open_job");
    $status_oj = 0;
    foreach($sql->result() as $obj){
        $status_oj = $obj->status_oj;
    }

    $sql = $this->db->query("SELECT * FROM users where id_user = '".$this->session->userdata('id')."'");
    $userstatus = 0;
    foreach($sql->result() as $obj) {
        $userstatus = $obj->status_crew;
    }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto"></div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="container">
                                <h3 class="m-subheader__title">Selamat Datang, <?php echo $this->session->userdata('email');?></h3>
                                <h6>Sub Description Goes Here</h6>
                                <p>Harap Melengkapi Data CV. <a href="<?php echo site_url('data_cv');?>" style="text-decoration: underline;">Klik</a></p>

                                <?php if(!$userstatus) { 
                                    if($status_oj == 1) { ?>
                                        <p>Pendaftaran lowongan kerja sudah dibuka. <a href="<?php echo site_url('applications');?>" style="text-decoration: underline;">Klik</a></p>
                                    <?php } else { ?>
                                        <p>Pendaftaran lowongan kerja masih ditutup.</p>
                                    <?php }
                                } ?>
                                
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt minus eius consequuntur dignissimos, aut, libero provident minima fugit porro nam eligendi officia non exercitationem similique explicabo dicta, cupiditate aspernatur at.</p>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt minus eius consequuntur dignissimos, aut, libero provident minima fugit porro nam eligendi officia non exercitationem similique explicabo dicta, cupiditate aspernatur at.</p>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt minus eius consequuntur dignissimos, aut, libero provident minima fugit porro nam eligendi officia non exercitationem similique explicabo dicta, cupiditate aspernatur at.</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>