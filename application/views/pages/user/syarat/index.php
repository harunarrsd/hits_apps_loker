<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Syarat</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif_add');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="container">
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Qui minus natus, odio perferendis deserunt illo sit numquam rem iure aspernatur ullam architecto cumque ipsam quas. Itaque harum asperiores debitis aliquid?</p>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Qui minus natus, odio perferendis deserunt illo sit numquam rem iure aspernatur ullam architecto cumque ipsam quas. Itaque harum asperiores debitis aliquid?</p>
                                <ol>
                                    <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam, cum provident quo quibusdam non modi officiis veritatis laborum reprehenderit eveniet! Modi quos vero nemo ex beatae, placeat expedita harum voluptates.</li>
                                    <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam, cum provident quo quibusdam non modi officiis veritatis laborum reprehenderit eveniet! Modi quos vero nemo ex beatae, placeat expedita harum voluptates.</li>
                                    <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam, cum provident quo quibusdam non modi officiis veritatis laborum reprehenderit eveniet! Modi quos vero nemo ex beatae, placeat expedita harum voluptates.</li>
                                    <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam, cum provident quo quibusdam non modi officiis veritatis laborum reprehenderit eveniet! Modi quos vero nemo ex beatae, placeat expedita harum voluptates.</li>
                                    <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam, cum provident quo quibusdam non modi officiis veritatis laborum reprehenderit eveniet! Modi quos vero nemo ex beatae, placeat expedita harum voluptates.</li>
                                </ol>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>