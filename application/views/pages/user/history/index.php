<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">History Applications</h3>
                <h6>Keterangan tambahan dapat diletakan disini</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>Nationality</th>
                            <th>City</th>
                            <th>Type Of Vessel</th>
                            <th>Range Salary</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $no = 0;
                        $sql = $this->db->query("SELECT * FROM history_applications LEFT JOIN users ON users.id_user = history_applications.id_applicant LEFT JOIN type_of_vessel using(id_tov) WHERE id_applicant = '".$this->session->userdata('id')."' ORDER BY id_history DESC");
                        foreach($sql->result() as $obj){
                            $no++;
                    ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?php echo $obj->first_name?> <?php echo $obj->middle_name?> <?php echo $obj->last_name_surname?></td>
                        <td><?php echo $obj->email?></td>
                        <td><?php echo $obj->nationality?></td>
                        <td><?php echo $obj->city?></td>
                        <td><?php echo $obj->name_tov?></td>
                        <td><?php echo $obj->range_salary_start?> - <?php echo $obj->range_salary_end?></td>
                        <?php if($obj->status_applicant=='1') { ?>
                            <td class="alert alert-warning text-center">Pending</td>
                        <?php } else if($obj->status_applicant=='2') { ?>
                            <td class="alert alert-success text-center">Verified</td>
                        <?php } else if($obj->status_applicant=='3') { ?>
                            <td class="alert alert-danger text-center">Waiting</td>
                        <?php } else if($obj->status_applicant=='4') { ?>
                            <td class="alert alert-success text-center">Accepted</td>
                        <?php } else { ?>
                            <td class="alert alert-danger text-center">Rejected</td>
                        <?php } ?>
                    </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>