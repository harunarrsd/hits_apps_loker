<?php
    $sql = $this->db->query("SELECT * FROM users where id_user = '".$this->session->userdata('id')."'");
    $useremail = "";
    foreach($sql->result() as $obj) {
        $useremail = $obj->email;
    }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Setting</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif_add');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <?php echo form_open_multipart('setting/update_password/');?>
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Email</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="email" value="<?= $useremail; ?>" type="text" placeholder="Email" disabled>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Old Password</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="old_password" value="" type="password" id="old_password" placeholder="Old Password" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">New Password</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="new_password" value="" type="password" id="new_password" placeholder="New Password" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Retype New Password</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="retype_new_password" value="" type="password" id="retype_new_password" placeholder="Retype New Password" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                </div>
                                <div class="col-10">
                                    <button type="reset" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>