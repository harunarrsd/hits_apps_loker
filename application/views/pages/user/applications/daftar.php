<?php
foreach ($main['sql2']->result() as $obj) { 
   $id_t = $obj->id_tov;
   $name_t = $obj->name_tov;
}
?>
<script src="<?php echo base_url("assets/js/jquery.min.js"); ?>" type="text/javascript"></script>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title">Applications / Daftar</h3>
            <h6>Sub Description Goes Here</h6>
         </div>
         <div class="m-loader--left">
            <a href="<?php echo site_url('applications');?>" class="btn btn-secondary btn-md"><i class="fa fa-arrow-left"></i> Back</a>
         </div>
      </div>
      <?php echo $this->session->flashdata('notif');?>
   </div>
   <!-- END: Subheader -->

   <!--begin::Portlet-->
   <div class="m-content">
      <div class="row">
         <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">
               <div class="m-portlet__body">
                  <div class="container">
                     <p><b>Harap Melengkapi Data CV terlebih dahulu. <a href="<?php echo site_url('data_cv');?>" style="text-decoration: underline;">Klik</a></b></p>
                     <?php echo form_open_multipart('applications/create_history_applications/');?>
                     <input type="hidden" value="<?php echo $this->session->userdata('id');?>" name="id">
                     <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card card-body">
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="row">
                                    <div class="col-md-4">
                                       <label>Type Of Vessel</label>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="hidden" name="id_tov" class="form-control" value="<?php echo $id_t?>">
                                       <input disabled type="text" class="form-control" value="<?php echo $name_t?>">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-7">
                                 <div class="row">
                                    <div class="col-md-3">
                                       <label>Range Salary</label>
                                    </div>
                                    <div class="col-md-4">
                                       <input type="text" class="form-control" placeholder="Ex.1000xxx" name="salary_start" required>
                                    </div>
                                    <div class="col-md-4">
                                       <input type="text" class="form-control" placeholder="Ex.3000xxx" name="salary_end" required>
                                    </div>
                                 </div>
                              </div>
                           </div><br>
                           <div class="row">
                              <div class="col-md-10"></div>
                              <div class="col-md-2" align="right">
                                 <button type="Submit" class="btn btn-primary">Daftar</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <!-- </form> -->
         </div>
      </div>
   </div>
</div>
<!--end::Portlet-->
</div>

<script>
   function previewImage() {
      document.getElementById("image-preview").style.display = "block";
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("image-source").files[0]);

      oFReader.onload = function(oFREvent) {
         document.getElementById("image-preview").src = oFREvent.target.result;
      };
   };
</script>