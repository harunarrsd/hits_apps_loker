<script src="<?php echo base_url("assets/js/jquery.min.js"); ?>" type="text/javascript"></script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Applications</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="container">
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Non dignissimos fugit repellat maxime omnis. Praesentium, saepe. Nihil ducimus quibusdam rem consectetur ea sapiente quos blanditiis aperiam fuga praesentium! Esse, rerum?</p>
                                <?php
                                    $sql = $this->db->query("SELECT * FROM open_job JOIN type_of_vessel using(id_tov) WHERE open_job.status_oj = 1");
                                    foreach($sql->result() as $sql2){
                                ?>
                                <div class="row">
                                    <div class="col-md-10">
                                        <p>Jenis Kapal : <?php echo $sql2->name_tov?></p>
                                        <p>Waktu : <b><?php echo date('d M Y', strtotime($sql2->start_date))?></b> Sampai <b><?php echo date('d M Y', strtotime($sql2->end_date))?></b></p>
                                        <p>Information : <?php echo $sql2->information;?></p>
                                    </div>
                                    <?php
                                        $sql3 = $this->db->query("SELECT * FROM history_applications WHERE id_applicant = '".$this->session->userdata('id')."' AND id_tov = '".$sql2->id_tov."'");
                                        // echo $sql3->num_rows();
                                        if($sql3->num_rows() > 0){
                                    ?>
                                    <div class="col-md-2">
                                        <a class="btn btn-danger btn-sm text-white" style="margin-left: auto;display: block;">Sudah Mendaftar</a>
                                    </div>
                                    <?php
                                        } else {
                                    ?>
                                    <div class="col-md-2">
                                        <a href="<?php echo site_url();?>applications/daftar/<?php echo $sql2->id_tov?>" class="btn btn-info btn-sm" style="margin-left: auto;display: block;">Daftar</a>
                                    </div>
                                    <?php
                                        }
                                    ?>
                                </div><br>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>