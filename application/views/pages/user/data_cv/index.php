<?php
    $sql = $this->db->query("SELECT * FROM users where id_user = '".$this->session->userdata('id')."'");
    foreach($sql->result() as $obj){
        $first_name = $obj->first_name;
        $middle_name = $obj->middle_name;
        $last_name = $obj->last_name_surname;
        $photo = $obj->photo;
        $nationality = $obj->nationality;
        $date_of_birth = $obj->date_of_birth;
        $tanggal = date('Y-m-d', strtotime($date_of_birth));
        $place_of_birth = $obj->place_of_birth;
        $id_position = $obj->id_position;
        $accept_lower_rank = $obj->accept_lower_rank;
        $available_from = $obj->available_from;
        $address = $obj->address;
        $province = $obj->province;
        $city = $obj->city;
        $phone = explode(",", $obj->phone);
        $phone_next_of_kin = $obj->phone_next_of_kin;
        $name_next_of_kin = $obj->name_next_of_kin;
        $relationship_next_of_kin = $obj->relationship_next_of_kin;
        $remark = $obj->remark;
    }
?>
<script src="<?php echo base_url("assets/js/jquery.min.js"); ?>" type="text/javascript"></script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Data CV</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
            <div class="m-loader--left">
                <a target="_blank" href="<?php echo site_url('data_cv/cetak');?>" target="_BLANK" class="btn btn-default btn-md"><i class="fa fa-print"></i> Print</a>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <?php echo form_open_multipart('data_cv/update_data_cv/');?>
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <center>
                                <h3>APPLICATION FOR EMPLOYMENT</h3>
                            </center>
                            <input type="hidden" value="<?php echo $this->session->userdata('id');?>" name="id">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9">
                                        <table border="1" width="100%">
                                            <thead align="center">
                                                <th colspan="3">Personnel Data</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>FIRST NAME
                                                        <input type="text" class="form-control text-center" name="first_name" placeholder="First Name" value="<?php echo $first_name;?>">
                                                    </td>
                                                    <td>MIDDLE NAME
                                                        <input type="text" class="form-control text-center" name="middle_name" placeholder="Middle Name" value="<?php echo $middle_name;?>">
                                                    </td>
                                                    <td>LAST NAME / SURNAME
                                                        <input type="text" class="form-control text-center" name="last_name" placeholder="Last Name / Surname" value="<?php echo $last_name;?>">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>NATIONALITY
                                                        <input type="text" class="form-control text-center" name="nationality" placeholder="Nationality" value="<?php echo $nationality;?>">
                                                    </td>
                                                    <td>DATE OF BIRTH (dd/mm/yyyy)
                                                        <input type="date" class="form-control text-center" name="date_of_birth" value="<?php if($date_of_birth=='') { ?><?php } else { echo $tanggal;?><?php } ?>">
                                                    </td>
                                                    <td>PLACE OF BIRTH
                                                        <input type="text" class="form-control text-center" name="place_of_birth" placeholder="Place Of Birth" value="<?php echo $place_of_birth;?>">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table><br>
                                        <table border="1" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                Post Applied For :
                                                            </div>
                                                            <div class="col-md-9">
                                                                <select name="id_position" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <?php
                                                                        foreach($main['read_position']->result() as $obj){
                                                                    ?>
                                                                    <option value="<?php echo $obj->id_position;?>" <?php if($id_position==$obj->id_position) echo 'selected' ?>>
                                                                        <?php echo $obj->name_position?>
                                                                    </option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Willing to accept lower rank :
                                                            </div>
                                                            <div class="col-md-6">
                                                                <select name="accept_lower_rank" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <option value="1" <?php if($accept_lower_rank=='1') echo 'selected';?>>Yes</option>
                                                                    <option value="0" <?php if($accept_lower_rank=='0') echo 'selected';?>>No</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                Available from :
                                                            </div>
                                                            <div class="col-md-7">
                                                                <input type="text" class="form-control" name="available_from" placeholder="Ex. Any Time, Next Month, 3rd April" value="<?php echo $available_from?>">
                                                            </div>
                                                        </div> 
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <img id="image-preview" src="<?php if($photo=='') { echo site_url('assets/img/dummy.png')?> <?php } else { echo site_url('upload/photo_user/'.$photo);?> <?php } ?>" alt="Image not found !" class="img-thumbnail">
                                        <input style="margin-top:5%;" id="image-source" onchange="previewImage();" type="file" name="photo">
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table border="1" width="100%">
                                            <thead align="center">
                                                <th colspan="3">Contact Address</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td valign="top">
                                                        Address :
                                                        <textarea rows="4" name="address" placeholder="Address" class="form-control"><?php echo $address?></textarea>
                                                    </td>
                                                    <td valign="top">
                                                        Province :
                                                        <select id="provinceopt" name="province" class="form-control" onchange="changeProvince()">
                                                            <option value="">Select</option>
                                                            <?php
                                                                foreach($main['read_provinsi']->result() as $obj){
                                                            ?>
                                                            <option value="<?php echo $obj->id;?>" <?php if($province==$obj->id) echo 'selected' ?>>
                                                                <?php echo ucwords(strtolower($obj->name)); ?>
                                                            </option>
                                                            <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </td>
                                                    <td valign="top">
                                                        City :
                                                        <select id="cityopt" name="city" class="form-control">
                                                            <option value="">Select</option>
                                                            <?php
                                                                foreach($main['read_kabupaten_kota']->result() as $obj){
                                                            ?>
                                                            <option value="<?php echo $obj->id;?>" <?php if($city==$obj->id) echo 'selected' ?> class="cityopt prov-<?=  $obj->provinsi_id; ?>">
                                                                <?php echo ucwords(strtolower($obj->name)); ?>
                                                            </option>
                                                            <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        Phone :
                                                        <input type="text" class="form-control text-center" name="phone" placeholder="Phone" value="<?= count($phone) > 0 ? $phone[0] : ''; ?>">
                                                        <input type="text" class="form-control text-center" name="phone_1" placeholder="Phone (Optional)" value="<?= count($phone) > 1 ? $phone[1] : ''; ?>">
                                                        <input type="text" class="form-control text-center" name="phone_2" placeholder="Phone (Optional)" value="<?= count($phone) > 2 ? $phone[2] : ''; ?>">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table><br>
                                        <table border="1" width="100%">
                                            <thead align="center">
                                                <th colspan="3">Next Of Kin</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Name next of kin :
                                                        <input type="text" class="form-control text-center" name="name_next_of_kin" placeholder="Name next of skin" value="<?php echo $name_next_of_kin?>">
                                                    </td>
                                                    <td>
                                                        Relationship next of kin :
                                                        <select name="relationship_next_of_kin" class="form-control">
                                                            <option value="">Select</option>
                                                            <option value="Ayah" <?php if($relationship_next_of_kin=='Ayah') echo 'selected' ?>>Ayah</option>
                                                            <option value="Ibu" <?php if($relationship_next_of_kin=='Ibu') echo 'selected' ?>>Ibu</option>
                                                            <option value="Mertua" <?php if($relationship_next_of_kin=='Mertua') echo 'selected' ?>>Mertua</option>
                                                            <option value="Anak" <?php if($relationship_next_of_kin=='Anak') echo 'selected' ?>>Anak</option>
                                                            <option value="Saudara Kandung" <?php if($relationship_next_of_kin=='Saudara Kandung') echo 'selected' ?>>Saudara Kandung</option>
                                                            <option value="Lainnya" <?php if($relationship_next_of_kin=='Lainnya') echo 'selected' ?>>Lainnya</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        Phone next of kin :
                                                        <input type="text" class="form-control text-center" name="phone_next_of_kin" placeholder="Phone next of kin" value="<?php echo $phone_next_of_kin?>">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table><br>
                                        <button type="button" class="btn" id="btn-tambah-form">Add Documents</button>
                                        <button type="button" class="btn" id="btn-reset-form">Reset Documents</button><br><br>
                                        <table border="1" width="100%">
                                            <thead align="center">
                                                <tr><th colspan="6">Document</th></tr>
                                                <tr>
                                                    <th>Document</th>
                                                    <th>Number</th>
                                                    <th>Place Of Issue</th>
                                                    <th>Date of Issue</th>
                                                    <th>Date of Expired</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $sql = $this->db->query("SELECT * FROM documents LEFT JOIN documents_user USING(id_documents_user) WHERE id_applications = '".$this->session->userdata('id')."'");
                                                    foreach($sql->result_array() as $sql2){
                                                ?>
                                                <tr id="document-<?php echo $sql2['id_documents']?>">
                                                    <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                        <?php echo $sql2['name_documents'];?>
                                                    </td>
                                                    <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                        <?php echo $sql2['number_documents'];?>
                                                    </td>
                                                    <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                        <?php echo $sql2['place_of_issue_documents'];?>
                                                    </td>
                                                    <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                        <?= validateDate($sql2['date_of_issue'], 'Y-m-d') ? $sql2['date_of_issue'] : '-' ; ?>
                                                    </td>
                                                    <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                        <?= validateDate($sql2['date_of_expire'], 'Y-m-d') ? $sql2['date_of_expire'] : '-' ; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if(!empty($sql2['photo_documents'])) { ?>
                                                            <a target="_blank" href="<?= site_url('upload/photo_documents/'.$sql2['photo_documents']) ?>" class="btn btn-sm btn-secondary" title="Delete">
                                                                <i class='fa fa-download'></i>
                                                            </a>
                                                        <?php } ?>
                                                        <a class="btn btn-sm btn-secondary hapus-doc" onclick="deleteDocuments(<?php echo $sql2['id_documents'];?>)" title="Delete"><i class='fa fa-trash'></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <div id="insert-form"></div>
                                        <input type="hidden" id="jumlah-form" value="1">
                                    </div>
                                </div><br>
                                <button type="button" class="btn" id="btn-tambah-form2">Add License</button>
                                <button type="button" class="btn" id="btn-reset-form2">Reset License</button><br><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table border="1" width="100%">
                                            <thead align="center">
                                                <tr><th colspan="7">License</th></tr>
                                                <tr>
                                                    <th>License</th>
                                                    <th>Grade of License</th>
                                                    <th>Number</th>
                                                    <th>Place of Issue</th>
                                                    <th>Date of Issue</th>
                                                    <th>Date of Expire</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $sql3 = $this->db->query("SELECT * FROM license LEFT JOIN license_user USING(id_license_user) WHERE id_applications = '".$this->session->userdata('id')."'");
                                                    foreach($sql3->result() as $sql4){
                                                ?>
                                                <tr id="license-<?php echo $sql4->id_license;?>">
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql4->name_license;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql4->grade_of_license;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql4->number_license;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql4->place_of_issue_license;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?= validateDate($sql4->date_of_issue_license, 'Y-m-d') ? $sql4->date_of_issue_license : '-' ; ?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?= validateDate($sql4->date_of_expire_license, 'Y-m-d') ? $sql4->date_of_expire_license : '-' ; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if(!empty($sql4->photo_license)) { ?>
                                                            <a target="_blank" href="<?= site_url('upload/photo_license/'.$sql4->photo_license) ?>" class="btn btn-sm btn-secondary" title="Delete">
                                                                <i class='fa fa-download'></i>
                                                            </a>
                                                        <?php } ?>
                                                        <a  class="btn btn-sm btn-secondary" onclick="deleteLicense(<?php echo $sql4->id_license;?>)" title="Delete"><i class='fa fa-trash'></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <div id="insert-form2"></div>
                                        <input type="hidden" id="jumlah-form2" value="1">
                                    </div>
                                </div><br>
                                <button type="button" class="btn" id="btn-tambah-form3">Add Certificate</button>
                                <button type="button" class="btn" id="btn-reset-form3">Reset Certificate</button><br><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table border="1" width="100%">
                                            <thead align="center">
                                                <tr><th colspan="6">Certificate</th></tr>
                                                <tr>
                                                    <th>Certificate Name</th>
                                                    <th>Number</th>
                                                    <th>Place Of Issue</th>
                                                    <th>Date of Issue</th>
                                                    <th>Expired Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $sql5 = $this->db->query("SELECT * FROM certification LEFT JOIN certificate USING(id_certificate) WHERE id_applications = '".$this->session->userdata('id')."'");
                                                    foreach($sql5->result() as $sql6){
                                                ?>
                                                <tr id="certificate-<?php echo $sql6->id_certification;?>">
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql6->name_certificate;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql6->number_certification;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql6->place_of_issue_certification;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?= validateDate($sql6->date_of_issue_certification, 'Y-m-d') ? $sql6->date_of_issue_certification : '-' ; ?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?= validateDate($sql6->expired_date_certification, 'Y-m-d') ? $sql6->expired_date_certification : '-' ; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if(!empty($sql6->photo_certification)) { ?>
                                                            <a target="_blank" href="<?= site_url('upload/photo_certification/'.$sql6->photo_certification) ?>" class="btn btn-sm btn-secondary" title="Delete">
                                                                <i class='fa fa-download'></i>
                                                            </a>
                                                        <?php } ?>
                                                        <a  class="btn btn-sm btn-secondary" onclick="deleteCertification(<?php echo $sql6->id_certification;?>)" title="Delete"><i class='fa fa-trash'></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <div id="insert-form3"></div>
                                        <input type="hidden" id="jumlah-form3" value="1">
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table border="1" width="100%">
                                            <thead>
                                                <th>Remark</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><textarea name="remark" rows="3" class="form-control"><?php echo $remark ?></textarea></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div><br>
                                <button type="button" class="btn" id="btn-tambah-form4">Add Experience</button>
                                <button type="button" class="btn" id="btn-reset-form4">Reset Experience</button><br><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table border="1" width="100%">
                                            <thead align="center">
                                                <tr><th colspan="9">Sea Experience</th></tr>
                                                <tr>
                                                    <th>Company / Manager</th>
                                                    <th>Vessel</th>
                                                    <th>Other Crew Nationality</th>
                                                    <th>Flag of Vessel</th>
                                                    <th>Type of Vessel</th>
                                                    <th>Rank</th>
                                                    <th>From (yyyy/mm/dd)</th>
                                                    <th>To (yyyy/mm/dd)</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $sql7 = $this->db->query("SELECT * FROM sea_experience LEFT JOIN type_of_vessel USING(id_tov) LEFT JOIN position USING(id_position) LEFT JOIN engine USING(id_engine) WHERE id_applications = '".$this->session->userdata('id')."'");
                                                    foreach($sql7->result() as $sql8){
                                                ?>
                                                <tr id="sea-experience-<?php echo $sql8->id_sea_experience;?>">
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql8->name_company_sea_experience;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql8->name_vessel_sea_experience;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql8->other_crew_nationality_sea_experience;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql8->flag_of_vessel_sea_experience;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql8->name_tov;?><br>
                                                        GT : <?php echo $sql8->gt_sea_experience;?> <br>
                                                        M/Engine : <?php echo $sql8->name_engine;?> <br>
                                                        BHP : <?php echo $sql8->bhp_sea_experience;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?php echo $sql8->name_position;?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?= validateDate($sql8->from_sea_experience, 'Y-m-d') ? $sql8->from_sea_experience : '-' ; ?>
                                                    </td>
                                                    <td style="padding:.85rem 1.15rem">
                                                        <?= validateDate($sql8->to_sea_experience, 'Y-m-d') ? $sql8->to_sea_experience : '-' ; ?>
                                                    </td>
                                                    <td align="center">
                                                        <a  class="btn btn-sm btn-secondary" onclick="deleteSeaExperience(<?php echo $sql8->id_sea_experience;?>)" title="Delete"><i class='fa fa-trash'></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <div id="insert-form4"></div>
                                        <input type="hidden" id="jumlah-form4" value="1">
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-11"></div>
                                    <div class="col-md-1">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>

<script>
function previewImage() {
    document.getElementById("image-preview").style.display = "block";
    var oFReader = new FileReader();
     oFReader.readAsDataURL(document.getElementById("image-source").files[0]);

    oFReader.onload = function(oFREvent) {
      document.getElementById("image-preview").src = oFREvent.target.result;
    };
};
</script>

<script>
$(document).ready(function(){

    $.ajaxSetup({
        type:"post",
        cache:false,
        dataType: "json"
    })

    $('.cityopt').hide();
    $('.prov-<?= $province; ?>').show();

    $("#btn-tambah-form").click(function(){ 
        var jumlah = parseInt($("#jumlah-form").val());
        var nextform = jumlah + 1;
        
        $("#insert-form").append(
            "<table border='1' width='100%'>" +
                "<tr>" +
                    "<td>" +
                        "<select name='name_documents[]' class='form-control'>" +
                            "<option value=''>Select</option>" +
                            "<?php
                                foreach($main['read_documents_user']->result() as $obj){
                            ?>"+
                            "<option value='<?php echo $obj->id_documents_user;?>'>" +
                                "<?php echo $obj->name_documents;?>" +
                            "</option>" +
                            "<?php
                                }
                            ?>" +
                        "</select>" +
                    "<br>Document</td>"+
                    "<td><input type='text' class='form-control' name='number_documents[]' placeholder='Number Documents'><br> Number</td>" +
                    "<td><input type='text' class='form-control' name='place_of_issue_documents[]' placeholder='Place Of Issue Documents'><br> Place Of Issue</td>" +
                    "<td><input type='date' class='form-control' name='date_of_issue[]' placeholder='Name Documents'><br> Date of issue</td>" +
                    "<td><input type='date' class='form-control' name='date_of_expire[]' placeholder='Name Documents'><br> Date of expire</td>" +
                    "<td><input type='file' class='form-control' name='photo_documents_"+jumlah+"'<br><br> Upload Documents</td>" +
                "</tr>" +
            "</table>");
        
        $("#jumlah-form").val(nextform);
    });
    
    $("#btn-reset-form").click(function(){
        $("#insert-form").html(""); 
        $("#jumlah-form").val("1");
    });

    // license
    $("#btn-tambah-form2").click(function(){
        var jumlah = parseInt($("#jumlah-form2").val());
        var nextform = jumlah + 1;
        
        $("#insert-form2").append(
            "<table border='1' width='100%'>" +
                "<tr>" +
                "<tr>" +
                    "<td>" +
                        "<select name='name_license[]' class='form-control'>" +
                            "<option value=''>Select</option>" +
                            "<?php
                                foreach($main['read_license_user']->result() as $obj){
                            ?>"+
                            "<option value='<?php echo $obj->id_license_user;?>'>" +
                                "<?php echo $obj->name_license;?>" +
                            "</option>" +
                            "<?php
                                }
                            ?>" +
                        "</select>" +
                    "</td>"+
                    "<td>"+
                        "<select name='grade_of_license[]' class='form-control'>" +
                            "<option value=''>Select</option>" +
                            "<option value='Management'>Management</option>" +
                            "<option value='Operasional'>Operasional</option>" +
                        "</select>" +
                    "</td>"+
                    "<td><input type='text' class='form-control' name='number_license[]' placeholder='Number'>" +
                    "<td><input type='text' class='form-control' name='place_of_issue_license[]' placeholder='Place Of Issue'>" +
                    "<td><input type='date' class='form-control' name='date_of_issue_license[]'>" +
                    "<td><input type='date' class='form-control' name='date_of_expire_license[]'>" +
                    "<td><input type='file' class='form-control' name='photo_license_"+jumlah+"'<br><br> Upload License</td>" +
                "</tr>" +
            "</table>");
        
        $("#jumlah-form2").val(nextform);
    });
    
    $("#btn-reset-form2").click(function(){
        $("#insert-form2").html("");
        $("#jumlah-form2").val("1");
    });

    // Certification
    $("#btn-tambah-form3").click(function(){
        var jumlah = parseInt($("#jumlah-form3").val());
        var nextform = jumlah + 1;
        
        $("#insert-form3").append(
            "<table border='1' width='100%'>" +
                "<tr>" +
                    "<td>" +
                        "<select name='id_certificate[]' class='form-control'>" +
                            "<option value=''>Select</option>" +
                            "<?php
                                foreach($main['read_sertifikat']->result() as $obj){
                            ?>"+
                            "<option value='<?php echo $obj->id_certificate;?>'>" +
                                "<?php echo $obj->name_certificate;?>" +
                            "</option>" +
                            "<?php
                                }
                            ?>" +
                        "</select>" +
                    "</td>"+
                    "<td><input type='number' class='form-control' name='number_certification[]' placeholder='Number Certification'>" +
                    "<td><input type='text' class='form-control' name='place_of_issue_certification[]' placeholder='Place Of Issue Certification'>" +
                    "<td><input type='date' class='form-control' name='date_of_issue_certification[]'>" +
                    "<td><input type='date' class='form-control' name='expired_date_certification[]'>" +
                    "<td><input type='file' class='form-control' name='photo_certification_"+jumlah+"'<br><br> Upload Certification</td>" +
                "</tr>" +
            "</table>");
        
        $("#jumlah-form3").val(nextform);
    });
    
    $("#btn-reset-form3").click(function(){
        $("#insert-form3").html("");
        $("#jumlah-form3").val("1");
    });

    // Sea Experience
    $("#btn-tambah-form4").click(function(){
        var jumlah = parseInt($("#jumlah-form4").val());
        var nextform = jumlah + 1;
        
        $("#insert-form4").append(
            "<table border='1' width='100%'>" +
                "<tr>" +
                    "<td><input type='text' class='form-control' name='name_company_se[]' placeholder='Company / Manager'>" +
                    "<td><input type='text' class='form-control' name='name_vessel_se[]' placeholder='Vessel'>" +
                    "<td>" +
                        "<select multiple name='nationality_se_"+jumlah+"[]' class='form-control'>" +
                            "<?php
                                foreach($main['read_negara']->result() as $obj){
                            ?>"+
                            "<option value='<?php echo $obj->name_negara;?>'>" +
                                "<?php echo $obj->name_negara;?>" +
                            "</option>" +
                            "<?php
                                }
                            ?>" +
                        "</select>" +
                        "<br><small><i>*Hold down the control (ctrl) button to select multiple options</i></small>" + 
                    "</td>"+
                    "<td>"+ 
                        "<select name='flag_of_vessel_se[]' class='form-control' style='width:100px;'>" +
                            "<option value=''>Select</option>" +
                            "<?php
                                foreach($main['read_negara']->result() as $obj){
                            ?>"+
                            "<option value='<?php echo $obj->name_negara;?>'>" +
                                "<?php echo $obj->name_negara;?>" +
                            "</option>" +
                            "<?php
                                }
                            ?>" +
                        "</select>" +
                    "<td>" +
                        "<select name='name_tov_se[]' class='form-control' style='width:100px;'>" +
                            "<option value=''>Type of Vessel</option>" +
                            "<?php
                                foreach($main['read_type_of_vessel']->result() as $obj){
                            ?>"+
                            "<option value='<?php echo $obj->id_tov;?>'>" +
                                "<?php echo $obj->name_tov;?>" +
                            "</option>" +
                            "<?php
                                }
                            ?>" +
                        "</select><br>" +
                        "<input type='text' class='form-control' name='gt_se[]' placeholder='GT'><br>"+
                        "<select name='engine_type_se[]' class='form-control' style='width:100px;'>" +
                            "<option value=''>M/Engine</option>" +
                            "<?php
                                foreach($main['read_engine']->result() as $obj){
                            ?>"+
                            "<option value='<?php echo $obj->id_engine;?>'>" +
                                "<?php echo $obj->name_engine;?>" +
                            "</option>" +
                            "<?php
                                }
                            ?>" +
                        "</select><br>" +
                        "<input type='text' class='form-control' name='bhp_se[]' placeholder='BHP'>"+
                    "</td>"+
                    "<td>" +
                        "<select name='rank_se[]' class='form-control' style='width:100px;'>" +
                            "<option value=''>Select</option>" +
                            "<?php
                                foreach($main['read_position']->result() as $obj){
                            ?>"+
                            "<option value='<?php echo $obj->id_position;?>'>" +
                                "<?php echo $obj->name_position;?>" +
                            "</option>" +
                            "<?php
                                }
                            ?>" +
                        "</select>" +
                    "</td>"+
                    "<td><input type='date' class='form-control' name='from_se[]'>" +
                    "<td><input type='date' class='form-control' name='to_se[]'>" +
                "</tr>" +
            "</table>");
        
        $("#jumlah-form4").val(nextform);
    });
    
    $("#btn-reset-form4").click(function(){
        $("#insert-form4").html("");
        $("#jumlah-form4").val("1");
    });

});

function deleteDocuments(id) {
    swal({
        title:"Hapus",
        text:"Yakin akan menghapus data ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Hapus",
    }).
    then((willDelete) => {
        if(willDelete.value) {
            $.get( "<?php echo site_url();?>data_cv/delete_documents/" + id, function( data )  {
                $('#document-' + id).remove();
                swal("Success", "Data berhasil dihapus", "success");
            })
            .fail(function() {
                swal("Failed", "Data tidak berhasil dihapus", "error");
            });
        }
    });
}

function deleteCertification(id) {
    swal({
        title:"Hapus",
        text:"Yakin akan menghapus data ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Hapus",
    }).
    then((willDelete) => {
        if(willDelete.value) {
            $.get( "<?php echo site_url();?>data_cv/delete_certification/" + id, function( data ) {
                $('#certificate-' + id).remove();
                swal("Success", "Data berhasil dihapus", "success");
            })
            .fail(function() {
                swal("Failed", "Data tidak berhasil dihapus", "error");
            });
        }
    });
}

function deleteLicense(id) {
    swal({
        title:"Hapus",
        text:"Yakin akan menghapus data ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Hapus",
    }).
    then((willDelete) => {
        if(willDelete.value) {
            $.get( "<?php echo site_url();?>data_cv/delete_license/" + id, function( data ) {
                $('#license-' + id).remove();
                swal("Success", "Data berhasil dihapus", "success");
            })
            .fail(function() {
                swal("Failed", "Data tidak berhasil dihapus", "error");
            });
        }
    });
}

function deleteSeaExperience(id) {
    swal({
        title:"Hapus",
        text:"Yakin akan menghapus data ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Hapus",
    }).
    then((willDelete) => {
        if(willDelete.value) {
            $.get( "<?php echo site_url();?>data_cv/delete_sea_experience/" + id, function( data ) {
                $('#sea-experience-' + id).remove();
                swal("Success", "Data berhasil dihapus", "success");
            })
            .fail(function() {
                swal("Failed", "Data tidak berhasil dihapus", "error");
            });
        }
    });
}

function changeProvince() {
    var provid = $('#provinceopt').val();
    $('.cityopt').hide();
    $('.prov-' + provid).show();
    $('#cityopt').val('');
}

</script>