<?php
  $id= "";
  $nama_principal = "";
  $penanggungjawab_principal = "";
  $lokasi_principal = "";
  $no_telp_principal = "";
  $negara_principal = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $obj) {
      $op = "edit";
      $id = $obj->id_company;
      $nama_principal = $obj->name_company;
      $penanggungjawab_principal = $obj->manager_company;
      $lokasi_principal = $obj->address_company;
      $no_telp_principal = $obj->phone_company;
      $negara_principal = $obj->nationality_company;
    }
  }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Create New Principal</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif_add');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <?php echo form_open_multipart('principal/create_principal/');?>
                    <input type="hidden" name="op" value="<?php echo $main['op'];?>">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Nama Principal</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="nama_principal" value="<?php echo $nama_principal?>" type="text" id="example-text-input" placeholder="Nama Principal" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Penanggung Jawab</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="penanggungjawab_principal" value="<?php echo $penanggungjawab_principal?>" placeholder="Penanggung Jawab Principal" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Address</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="lokasi_principal" value="<?php echo $lokasi_principal?>" placeholder="Lokasi" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">No Telepon</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="number" name="no_telp_principal" value="<?php echo $no_telp_principal?>" placeholder="No Telepon" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Negara</label>
                                <div class="col-10">
                                    <select name="negara_principal" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php
                                            foreach($main['read_negara']->result() as $obj){
                                        ?>
                                        <option value="<?php echo $obj->name_negara;?>" <?php if($negara_principal==$obj->name_negara) echo 'selected' ?>>
                                            <?php echo $obj->name_negara?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                    <!-- <select name="negara_principal" class="form-control m-input">
                                        <option value="">Pilih</option>
                                        <option value="Indonesia" <?php if($negara_principal=='Indonesia')echo 'selected'?>>Indonesia</option>
                                        <option value="Afrika Selatan" <?php if($negara_principal=='Afrika Selatan')echo 'selected'?>>Afrika Selatan</option>
                                        <option value="Papua Nugini" <?php if($negara_principal=='Papua Nugini')echo 'selected'?>>Papua Nugini</option>
                                        <option value="Brazil" <?php if($negara_principal=='Brazil')echo 'selected'?>>Brazil</option>
                                        <option value="Portugal" <?php if($negara_principal=='Portugal')echo 'selected'?>>Portugal</option>
                                        <option value="Amerika Serikat" <?php if($negara_principal=='Amerika Serikat')echo 'selected'?>>Amerika Serikat</option>
                                        <option value="Australia" <?php if($negara_principal=='Australia')echo 'selected'?>>Australia</option>
                                        <option value="Belanda" <?php if($negara_principal=='Belanda')echo 'selected'?>>Belanda</option>
                                        <option value="Germany" <?php if($negara_principal=='Germany')echo 'selected'?>>Germany</option>
                                        <option value="Denmark" <?php if($negara_principal=='Denmark')echo 'selected'?>>Denmark</option>
                                    </select> -->
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2">
                                    </div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="<?php echo site_url('principal')?>" class="btn btn-secondary">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>