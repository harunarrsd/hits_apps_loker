<link rel="stylesheet" href="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Daftar Principal</h3>
                <h6>Keterangan tambahan dapat diletakan disini</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif_edit');?>
        <?php echo $this->session->flashdata('notif_delete');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table id="example1" class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Principal</th>
                            <th>Nama Penanggung Jawab</th>
                            <th>Alamat</th>
                            <th>Phone</th>
                            <th>Nationality</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $no = 0;
                        foreach ($main['sql']->result() as $obj) {
                            $no++;
                        ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $obj->name_company?></td>
                            <td><?php echo $obj->manager_company?></td>
                            <td><?php echo $obj->address_company?></td>
                            <td><?php echo $obj->phone_company?></td>
                            <td><?php echo $obj->nationality_company?></td>
                            <td>
                                <a class="btn btn-sm btn-info" href="<?php echo site_url();?>principal/edit_principal/<?php echo $obj->id_company;?>" title="Edit"><i class='fa fa-edit'></i></a> 
                                <a  class="btn btn-sm btn-danger" href="javascript:if(confirm('Are you sure?')){document.location='<?php echo site_url();?>principal/delete_principal/<?php echo $obj->id_company;?>';}" title="Delete"><i class='fa fa-trash'></i></a>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>
<script src="<?php echo base_url();?>assets/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>