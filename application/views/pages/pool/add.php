<?php
  $id= "";
  $nama_pool = "";
  $deskripsi_pool = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $obj) {
      $op = "edit";
      $id = $obj->id_pool;
      $nama_pool = $obj->name_pool;
      $deskripsi_pool = $obj->deskripsi_pool;
    }
  }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Create New Pool</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif_add');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <?php echo form_open_multipart('pool/create_pool/');?>
                    <input type="hidden" name="op" value="<?php echo $main['op'];?>">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Nama Pool</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" name="nama_pool" id="example-text-input" placeholder="Pool Name" value="<?php echo $nama_pool;?>" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Deskripsi Pool</label>
                                <div class="col-10">
                                    <textarea class="form-control m-input" name="deskripsi_pool" cols="30" rows="10" placeholder="Deskripsi Pool" required><?php echo $deskripsi_pool;?></textarea>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Vessel</label>
                                <div class="col-10 m-checkbox-list">
                                        <?php foreach($main['read_vessel']->result() as $row){ ?>
                                            <label class="m-checkbox">
                                                <input type="checkbox" id="vessel" name="vessel[]" value="<?php echo $row->id_vessel;?>"> <?php echo ucfirst($row->name_vessel) ?>
                                                <span></span>
                                            </label>
                                        <?php } ?>

                                    <!-- <?php
                                        $sql = $this->db->query("SELECT * FROM pool_vessel JOIN vessel USING(id_vessel)");
                                        foreach($sql->result() as $obj2){
                                    ?>
                                            <input type="checkbox" name="vessel[]" <?php if($id==$obj2->id_pool) echo 'checked' ?> value="<?php echo $obj2->id_vessel;?>"> <?php echo ucfirst($obj2->name_vessel) ;?>
                                    <?php
                                        }
                                    ?> -->
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2">
                                    </div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="<?php echo site_url('pool')?>" class="btn btn-secondary">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>