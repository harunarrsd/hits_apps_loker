<?php
  $id= "";
  $nama_vessel = "";
  $engine_vessel = "";
  $gt_vessel = "";
  $bhp_vessel = "";
  $flag_vessel = "";
  $id_tov = "";
  $dwt = "";
  $id_company = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $obj) {
      $op = "edit";
      $id = $obj->id_vessel;
      $nama_vessel = $obj->name_vessel;
      $engine_vessel = $obj->id_engine;
      $gt_vessel = $obj->gt_vessel;
      $bhp_vessel = $obj->bhp_vessel;
      $flag_vessel = $obj->flag_vessel;
      $id_tov = $obj->id_tov;
      $dwt = $obj->dwt_vessel;
      $id_company = $obj->id_company;
    }
  }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Create New Vessel</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif_add');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <?php echo form_open_multipart('vessel/create_vessel/');?>
                    <input type="hidden" name="op" value="<?php echo $main['op'];?>">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Nama Vessel</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="name_vessel" value="<?php echo $nama_vessel?>" type="text" id="example-text-input" placeholder="Nama Vessel" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">GT Vessel</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="gt_vessel" value="<?php echo $gt_vessel?>" type="text" id="example-text-input" placeholder="GT Vessel" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Engine Vessel</label>
                                <div class="col-10">
                                    <select name="engine_vessel" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php
                                            foreach($main['read_engine']->result() as $obj){
                                        ?>
                                        <option value="<?php echo $obj->id_engine;?>" <?php if($engine_vessel==$obj->id_engine) echo 'selected' ?>>
                                            <?php echo $obj->name_engine?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                    <!-- <input class="form-control m-input" name="engine_vessel" value="<?php echo $engine_vessel?>" type="text" id="example-text-input" placeholder="Engine Vessel" required> -->
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">BHP Vessel</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="bhp_vessel" value="<?php echo $bhp_vessel?>" type="text" id="example-text-input" placeholder="BHP Vessel" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Flag of Vessel</label>
                                <div class="col-10">
                                    <select name="flag_vessel" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php
                                            foreach($main['read_negara']->result() as $obj){
                                        ?>
                                        <option value="<?php echo $obj->name_negara;?>" <?php if($flag_vessel==$obj->name_negara) echo 'selected' ?>>
                                            <?php echo $obj->name_negara;?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Type of Vessel</label>
                                <div class="col-10">
                                    <select name="id_tov" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php
                                            foreach($main['read_type_of_vessel']->result() as $obj){
                                        ?>
                                        <option value="<?php echo $obj->id_tov;?>" <?php if($id_tov==$obj->id_tov) echo 'selected' ?>>
                                            <?php echo $obj->name_tov;?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                    <!-- <input class="form-control m-input" name="engine_vessel" value="<?php echo $engine_vessel?>" type="text" id="example-text-input" placeholder="Engine Vessel" required> -->
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Principal</label>
                                <div class="col-10">
                                    <select name="id_company" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php
                                            foreach($main['read_principal']->result() as $obj){
                                        ?>
                                        <option value="<?php echo $obj->id_company;?>" <?php if($id_company==$obj->id_company) echo 'selected' ?>>
                                            <?php echo $obj->name_company;?>
                                        </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                    <!-- <input class="form-control m-input" name="engine_vessel" value="<?php echo $engine_vessel?>" type="text" id="example-text-input" placeholder="Engine Vessel" required> -->
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">DWT</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="dwt_vessel" value="<?php echo $dwt?>" type="text" id="example-text-input" placeholder="DWT" required>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2">
                                    </div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="<?php echo site_url('vessel')?>" class="btn btn-secondary">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>