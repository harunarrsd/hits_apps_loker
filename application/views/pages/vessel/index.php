<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Daftar Vessel</h3>
                <h6>Keterangan tambahan dapat diletakan disini</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Vessel</th>
                            <th>GT Vessel</th>
                            <th>Engine Vessel</th>
                            <th>BHP Vessel</th>
                            <th>DWT Vessel</th>
                            <th>Flag of Vessel</th>
                            <th>Type of Vessel</th>
                            <th>Principal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $no = 0;
                        foreach($main['sql']->result() as $obj){
                            $no++;
                    ?>
                        <tr>
                            <td><?php echo $no?></td>
                            <td><?php echo $obj->name_vessel?></td>
                            <td><?php echo $obj->gt_vessel?></td>
                            <td><?php echo $obj->name_engine?></td>
                            <td><?php echo $obj->bhp_vessel?></td>
                            <td><?php echo $obj->dwt_vessel?></td>
                            <td><?php echo $obj->flag_vessel?></td>
                            <td><?php echo $obj->name_tov?></td>
                            <td><?php echo $obj->name_company?></td>
                            <td>
                                <a class="btn btn-sm btn-info" href="<?php echo site_url();?>vessel/edit_vessel/<?php echo $obj->id_vessel;?>" title="Edit"><i class='fa fa-edit'></i></a> 
                                <a  class="btn btn-sm btn-danger" href="javascript:if(confirm('Are you sure?')){document.location='<?php echo site_url();?>vessel/delete_vessel/<?php echo $obj->id_vessel;?>';}" title="Delete"><i class='fa fa-trash'></i></a>
                            </td>
                        </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>