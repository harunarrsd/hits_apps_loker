<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $main['title']; ?></title>
    <?php $this->load->view('layout/head') ?>
</head>
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <!-- begin:: Page -->
    <div class="home">
        <div class="logo">
            <img src="<?php echo base_url();?>assets/img/dummy.png" class="img-responsive">
        </div>
        <div class="text">
            <h3>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Hic aut magni mollitia aliquam ex deleniti, at sed dolore placeat sunt necessitatibus? Omnis fuga corporis dicta, sed amet corrupti culpa enim!</h3>
            <div style="margin-left: 3%;margin-right: 3%;">
                <?php echo $this->session->flashdata('notif');?>
            </div>
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_login">Login</a>
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_daftar">Daftar</a>
        </div>
    </div>
	<!-- end:: Page -->

    <div class="modal fade" id="modal_daftar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Signup</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php echo form_open_multipart('home/create_applications/');?>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-4">
                                <label>Email</label>
                            </div>
                            <div class="col-8">
                                <input type="email" name="email_daftar" class="form-control" placeholder="Email" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-4">
                                <label>Password</label>
                            </div>
                            <div class="col-8">
                                <input type="password" id="pw" name="password_daftar" class="form-control" placeholder="Password" required>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-4">
                                <label>Retype Password</label>
                            </div>
                            <div class="col-8">
                                <input type="password" id="re_pw" name="retype_password" class="form-control" placeholder="Retype Password" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="Submit" id="btnSubmit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php echo form_open('home/ceklogin')?>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <label>Email</label>
                        </div>
                        <div class="col-8">
                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-4">
                            <label>Password</label>
                        </div>
                        <div class="col-8">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="Submit" name="login" class="btn btn-primary">Login</button>
                </div>
                <?php echo form_close()?>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="test" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
        </div>
    </div>

    <!-- javascript -->
    <?php $this->load->view('layout/javascript')?>
    <!-- END javascript -->
</body>
</html>