<style>
#image-preview{
    width : 100%;
    height : auto;
}
#image-preview2{
    width : 100%;
    height : auto;
}
#image-preview3{
    width : 100%;
    height : auto;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Daftar All Crew</h3>
                <h6>Keterangan tambahan dapat diletakan disini</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <!--end::Section-->

                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                        <thead>
                            <tr>
                            <th>No.</th>
                                <th>Nama Lengkap</th>
                                <th>Tempat, Tanggal Lahir</th>
                                <th>Alamat</th>
                                <th>Kota</th>
                                <th>Email</th>
                                <th>Nationality</th>
                                <th>No. Handphone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $no = 0;
                        foreach ($main['sql0']->result() as $obj) {
                            $id = $obj->id_user;
                            $no++;
                        ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td>
                                    <?php echo $obj->first_name?> <?php echo $obj->middle_name?> <?php echo $obj->last_name_surname?>
                                </td>
                                <td><?php echo $obj->place_of_birth?>, <?php echo $obj->date_of_birth?></td>
                                <td><?php echo $obj->address?></td>
                                <td><?php echo $obj->city_name?></td>
                                <td><?php echo $obj->email?></td>
                                <td><?php echo $obj->nationality?></td>
                                <td><?php echo $obj->phone?></td>
                                <td>
                                    <a href="<?php echo site_url();?>crew/data_cv_applications/<?php echo $id;?>" class="btn btn-sm btn-info text-white" title="View">
                                        <i class='fa fa-eye'></i>
                                    </a>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>