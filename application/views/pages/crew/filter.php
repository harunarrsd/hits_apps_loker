<style>
#image-preview{
    width : 100%;
    height : auto;
}
#image-preview2{
    width : 100%;
    height : auto;
}
#image-preview3{
    width : 100%;
    height : auto;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Daftar Crew</h3>
                <h6>Keterangan tambahan dapat diletakan disini</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">

                    <!-- begin::Item-->
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
                            <span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span>
                            <span class="m-accordion__item-title">Filter Data</span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">
                                <?php echo form_open_multipart('crew/filter/');?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_pool">
                                                <option value="#">Option List Pool</option>
                                                <?php foreach($main['sql4']->result() as $sql4){
                                                ?>
                                                    <option value="<?php echo $sql4->id_pool;?>" <?php if($main['filter_pool']==$sql4->id_pool) echo 'selected'?>>
                                                        <?php echo $sql4->name_pool;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_company">
                                                <option value="#">Option List Principal</option>
                                                <?php foreach($main['sql2']->result() as $sql2){
                                                ?>
                                                    <option value="<?php echo $sql2->id_company;?>" <?php if($main['filter_company']==$sql2->id_company) echo 'selected'?>>
                                                        <?php echo $sql2->name_company;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_rank">
                                                <option value="">Option List Rank</option>
                                                <?php foreach($main['sql6']->result() as $sql6){
                                                ?>
                                                    <option value="<?php echo $sql6->id_position;?>" <?php if($main['filter_rank']==$sql6->id_position) echo 'selected'?>>
                                                        <?php echo $sql6->name_position;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_age">
                                                <option value="#">Option List Age</option>
                                                <!-- age -->
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_address">
                                                <option value="#">Option List Address</option>
                                                <?php foreach($main['sql7']->result() as $sql7){
                                                ?>
                                                    <option value="<?php echo $sql7->city;?>" <?php if($main['filter_address']==$sql7->city) echo 'selected'?>>
                                                        <?php echo $sql7->city;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_documents">
                                                <option value="#">Option List Documents</option>
                                                <?php foreach($main['sql8']->result() as $sql8){
                                                ?>
                                                    <option value="<?php echo $sql8->id_documents_user;?>" <?php if($main['filter_documents']==$sql8->id_documents_user) echo 'selected'?>>
                                                        <?php echo $sql8->name_documents;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_certificate">
                                                <option value="#">Option List Certificate</option>
                                                <?php foreach($main['sql9']->result() as $sql9){
                                                ?>
                                                    <option value="<?php echo $sql9->id_certificate;?>" <?php if($main['filter_certificate']==$sql9->id_certificate) echo 'selected'?>>
                                                        <?php echo $sql9->name_certificate;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_license">
                                                <option value="#">Option List License</option>
                                                <?php foreach($main['sql10']->result() as $sql10){
                                                ?>
                                                    <option value="<?php echo $sql10->id_license_user;?>" <?php if($main['filter_license']==$sql10->id_license_user) echo 'selected'?>>
                                                        <?php echo $sql10->name_license;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_crew_nationality">
                                                <option value="#">Option List Crew Nationality</option>
                                                <!-- Crew Nationality -->
                                            </select>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_vessel">
                                                <option value="#">Option List Vessel</option>
                                                <?php foreach($main['sql3']->result() as $sql3){
                                                ?>
                                                    <option value="<?php echo $sql3->id_vessel;?>" <?php if($main['filter_vessel']==$sql3->id_vessel) echo 'selected'?>>
                                                        <?php echo $sql3->name_vessel;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_tov">
                                                <option value="#">Option List Type of Vessel</option>
                                                <?php foreach($main['sql5']->result() as $sql5){
                                                ?>
                                                    <option value="<?php echo $sql5->id_tov;?>" <?php if($main['filter_tov']==$sql5->id_tov) echo 'selected'?>>
                                                        <?php echo $sql5->name_tov;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary">Cari Sekarang</button>
                                            <a href="<?php echo site_url('crew');?>" class="btn btn-primary">Semua Data Crew</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!--end::Item-->
                </div>

                <!--end::Section-->

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                        <th>No.</th>
                            <th>Nama Lengkap</th>
                            <th>Alamat</th>
                            <th>Rank</th>
                            <th>Type Of Vessel</th>
                            <th>Company</th>
                            <th>Vessel</th>
                            <th>Pool</th>
                            <th>Range of salary</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $no = 0;
                    foreach ($main['filter']->result() as $obj) {
                        $id = $obj->id_history;
                        $no++;
                    ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td>
                                <a href="<?php echo site_url();?>crew/data_cv/<?php echo $id;?>">
                                    <?php echo $obj->first_name?> <?php echo $obj->middle_name?> <?php echo $obj->last_name_surname?>
                                </a>
                            </td>
                            <td><?php echo $obj->address?></td>
                            <td><?php echo $obj->name_position?></td>
                            <td><?php echo $obj->name_tov?></td>
                            <td><?php echo $obj->name_company?></td>
                            <td><?php echo $obj->name_vessel?></td>
                            <td><?php echo $obj->name_pool?></td>
                            <td><?php echo $obj->range_salary_start?> - <?php echo $obj->range_salary_end?></td>
                            <td>
                                <a class="btn btn-sm btn-success text-white" data-toggle="modal" data-target="#m_modal_6<?php echo $id?>" title="Detail"><i class='fa fa-edit'></i></a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>

<?php 
    $no = 0;
foreach ($main['sql']->result() as $obj) {
    $no++;
    $id = $obj->id_history;
    $id_com = $obj->id_company;
    $id_ves = $obj->id_vessel;
    $id_po = $obj->id_pool;
    $work_from = $obj->work_from;
    $tanggal = date('Y-m-d', strtotime($work_from));
    $work_to = $obj->work_to;
    $tanggal2 = date('Y-m-d', strtotime($work_to));
?>
<div class="modal fade" id="m_modal_6<?php echo $id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Crew</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('crew/update_wait/');?>
                <input type="hidden" value="<?php echo $id?>" name="id">
                    <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="400">
                        <div class="row">
                            <div class="col-4">
                                <label>Nama</label>
                            </div>
                            <div class="col-8">
                                <p>: <?php echo $obj->first_name?> <?php echo $obj->middle_name?> <?php echo $obj->last_name_surname?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Tempat Tanggal Lahir</label>
                            </div>
                            <div class="col-8">
                                <p>: <?php echo $obj->place_of_birth?>, <?php echo $obj->date_of_birth?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Alamat</label>
                            </div>
                            <div class="col-8">
                                <p>: <?php echo $obj->address?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Postion/Rank</label>
                            </div>
                            <div class="col-8">
                                <p>: <?php echo $obj->name_position?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>No. Handphone</label>
                            </div>
                            <div class="col-8">
                                <p>: <?php echo $obj->phone?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Email</label>
                            </div>
                            <div class="col-8">
                                <p>: <?php echo $obj->email?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Nationality</label>
                            </div>
                            <div class="col-8">
                                <p>: <?php echo $obj->nationality?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Type of Vessel</label>
                            </div>
                            <div class="col-8">
                                <p>: <?php echo $obj->name_tov?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <label>Status</label>
                            </div>
                            <div class="col-8">
                                <select name="status" class="form-control">
                                    <option value="<?php echo $obj->status?>"><?php if($obj->status==4) echo 'Accepted'?></option>
                                    <option value="3">Waiting</option>
                                    <option value="5">Rejected</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-4">
                                <label>Company/Principal</label>
                            </div>
                            <div class="col-8">
                                <select name="id_company" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        foreach($main['sql2']->result() as $obj2){
                                    ?>
                                    <option value="<?php echo $obj2->id_company?>" <?php if($id_com==$obj2->id_company) echo 'selected'?>>
                                    <?php echo $obj2->name_company?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-4">
                                <label>Vessel</label>
                            </div>
                            <div class="col-8">
                                <select name="id_vessel" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        foreach($main['sql3']->result() as $obj2){
                                    ?>
                                    <option value="<?php echo $obj2->id_vessel?>" <?php if($id_ves==$obj2->id_vessel) echo 'selected'?>>
                                    <?php echo $obj2->name_vessel?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-4">
                                <label>Pool</label>
                            </div>
                            <div class="col-8">
                                <select name="id_pool" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        foreach($main['sql4']->result() as $obj2){
                                    ?>
                                    <option value="<?php echo $obj2->id_pool?>" <?php if($id_po==$obj2->id_pool) echo 'selected'?>>
                                    <?php echo $obj2->name_pool?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-4">
                                <label>Working Period From</label>
                            </div>
                            <div class="col-8">
                                <?php
                                    if($work_from==NULL){
                                ?>
                                    <input class="form-control m-input" type="date" name="work_from" id="example-text-input" value="<?php echo " ";?>" required>
                                <?php
                                    } else {
                                ?>
                                    <input class="form-control m-input" type="date" name="work_from" id="example-text-input" value="<?php echo $tanggal?>" required>
                                <?php
                                    }
                                ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-4">
                                <label>Working Period To</label>
                            </div>
                            <div class="col-8">
                                <?php
                                    if($work_to==NULL){
                                ?>
                                    <input class="form-control m-input" type="date" name="work_to" id="example-text-input" value="<?php echo " ";?>" required>
                                <?php
                                    } else {
                                ?>
                                    <input class="form-control m-input" type="date" name="work_to" id="example-text-input" value="<?php echo $tanggal2?>" required>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="Submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>