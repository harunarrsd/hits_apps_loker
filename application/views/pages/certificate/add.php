<?php
  $id= "";
  $nama_sertifikat = "";
  if ($main['op']=="edit") {
    foreach ($main['sql']->result() as $obj) {
      $op = "edit";
      $id = $obj->id_certificate;
      $nama_sertifikat = $obj->name_certificate;
    }
  }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Create New Certificate</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif_add');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <?php echo form_open_multipart('certificate/create_certificate/');?>
                    <input type="hidden" name="op" value="<?php echo $main['op'];?>">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Nama Sertifikat</label>
                                <div class="col-10">
                                    <input class="form-control m-input" name="nama_sertifikat" value="<?php echo $nama_sertifikat?>" type="text" id="example-text-input" placeholder="Nama Sertifikat" required>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2">
                                    </div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="<?php echo site_url('certificate');?>" class="btn btn-secondary">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>