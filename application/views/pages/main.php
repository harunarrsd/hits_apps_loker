<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $main['title']; ?></title>
    <?php $this->load->view('layout/head') ?>
</head>
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper">
								<div class="m-login__logo">
									<a href="#">
										<img src="<?php echo base_url();?>assets/app/media/img/logos/logo-2.png">
									</a>
								</div>
								<div class="m-login__signin">
									<div class="m-login__head">
										<h3 class="m-login__title">Sign Up</h3>
										<div class="m-login__desc">Enter your details to create your account:</div>
										<br>
										<?php echo $this->session->flashdata('notif');?>
									</div>
									<?php echo form_open_multipart('main/create_signup/');?>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Nama Lengkap" name="nama_lengkap" required>
										</div>
                                        <div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Tempat Lahir" name="tempat_lahir" required>
                                        </div>
                                        <div class="form-group m-form__group">
											<input class="form-control m-input" id="m_datepicker_1" data-date-format="yyyy/mm/dd" type="text" placeholder="Tanggal Lahir" name="tanggal_lahir" required>
										</div>
										<div class="form-group m-form__group">
											<textarea class="form-control m-input" rows=3 placeholder="Alamat" name="alamat" required></textarea>
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="number" placeholder="No. Kartu Tanda Penduduk" name="no_ktp" autocomplete="off" required>
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Seaferer Code" name="seaferer_code" autocomplete="off" required>
										</div>
										<div class="form-group m-form__group">
											<select class="form-control m-input" name="posisi" required>
												<option value="">Job Position</option>
												<option value="1ST ASST ENGINEER">1ST ASST ENGINEER</option>
												<option value="1ST OFFICER">1ST OFFICER</option>
												<option value="2ND ASST ENGINEER">2ND ASST ENGINEER</option>
												<option value="2ND OFFICER">2ND OFFICER</option>
												<option value="3RD ASST ENGINEER">3RD ASST ENGINEER</option>
												<option value="3RD OFFICER">3RD OFFICER</option>
												<option value="4TH ASST ENGINEER">4TH ASST ENGINEER</option>
												<option value="4TH OFFICER">4TH OFFICER</option>
												<option value="ABLE BODY SEAMAN">ABLE BODY SEAMAN</option>
												<option value="ADDL 1ST ASST ENGINEER">ADDL 1ST ASST ENGINEER</option>
												<option value="ADDL 1ST OFFICER">ADDL 1ST OFFICER</option>
												<option value="ADDL 2ND ASST ENGINEER">ADDL 2ND ASST ENGINEER</option>
												<option value="ADDL 2ND OFFICER">ADDL 2ND OFFICER</option>
												<option value="ADDL 3RD ASST ENGINEER">ADDL 3RD ASST ENGINEER</option>
												<option value="ADDL CHIEF ENGINEER">ADDL CHIEF ENGINEER</option>
												<option value="ADDL CHIEF OFFICER">ADDL CHIEF OFFICER</option>
												<option value="ADDL GAS ENGINEER">ADDL GAS ENGINEER</option>
												<option value="ADDL MASTER">ADDL MASTER</option>
												<option value="ASSISTANT ENGINEER">ASSISTANT ENGINEER</option>
												<option value="ASST. ENGINEER (TUIE)">ASST. ENGINEER (TUIE)</option>
												<option value="BOSUN">BOSUN</option>
												<option value="CARPENTER">CARPENTER</option>
												<option value="CHIEF COOK">CHIEF COOK</option>
												<option value="CHIEF ENGINEER">CHIEF ENGINEER</option>
												<option value="CHIEF OFFICER">CHIEF OFFICER</option>
												<option value="DECK BOY">DECK BOY</option>
												<option value="DECK CADET">DECK CADET</option>
												<option value="DECK CADET (LING)">DECK CADET (LING)</option>
												<option value="DECK CADET (OFF)">DECK CADET (OFF)</option>
												<option value="DECK CADET (TUID)">DECK CADET (TUID)</option>
												<option value="DECK GAS MAN">DECK GAS MAN</option>
												<option value="DECK TRAINEE">DECK TRAINEE</option>
												<option value="ELECTRICIAN">ELECTRICIAN</option>
												<option value="ENG. TRAINEE">ENG. TRAINEE</option>
												<option value="ENGINE BOY">ENGINE BOY</option>
												<option value="ENGINE CADET">ENGINE CADET</option>
												<option value="ENGINE CADET (LING)">ENGINE CADET (LING)</option>
												<option value="ENGINE CADET (OFF)">ENGINE CADET (OFF)</option>
												<option value="ENGINE GAS MAN">ENGINE GAS MAN</option>
												<option value="GAS ENGINEER">GAS ENGINEER</option>
												<option value="MASTER">MASTER</option>
												<option value="MESSMAN">MESSMAN</option>
												<option value="OILER">OILER</option>
												<option value="OILER#1">OILER#1</option>
												<option value="ORDINARY SEAMAN">ORDINARY SEAMAN</option>
												<option value="PUMP MAN">PUMP MAN</option>
												<option value="SECOND COOK">SECOND COOK</option>
												<option value="SENIOR 1ST ASST ENGINEER">SENIOR 1ST ASST ENGINEER</option>
												<option value="SENIOR 2ND ASST ENGINEER">SENIOR 2ND ASST ENGINEER</option>
												<option value="SENIOR 2ND OFFICER">SENIOR 2ND OFFICER</option>
												<option value="SENIOR CHIEF OFFICER">SENIOR CHIEF OFFICER</option>
												<option value="STEWARDESS C">STEWARDESS C</option>
												<option value="TRAINEE 1ST ASST.ENG">TRAINEE 1ST ASST.ENG</option>
												<option value="TRAINEE 1ST OFFICER">TRAINEE 1ST OFFICER</option>
												<option value="TRAINEE 2ND ASST.ENG">TRAINEE 2ND ASST.ENG</option>
												<option value="TRAINEE 2ND OFFICER">TRAINEE 2ND OFFICER</option>
												<option value="TRAINEE 3RD ASST.ENG">TRAINEE 3RD ASST.ENG</option>
												<option value="TRAINEE 3RD OFFICER">TRAINEE 3RD OFFICER</option>
												<option value="TRAINEE CHIEF ENGINEER">TRAINEE CHIEF ENGINEER</option>
												<option value="TRAINEE CHIEF OFFICER">TRAINEE CHIEF OFFICER</option>
												<option value="TRAINEE GAS ENGINEER">TRAINEE GAS ENGINEER</option>
												<option value="TRAINEE MASTER">TRAINEE MASTER</option>
												<option value="WIPER">WIPER</option>
											</select>
										</div>
                                        <div class="form-group m-form__group">
											<input class="form-control m-input m-login__form-input--last" type="number" placeholder="No. Handphone" name="no_hp" required>
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="email" placeholder="Email" name="email" required>
										</div>
										<div class="form-group m-form__group">
											<select class="form-control m-input" name="jenis_kelamin" required>
												<option value="#">Jenis Kelamin</option>
												<option value="Laki - Laki">Laki - Laki</option>
												<option value="Perempuan">Perempuan</option>
											</select>
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input m-login__form-input--last" type="number" placeholder="Usia" name="usia" required>
										</div>
										<div class="m-login__form-action">
											<button class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air" type="submit">Sign Up</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center" style="background-image: url(<?php echo base_url();?>assets/app/media/img//bg/bg-4.jpg)">
					<div class="m-grid__item">
						<h3 class="m-login__welcome">Daftar Lowongan Kerja</h3>
						<p class="m-login__msg">
							Lorem ipsum dolor sit amet, coectetuer adipiscing<br>elit sed diam nonummy et nibh euismod
						</p>
					</div>
				</div>
			</div>
		</div>

	<!-- end:: Page -->

    <!-- javascript -->
    <?php $this->load->view('layout/javascript')?>
    <!-- END javascript -->
</body>
</html>