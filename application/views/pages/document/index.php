<link rel="stylesheet" href="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Daftar Documents</h3>
                <h6>Keterangan tambahan dapat diletakan disini</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table id="example1" class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Documents</th>
                            <th>Type Documents</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $no = 0;
                        foreach($main['sql']->result() as $obj){
                            $no++;
                    ?>
                        <tr>
                            <td><?php echo $no?></td>
                            <td><?php echo $obj->name_documents;?></td>
                            <td><?php echo $obj->type_documents;?></td>
                            <td>
                                <a class="btn btn-sm btn-info" href="<?php echo site_url();?>document/edit_documents_user/<?php echo $obj->id_documents_user;?>" title="Edit"><i class='fa fa-edit'></i></a> 
                                <a  class="btn btn-sm btn-danger" href="javascript:if(confirm('Are you sure?')){document.location='<?php echo site_url();?>document/delete_documents_user/<?php echo $obj->id_documents_user;?>';}" title="Delete"><i class='fa fa-trash'></i></a>
                            </td>
                        </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>
<script src="<?php echo base_url();?>assets/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>