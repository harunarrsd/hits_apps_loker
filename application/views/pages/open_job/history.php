<link rel="stylesheet" href="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Type Of Vessel / History Open Job</h3>
                <h6>Keterangan tambahan dapat diletakan disini</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table id="example1" class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name Type Of Vessel</th>
                            <th>Information</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $no = 0;
                        $query = $this->db->query("SELECT * FROM open_job JOIN type_of_vessel using(id_tov)");
                        foreach($query->result() as $obj){
                            $no++;
                    ?>
                        <tr>
                            <td><?php echo $no?></td>
                            <td><?php echo $obj->name_tov?></td>
                            <td><?php echo $obj->information?></td>
                            <td><?php echo $obj->start_date?></td>
                            <td><?php echo $obj->end_date?></td>
                            <td>
                                <?php if($obj->status_oj==1) {?>
                                    <a class="btn btn-sm btn-success text-white" title="Open">Open</a>
                                <?php } else {?>
                                    <a class="btn btn-sm btn-danger text-white" title="Close">Close</a>
                                <?php } ?>
                            </td>>
                            <td>
                                <!-- <a class="btn btn-sm btn-success" href="<?php echo site_url();?>open_job/open_job/<?php echo $obj->id_tov;?>" title="Edit">Open Job</a> -->
                                <a class="btn btn-sm btn-info" href="<?php echo site_url();?>open_job/edit_open_job/<?php echo $obj->id_oj;?>" title="Edit"><i class='fa fa-edit'></i></a>
                                <!-- <a  class="btn btn-sm btn-danger" href="javascript:if(confirm('Are you sure?')){document.location='<?php echo site_url();?>open_job/delete_open_job/<?php echo $obj->id_tov;?>';}" title="Delete"><i class='fa fa-trash'></i></a> -->
                                <?php if($obj->status_oj==1) {?>
                                    <a class="btn btn-sm btn-danger" href="javascript:if(confirm('Are you sure?')){document.location='<?php echo site_url();?>open_job/update_open_job4/<?php echo $obj->id_oj;?>';}" title="Close">Close Job</a>
                                <?php } else {?>
                                    <a class="btn btn-sm btn-success" href="javascript:if(confirm('Are you sure?')){document.location='<?php echo site_url();?>open_job/update_open_job3/<?php echo $obj->id_oj;?>';}" title="Open">Open Job</a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>
<script src="<?php echo base_url();?>assets/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>