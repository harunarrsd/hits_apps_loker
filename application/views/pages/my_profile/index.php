<?php
    $sql = $this->db->query("SELECT * FROM applications where id_applications = '".$this->session->userdata('id')."'");
    foreach($sql->result() as $obj){
        $email = $obj->email;
        $first_name = $obj->first_name;
        $middle_name = $obj->middle_name;
        $last_name = $obj->last_name_surname;
        $photo = $obj->photo;
        $nationality = $obj->nationality;
        $date_of_birth = $obj->date_of_birth;
        $tanggal = date('Y-m-d', strtotime($date_of_birth));
        $place_of_birth = $obj->place_of_birth;
    }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">My Profile</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Your Profile
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <img src="<?php if($photo=='') { echo site_url('assets/img/dummy-pic.png')?> <?php } else { echo site_url('upload/photo_user/'.$photo);?> <?php } ?>" alt="" />
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name"><?php echo $last_name;?></span>
                                <a href="" class="m-card-profile__email m-link"><?php echo $email;?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            <?php echo form_open_multipart('my_profile/update_my_profile/');?>
                            <form class="m-form m-form--fit m-form--label-align-right">
                                <input type="hidden" value="<?php echo $this->session->userdata('id');?>" name="id">
                                <div class="m-portlet__body">
                                    <!-- <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">Personnel Data</h3>
                                        </div>
                                    </div> -->
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Email</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="<?php echo $email;?>" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">First Name</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="<?php echo $first_name;?>" name="first_name">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Middle Name</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="<?php echo $middle_name;?>" name="middle_name">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Last Name / Surname</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="<?php echo $last_name;?>" name="last_name">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Nationality</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="<?php echo $nationality?>" name="nationality">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Place of birth</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="<?php echo $place_of_birth;?>" name="place_of_birth">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Date of birth</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="date" value="<?php echo $tanggal;?>" name="date_of_birth">
                                            <!-- <span class="m-form__help">If you want your invoices addressed to a company. Leave blank to use your full name.</span> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2">
                                            </div>
                                            <div class="col-7"><br>
                                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">Save changes</button>&nbsp;&nbsp;
                                                <!-- <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>