<?php
    // $sql = $this->db->query("SELECT * FROM applications where id_applications = '".$this->session->userdata('id')."'");
    foreach($main['sql']->result() as $obj){
        $id = $obj->id_history;
        $idap = $obj->id_applicant;
        $first_name = $obj->first_name;
        $middle_name = $obj->middle_name;
        $last_name = $obj->last_name_surname;
        $photo = $obj->photo;
        $nationality = $obj->nationality;
        $date_of_birth = $obj->date_of_birth;
        $tanggal = date('Y-m-d', strtotime($date_of_birth));
        $place_of_birth = $obj->place_of_birth;
        $id_position = $obj->id_position;
        $accept_lower_rank = $obj->accept_lower_rank;
        $available_from = $obj->available_from;
        $address = $obj->address;
        $province = $obj->province;
        $city = $obj->city;
        $phone = explode(",", $obj->phone);
        $next_of_kin = $obj->name_next_of_kin;
        $relationship = $obj->relationship_next_of_kin;
        $phone_next_of_kin = $obj->phone_next_of_kin;
        $remark = $obj->remark;
        $status = $obj->status_applicant;
        $file_wawancara = $obj->file_wawancara;
        $type_of_vessel = $obj->name_tov;
        $salary_start = $obj->range_salary_start;
        $salary_end = $obj->range_salary_end;
    }
?>
<!-- BEGIN page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Data CV Pelamar</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
            <div class="m-loader--left">
                <!-- <a href="<?php echo site_url('data_cv/cetak');?>" target="_BLANK" class="btn btn-default btn-md"><i class="fa fa-print"></i> Print</a> -->
                
                <?php 
                    if($status==1){
                ?>
                    <a href="<?php echo site_url('applications');?>" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                <?php
                    } else if($status==2){
                ?>
                    <a href="<?php echo site_url('applications');?>" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                <?php
                    } else if($status==3){
                ?>
                    <a href="<?php echo site_url('applications/wait');?>" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                <?php
                    } else if($status==5){
                ?>
                    <a href="<?php echo site_url('applications/trash');?>" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                <?php
                    }
                ?>
                <a href="<?php echo site_url();?><?php echo 'crew/cetak/'.$idap;?>" target="_BLANK" class="btn btn-default btn-md"><i class="fa fa-print"></i> Print Internal</a>
                <a href="<?php echo site_url();?><?php echo 'crew/cetak_eksternal/'.$idap;?>" target="_BLANK" class="btn btn-default btn-md"><i class="fa fa-print"></i> Print Eksternal</a>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!-- begin::Body -->
    <div class="m-body" style="padding-left: 0px; padding-top:0px !important;">
        <script src="<?php echo base_url("assets/js/jquery.min.js"); ?>" type="text/javascript"></script>
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!--begin::Portlet-->
            <div class="m-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-portlet m-portlet--tab">
                            <!--begin::Form-->
                            <?php echo form_open_multipart('data_cv/update_data_cv/');?>
                            <form class="m-form m-form--fit m-form--label-align-right">
                                <div class="m-portlet__body">
                                    <center>
                                        <h3>APPLICATION FOR EMPLOYMENT</h3>
                                    </center><br>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table border="1" width="100%">
                                                    <thead align="center">
                                                        <tr><th colspan="3">Job Application</th></tr>
                                                        <tr>
                                                            <th>Type Of Vessel</th>
                                                            <th>Range Of Salary</th>
                                                            <th>Interview Result</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:.85rem 1.15rem"><?php echo $type_of_vessel; ?></td>
                                                            <td style="padding:.85rem 1.15rem"><?php echo $salary_start; ?> - <?php echo $salary_end; ?></td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php if (!empty($file_wawancara)) { ?>
                                                                <a href="<?php echo site_url();?>upload/file_wawancara/<?php echo $file_wawancara;?>">
                                                                    <?php echo $file_wawancara; ?>
                                                                </a>
                                                                <?php }?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <table border="1" width="100%">
                                                    <thead align="center">
                                                        <th colspan="3">Personnel Data</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>FIRST NAME
                                                                <input type="text" disabled class="form-control text-center" name="first_name" placeholder="First Name" value="<?php echo $first_name;?>">
                                                            </td>
                                                            <td>MIDDLE NAME
                                                                <input type="text" disabled class="form-control text-center" name="middle_name" placeholder="Middle Name" value="<?php echo $middle_name;?>">
                                                            </td>
                                                            <td>LAST NAME / SURNAME
                                                                <input type="text" disabled class="form-control text-center" name="last_name" placeholder="Last Name / Surname" value="<?php echo $last_name;?>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>NATIONALITY
                                                                <input type="text" disabled class="form-control text-center" name="nationality" placeholder="Nationality" value="<?php echo $nationality;?>">
                                                            </td>
                                                            <td>DATE OF BIRTH (dd/mm/yyyy)
                                                                <input type="date" disabled class="form-control text-center" name="date_of_birth" value="<?php if($date_of_birth=='') { ?><?php } else { echo $tanggal;?><?php } ?>">
                                                            </td>
                                                            <td>PLACE OF BIRTH
                                                                <input type="text" disabled class="form-control text-center" name="place_of_birth" placeholder="Place Of Birth" value="<?php echo $place_of_birth;?>">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table><br>
                                                <table border="1" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="3">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        Post Applied For :
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <select disabled name="id_position" class="form-control">
                                                                            <option value="">Select</option>
                                                                            <?php
                                                                                foreach($main['read_position']->result() as $obj){
                                                                            ?>
                                                                            <option value="<?php echo $obj->id_position;?>" <?php if($id_position==$obj->id_position) echo 'selected' ?>>
                                                                                <?php echo $obj->name_position?>
                                                                            </option>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        Willing to accept lower rank :
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <select disabled name="accept_lower_rank" class="form-control">
                                                                            <option value="">Select</option>
                                                                            <option value="1" <?php if($accept_lower_rank=='1') echo 'selected';?>>Yes</option>
                                                                            <option value="0" <?php if($accept_lower_rank=='0') echo 'selected';?>>No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        Available from :
                                                                    </div>
                                                                    <div class="col-md-7">
                                                                        <input type="text" disabled class="form-control" name="available_from" placeholder="Available From" value="<?php echo $available_from?>">
                                                                    </div>
                                                                </div> 
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-3">
                                                <img id="image-preview" src="<?php if($photo=='') { echo site_url('assets/img/dummy.png')?> <?php } else { echo site_url('upload/photo_user/'.$photo);?> <?php } ?>" alt="Image not found !" class="img-thumbnail">
                                                <!-- <input style="margin-top:5%;" id="image-source" onchange="previewImage();" type="file" name="photo"> -->
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table border="1" width="100%">
                                                    <thead align="center">
                                                        <th colspan="3">Contact Address</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top">
                                                                Address :
                                                                <textarea name="address" rows="4" disabled placeholder="Address" class="form-control"><?php echo $address?></textarea>
                                                            </td>
                                                            <td valign="top">
                                                                Province :
                                                                <select disabled name="city" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <?php
                                                                        foreach($main['read_provinsi']->result() as $obj){
                                                                    ?>
                                                                    <option value="<?php echo $obj->id;?>" <?php if($province==$obj->id) echo 'selected' ?>>
                                                                        <?php echo ucwords(strtolower($obj->name)); ?>
                                                                    </option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </td>
                                                            <td valign="top">
                                                                City :
                                                                <select disabled name="city" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <?php
                                                                        foreach($main['read_kabupaten_kota']->result() as $obj){
                                                                    ?>
                                                                    <option value="<?php echo $obj->id;?>" <?php if($city==$obj->id) echo 'selected' ?> class="cityopt prov-<?=  $obj->provinsi_id; ?>">
                                                                        <?php echo ucwords(strtolower($obj->name)); ?>
                                                                    </option>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                Phone :
                                                                <input type="text" disabled class="form-control text-center" name="phone" placeholder="Phone" value="<?= count($phone) > 0 ? $phone[0] : ''; ?>">
                                                                <input type="text" disabled class="form-control text-center" name="phone_1" placeholder="Phone (Optional)" value="<?= count($phone) > 1 ? $phone[1] : ''; ?>">
                                                                <input type="text" disabled class="form-control text-center" name="phone_2" placeholder="Phone (Optional)" value="<?= count($phone) > 2 ? $phone[2] : ''; ?>">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table><br>
                                                <table border="1" width="100%">
                                                    <thead align="center">
                                                        <th colspan="3">Next Of Kin</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Name next of kin :
                                                                <input type="text" disabled class="form-control text-center" name="name_next_of_kin" placeholder="Nama next of skin" value="<?php echo $next_of_kin?>">
                                                            </td>
                                                            <td>
                                                                Relationship next of kin :
                                                                <input type="text" disabled class="form-control text-center" name="relationship_next_of_kin" placeholder="Relationship next of kin" value="<?php echo $relationship?>">
                                                            </td>
                                                            <td>
                                                                Phone next of kin :
                                                                <input type="number" disabled class="form-control text-center" name="phone_next_of_kin" placeholder="Phone next of kin" value="<?php echo $phone_next_of_kin?>">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table><br>
                                                <table border="1" width="100%">
                                                    <thead align="center">
                                                        <tr><th colspan="6">Document</th></tr>
                                                        <tr>
                                                            <th>Document</th>
                                                            <th>Number</th>
                                                            <th>Place Of Issue</th>
                                                            <th>Date of Issue</th>
                                                            <th>Date of Expired</th>
                                                            <th>File</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $sql = $this->db->query('SELECT * FROM documents LEFT JOIN documents_user USING(id_documents_user) WHERE id_applications = "'.$idap.'"');
                                                            foreach($sql->result_array() as $sql2){
                                                        ?>
                                                        <tr id="document-<?php echo $sql2['id_documents']?>">
                                                            <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                                <?php echo $sql2['name_documents'];?>
                                                            </td>
                                                            <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                                <?php echo $sql2['number_documents'];?>
                                                            </td>
                                                            <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                                <?php echo $sql2['place_of_issue_documents'];?>
                                                            </td>
                                                            <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                                <?= validateDate($sql2['date_of_issue'], 'Y-m-d') ? $sql2['date_of_issue'] : '-' ; ?>
                                                            </td>
                                                            <td data-id="<?php echo $sql2['id_documents']?>" style="padding:.85rem 1.15rem">
                                                                <?= validateDate($sql2['date_of_expire'], 'Y-m-d') ? $sql2['date_of_expire'] : '-' ; ?>
                                                            </td>
                                                            <td align="center">
                                                                <?php if(!empty($sql2['photo_documents'])) { ?>
                                                                    <a target="_blank" href="<?= site_url('upload/photo_documents/'.$sql2['photo_documents']) ?>" class="btn btn-sm btn-secondary" title="Delete">
                                                                        <i class='fa fa-download'></i>
                                                                    </a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <div id="insert-form"></div>
                                                <input type="hidden" id="jumlah-form" value="1">
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table border="1" width="100%">
                                                    <thead align="center">
                                                        <tr><th colspan="7">License</th></tr>
                                                        <tr>
                                                            <th>License</th>
                                                            <th>Grade of License</th>
                                                            <th>Number</th>
                                                            <th>Place of Issue</th>
                                                            <th>Date of Issue</th>
                                                            <th>Date of Expire</th>
                                                            <th>File</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $sql3 = $this->db->query('SELECT * FROM license LEFT JOIN license_user USING(id_license_user) WHERE id_applications = "'.$idap.'"');
                                                            foreach($sql3->result() as $sql4){
                                                        ?>
                                                        <tr id="license-<?php echo $sql4->id_license;?>">
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql4->name_license;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql4->grade_of_license;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql4->number_license;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql4->place_of_issue_license;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?= validateDate($sql4->date_of_issue_license, 'Y-m-d') ? $sql4->date_of_issue_license : '-' ; ?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?= validateDate($sql4->date_of_expire_license, 'Y-m-d') ? $sql4->date_of_expire_license : '-' ; ?>
                                                            </td>
                                                            <td align="center">
                                                                <?php if(!empty($sql4->photo_license)) { ?>
                                                                    <a target="_blank" href="<?= site_url('upload/photo_license/'.$sql4->photo_license) ?>" class="btn btn-sm btn-secondary" title="Delete">
                                                                        <i class='fa fa-download'></i>
                                                                    </a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <div id="insert-form2"></div>
                                                <input type="hidden" id="jumlah-form2" value="1">
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table border="1" width="100%">
                                                    <thead align="center">
                                                        <tr><th colspan="6">Certificate</th></tr>
                                                        <tr>
                                                            <th>Certificate Name</th>
                                                            <th>Number</th>
                                                            <th>Place Of Issue</th>
                                                            <th>Date of Issue</th>
                                                            <th>Expired Date</th>
                                                            <th>File</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            $sql5 = $this->db->query('SELECT * FROM certification LEFT JOIN certificate USING(id_certificate) WHERE id_applications = "'.$idap.'"');
                                                            foreach($sql5->result() as $sql6){
                                                        ?>
                                                        <tr id="certificate-<?php echo $sql6->id_certification;?>">
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql6->name_certificate;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql6->number_certification;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql6->place_of_issue_certification;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?= validateDate($sql6->date_of_issue_certification, 'Y-m-d') ? $sql6->date_of_issue_certification : '-' ; ?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?= validateDate($sql6->expired_date_certification, 'Y-m-d') ? $sql6->expired_date_certification : '-' ; ?>
                                                            </td>
                                                            <td align="center">
                                                            <?php if(!empty($sql6->photo_certification)) { ?>
                                                                <a target="_blank" href="<?= site_url('upload/photo_certification/'.$sql6->photo_certification) ?>" class="btn btn-sm btn-secondary" title="Delete">
                                                                    <i class='fa fa-download'></i>
                                                                </a>
                                                            <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table border="1" width="100%">
                                                    <thead>
                                                        <th>Remark</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><textarea disabled name="remark" rows="3" class="form-control"><?php echo $remark ?></textarea></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table border="1" width="100%">
                                                    <thead align="center">
                                                        <tr><th colspan="8">Sea Experience</th></tr>
                                                        <tr>
                                                            <th>Company / Manager</th>
                                                            <th>Vessel</th>
                                                            <th>Other Crew Nationality</th>
                                                            <th>Flag of Vessel</th>
                                                            <th>Type of Vessel</th>
                                                            <th>Rank</th>
                                                            <th>From (yyyy/mm/dd)</th>
                                                            <th>To (yyyy/mm/dd)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                        <?php
                                                            $sql7 = $this->db->query('SELECT * FROM sea_experience LEFT JOIN type_of_vessel USING(id_tov) LEFT JOIN position USING(id_position) LEFT JOIN engine USING(id_engine) WHERE id_applications = "'.$idap.'"');
                                                            foreach($sql7->result() as $sql8){
                                                        ?>
                                                        <tr id="sea-experience-<?php echo $sql8->id_sea_experience;?>">
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql8->name_company_sea_experience;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql8->name_vessel_sea_experience;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql8->other_crew_nationality_sea_experience;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql8->flag_of_vessel_sea_experience;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql8->name_tov;?><br>
                                                                GT : <?php echo $sql8->gt_sea_experience;?> <br>
                                                                M/Engine : <?php echo $sql8->name_engine;?> <br>
                                                                BHP : <?php echo $sql8->bhp_sea_experience;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?php echo $sql8->name_position;?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?= validateDate($sql8->from_sea_experience, 'Y-m-d') ? $sql8->from_sea_experience : '-' ; ?>
                                                            </td>
                                                            <td style="padding:.85rem 1.15rem">
                                                                <?= validateDate($sql8->to_sea_experience, 'Y-m-d') ? $sql8->to_sea_experience : '-' ; ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><br>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- end:: Body -->

</div>
<!-- END page -->