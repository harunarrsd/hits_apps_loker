<link href="<?php echo base_url();?>assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Daftar Pelamar Tidak Diterima</h3>
                <h6>Keterangan tambahan dapat diletakan disini</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                <!--begin::Section-->
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">

                    <!-- begin::Item-->
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
                            <span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span>
                            <span class="m-accordion__item-title">Filter Data</span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">
                                <form action="<?php echo site_url();?>applications/trash" method="get">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_rank">
                                                <option value="">Option List Rank</option>
                                                <?php foreach($main['sql6']->result() as $sql6){
                                                ?>
                                                    <option value="<?php echo $sql6->id_position;?>" <?= $main['position'] == $sql6->id_position ? 'selected' : ''; ?>>
                                                        <?php echo $sql6->name_position;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control m-input" name="filter_tov">
                                                <option value="">Option List Type of Vessel</option>
                                                <?php foreach($main['sql5']->result() as $sql5){
                                                ?>
                                                    <option value="<?php echo $sql5->id_tov;?>" <?= $main['tov'] == $sql5->id_tov ? 'selected' : ''; ?>>
                                                        <?php echo $sql5->name_tov;?>
                                                    </option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary">Cari Sekarang</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!--end::Item-->
                </div>

                <!--end::Section-->

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Lengkap</th>
                            <th>Alamat</th>
                            <th>Email</th>
                            <th>Rank</th>
                            <th>Type Of Vessel</th>
                            <th>Range Of Salary</th>
                            <!-- <th>Status</th> -->
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $no = 0;
                        foreach ($main['sql']->result() as $obj) {
                            $no++;
                            $id = $obj->id_history;
                            $age = '-';
                            if(validateDate($obj->date_of_birth, 'Y-m-d')) {
                                $oDateNow = new DateTime();
                                $oDateBirth = DateTime::createFromFormat('Y-m-d', $obj->date_of_birth);
                                $oDateIntervall = $oDateNow->diff($oDateBirth);
                                $age = $oDateIntervall->y;
                            }
                        ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td>
                                <?php echo $obj->first_name?> <?php echo $obj->middle_name?> <?php echo $obj->last_name_surname?> (<?= $age; ?> th)
                            </td>
                            <td><?php echo $obj->address?></td>
                            <td><?php echo $obj->email?></td>
                            <td><?php echo $obj->name_position?></td>
                            <td><?php echo $obj->name_tov?></td>
                            <td><?php echo $obj->range_salary_start?> - <?php echo $obj->range_salary_end?></td>
                            <!-- <?php if($obj->status=='1') { ?>
                                <td class="alert alert-warning text-center">Pending</td>
                            <?php } else if($obj->status=='2') { ?>
                                <td class="alert alert-success text-center">Verified</td>
                            <?php } else if($obj->status=='3') { ?>
                                <td class="alert alert-danger text-center">Waiting</td>
                            <?php } else if($obj->status=='4') { ?>
                                <td class="alert alert-success text-center">Accepted</td>
                            <?php } else { ?>
                                <td class="alert alert-danger text-center">Rejected</td>
                            <?php } ?> -->
                            <td>
                                <a class="btn btn-sm btn-success text-white" data-toggle="modal" data-target="#m_modal_6<?php echo $id?>" title="Detail"><i class='fa fa-edit'></i></a>
                                <a href="<?php echo site_url();?>applications/data_cv/<?php echo $id;?>" class="btn btn-sm btn-info text-white" title="View">
                                    <i class='fa fa-eye'></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>

<?php 
    $no = 0;
foreach ($main['sql']->result() as $obj) {
    $no++;
    $id = $obj->id_history;
?>
<div class="modal fade" id="m_modal_6<?php echo $id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Applications</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <?php echo form_open_multipart('applications/update_trash/');?>
                <input type="hidden" value="<?php echo $id?>" name="id">
                <div class="row">
                    <div class="col-4">
                        <label>Nama</label>
                    </div>
                    <div class="col-8">
                        <p>: <?php echo $obj->first_name?> <?php echo $obj->middle_name?> <?php echo $obj->last_name_surname?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>Tempat Tanggal Lahir</label>
                    </div>
                    <div class="col-8">
                        <p>: <?php echo $obj->place_of_birth?>, <?php echo $obj->date_of_birth?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>Alamat</label>
                    </div>
                    <div class="col-8">
                        <p>: <?php echo $obj->address?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>Postion/Rank</label>
                    </div>
                    <div class="col-8">
                        <p>: <?php echo $obj->name_position?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>No. Handphone</label>
                    </div>
                    <div class="col-8">
                        <p>: <?php echo $obj->phone?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>Email</label>
                    </div>
                    <div class="col-8">
                        <p>: <?php echo $obj->email?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>Nationality</label>
                    </div>
                    <div class="col-8">
                        <p>: <?php echo $obj->nationality?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>Type of Vessel</label>
                    </div>
                    <div class="col-8">
                        <p>: <?php echo $obj->name_tov?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>Status</label>
                    </div>
                    <div class="col-8">
                        <select name="status" class="form-control">
                            <option value="2">Verified</option>
                            <option value="3">Waiting</option>
                            <option value="5" <?php if($obj->status_applicant==5) echo 'selected'?>>Rejected</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="Submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php
    }
?>