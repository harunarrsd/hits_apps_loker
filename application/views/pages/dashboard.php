<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">

        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin:: Widgets/Activity-->
                <div class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light  m-portlet--rounded-force">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text m--font-light">
                                    Visitors / Jan - Des
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-widget17">
                            <div class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
                                <div class="m-widget17__chart" style="height:320px;">
                                    <canvas id="m_chart_daily_sales"></canvas>
                                </div>
                            </div>
                            <div class="m-widget17__stats">
                                <div class="m-widget17__items m-widget17__items-col1">
                                    <div class="m-widget17__item">
                                        <span class="m-widget17__icon">
                                            <i class="flaticon-truck m--font-brand"></i>
                                        </span>
                                        <span class="m-widget17__subtitle">
                                            Crew
                                        </span>
                                        <span class="m-widget17__desc">
                                            <?php echo $main['sql1']?>
                                        </span>
                                    </div>
                                    <div class="m-widget17__item">
                                        <span class="m-widget17__icon">
                                            <i class="flaticon-paper-plane m--font-info"></i>
                                        </span>
                                        <span class="m-widget17__subtitle">
                                            Applications
                                        </span>
                                        <span class="m-widget17__desc">
                                            <?php echo $main['sql']?>
                                        </span>
                                    </div>
                                </div>
                                <div class="m-widget17__items m-widget17__items-col2">
                                    <div class="m-widget17__item">
                                        <span class="m-widget17__icon">
                                            <i class="flaticon-pie-chart m--font-success"></i>
                                        </span>
                                        <span class="m-widget17__subtitle">
                                            Principal
                                        </span>
                                        <span class="m-widget17__desc">
                                           <?php echo $main['sql2']?>
                                        </span>
                                    </div>
                                    <div class="m-widget17__item">
                                        <span class="m-widget17__icon">
                                            <i class="flaticon-time m--font-danger"></i>
                                        </span>
                                        <span class="m-widget17__subtitle">
                                            Vessel
                                        </span>
                                        <span class="m-widget17__desc">
                                            <?php echo $main['sql3']?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end:: Widgets/Activity-->
            </div>
        </div>

        <!--End::Section-->
    </div>
</div>