<?php
    $id = "";
    $name_tov = "";
    if ($main['op']=="edit") {
        foreach ($main['sql']->result() as $obj) {
          $op = "edit";
          $id = $obj->id_tov;
          $name_tov = $obj->name_tov;
        }
      }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">Type Of Vessel</h3>
                <h6>Sub Description Goes Here</h6>
            </div>
        </div>
        <?php echo $this->session->flashdata('notif');?>
    </div>
    <!-- END: Subheader -->

    <!--begin::Portlet-->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    <?php echo form_open_multipart('type_of_vessel/create_open_job/');?>
                    <input type="hidden" name="op" value="<?php echo $main['op'];?>">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <form class="m-form m-form--fit m-form--label-align-right">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">Name Type Of Vessel</label>
                                <div class="col-10">
                                    <!-- <textarea name="note_oj" class="form-control" cols="30" rows="10" placeholder="Note"></textarea> -->
                                    <input class="form-control m-input" type="text" name="name_tov" id="example-text-input" placeholder="Name Type Of Vessel" value="<?php echo $name_tov?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-2">
                                    </div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="<?php echo site_url('open_job');?>" class="btn btn-secondary">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Portlet-->
</div>