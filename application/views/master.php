<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $main['title']; ?></title>
    <?php $this->load->view('layout/head') ?>
</head>
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default m-brand--minimize m-aside-left--minimize">
    
    <!-- BEGIN page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">

        <!-- header -->
        <?php echo $main['header']; ?>
        <!-- END header -->

        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <!-- navbar -->
            <?php echo $main['sidebar']; ?>
            <!-- END navbar -->

            <!-- pages -->
            <?php echo $main['pages']; ?>
            <!-- END pages -->
        
        </div>
        <!-- end:: Body -->

        <!-- footer -->
        <?php $this->load->view('layout/footer')?>
        <!-- END footer -->

    </div>
    <!-- END page -->

    <!-- begin::Scroll Top -->
    <div id="m_scroll_top" class="m-scroll-top">
        <i class="la la-arrow-up"></i>
    </div>

    <!-- end::Scroll Top -->

    <!-- javascript -->
    <?php $this->load->view('layout/javascript')?>
    <!-- END javascript -->
</body>
</html>