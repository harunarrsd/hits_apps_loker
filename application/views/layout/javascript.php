<!--begin:: Global Mandatory Vendors -->
<script src="<?php echo base_url();?>assets/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/wnumb/wNumb.js" type="text/javascript"></script>

<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<script src="<?php echo base_url();?>assets/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jquery.repeater/src/repeater.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/nouislider/distribute/nouislider.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jstree/dist/jstree.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/raphael/raphael.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/morris.js/morris.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/chartist/dist/chartist.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Bundle -->
<script src="<?php echo base_url();?>assets/js/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors -->
<script src="<?php echo base_url();?>assets/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/datatables/datatables.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts -->
<script src="<?php echo base_url();?>assets/app/js/dashboard.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/datatables/extensions/buttons.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/login.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/dropzone.js" type="text/javascript"></script>

<!--end::Page Scripts -->

<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
            var password = $("#pw").val();
            var confirmPassword = $("#re_pw").val();
            if (password != confirmPassword) {
                alert("Passwords do not match.");
                return false;
            }
            return true;
        });
    });
</script>