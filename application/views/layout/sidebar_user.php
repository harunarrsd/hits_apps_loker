<?php
    $sql = $this->db->query("SELECT * FROM users where id_user = '".$this->session->userdata('id')."'");
    $userstatus = 0;
    foreach($sql->result() as $obj) {
        $userstatus = $obj->status_crew;
    }
?>
<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item  m-menu__item--submenu <?php if($this->uri->segment(1) == 'dashboard') { echo 'm-menu__item--active m-menu__item--open'; } ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="<?php echo site_url('dashboard');?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-home-1"></i>
                    <span class="m-menu__link-text">Home</span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php if($this->uri->segment(1) == 'data_cv') { echo 'm-menu__item--active m-menu__item--open'; } ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="<?php echo site_url('data_cv');?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-file-2"></i>
                    <span class="m-menu__link-text">Data CV</span>
                </a>
            </li>
            <?php if(!$userstatus) { ?>
            <li class="m-menu__item  m-menu__item--submenu <?php if($this->uri->segment(1) == 'applications') { echo 'm-menu__item--active m-menu__item--open'; } ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="<?php echo site_url('applications');?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-list"></i>
                    <span class="m-menu__link-text">Applications</span>
                </a>
            </li>
            <?php } ?>
            <li class="m-menu__item  m-menu__item--submenu <?php if($this->uri->segment(1) == 'history') { echo 'm-menu__item--active m-menu__item--open'; } ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="<?php echo site_url('history');?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-time-1"></i>
                    <span class="m-menu__link-text">History / Job</span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php if($this->uri->segment(1) == 'syarat') { echo 'm-menu__item--active m-menu__item--open'; } ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="<?php echo site_url('syarat');?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-interface-5"></i>
                    <span class="m-menu__link-text">Syarat</span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php if($this->uri->segment(1) == 'setting') { echo 'm-menu__item--active m-menu__item--open'; } ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="<?php echo site_url('setting');?>" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-settings"></i>
                    <span class="m-menu__link-text">Setting</span>
                </a>
            </li>
        </ul>
    </div>

    <!-- END: Aside Menu -->
</div>

<!-- END: Left Aside -->