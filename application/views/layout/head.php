<!-- begin::Head -->
<meta charset="utf-8" />
<meta name="description" content="Latest updates and statistic charts">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

<!--begin::Web font -->
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
    WebFont.load({
    google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
    active: function() {
        sessionStorage.fonts = true;
    }
    });
</script>
<script src="<?php echo base_url();?>assets/jquery/dist/jquery.js" type="text/javascript"></script>

<!--end::Web font -->

<!--begin:: Global Mandatory Vendors -->
<link href="<?php echo base_url();?>assets/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<link href="<?php echo base_url();?>assets/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/animate.css/animate.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/morris.js/morris.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/metronic/css/styles.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Styles -->
<link href="<?php echo base_url();?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

<!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

<!--end::Global Theme Styles -->

<!--begin::Page Vendors Styles -->
<link href="<?php echo base_url();?>assets/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

<!--end::Page Vendors Styles -->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/dummy.png" />
<!-- end::Head -->