<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_hits extends CI_Model {

    function create_applications($data) {
        $this->db->insert('users',$data);
    }

    function proseslogin($email,$pass) {
        $this->db->where('email',$email);
        $this->db->where('password',$pass);
        return $this->db->get('users')->row();
    }

    function get_user($id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id_user',$id);
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $sql = $this->db->get();

        return $sql;
    }

    function get_applicant($id) {
        $this->db->select('*');
        $this->db->from('history_applications');
        $this->db->join('type_of_vessel', 'type_of_vessel.id_tov = history_applications.id_tov', 'left');
        $this->db->join('users', 'users.id_user = history_applications.id_applicant', 'left');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->where('history_applications.id_history', $id);
        $sql = $this->db->get();

        return $sql;
    }

    function create_pool($data) {
        $this->db->insert('pool',$data);
    }

    function create_principal($data) {
        $this->db->insert('company',$data);
    }

    function create_vessel($data) {
        $this->db->insert('vessel',$data);
    }

    function create_position($data) {
        $this->db->insert('position',$data);
    }

    function create_certificate($data) {
        $this->db->insert('certificate',$data);
    }

    function create_documents($data) {
        return $this->db->insert_batch('documents',$data);
    }

    function create_license($data) {
        return $this->db->insert_batch('license',$data);
    }

    function create_certification($data) {
        return $this->db->insert_batch('certification',$data);
    }

    function create_sea_experience($data) {
        return $this->db->insert_batch('sea_experience',$data);
    }

    function create_history_applications($data) {
        return $this->db->insert('history_applications',$data);
    }

    function create_open_job($data) {
        return $this->db->insert('type_of_vessel',$data);
    }

    function create_open_job2($data) {
        return $this->db->insert('open_job',$data);
    }

    function create_documents_user($data) {
        return $this->db->insert('documents_user',$data);
    }

    function create_license_user($data) {
        return $this->db->insert('license_user',$data);
    }

    function create_replacement($data) {
        return $this->db->insert('replacement',$data);
    }

    function read_applications() {
        $sql = $this->db->query('SELECT * FROM users');
        return $sql;
    }

    function read_crew() {
        $this->db->select('users.*, position.name_position, kabupaten_kota.name as city_name');
        $this->db->from('users');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->join('kabupaten_kota', 'kabupaten_kota.id = users.city', 'left');
        $this->db->where('status_crew > 0 OR status_crew IS NOT NULL');
        $sql = $this->db->get();

        return $sql;
    }

    function read_on_board_crew($company = NULL, $vessel = NULL, $pool = NULL, $position = NULL) {
        $this->db->select('*');
        $this->db->from('replacement');
        $this->db->join('company', 'company.id_company = replacement.id_company', 'left');
        $this->db->join('vessel', 'vessel.id_vessel = replacement.id_vessel', 'left');
        $this->db->join('pool', 'pool.id_pool = replacement.id_pool', 'left');
        $this->db->join('users', 'users.id_user = replacement.id_crew', 'left');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->where('replacement.active', 1);
        if($company) {
            $this->db->where('replacement.id_company', $company);
        }
        if($vessel) {
            $this->db->where('replacement.id_vessel', $vessel);
        }
        if($pool) {
            $this->db->where('replacement.id_pool', $pool);
        }
        if($position) {
            $this->db->where('users.id_position', $position);
        }
        $sql = $this->db->get();

        return $sql;
    }

    function read_history_applications($position = NULL, $tov = NULL) {
        $this->db->select('*');
        $this->db->from('history_applications');
        $this->db->join('type_of_vessel', 'type_of_vessel.id_tov = history_applications.id_tov', 'left');
        $this->db->join('users', 'users.id_user = history_applications.id_applicant', 'left');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->where("(history_applications.status_applicant = 1 OR history_applications.status_applicant = 2)");
        if($position) {
            $this->db->where('users.id_position', $position);
        }
        if($tov) {
            $this->db->where('history_applications.id_tov', $tov);
        }
        $sql = $this->db->get();

        return $sql;
    }

    function read_history_applications2() {
        $this->db->select('*');
        $this->db->from('history_applications');
        $this->db->join('type_of_vessel', 'type_of_vessel.id_tov = history_applications.id_tov', 'left');
        $this->db->join('users', 'users.id_user = history_applications.id_applicant', 'left');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->where('history_applications.status_applicant', 4);
        $sql = $this->db->get();

        return $sql;
    }

    function read_applications_waiting($position = NULL, $tov = NULL) {
        $this->db->select('*');
        $this->db->from('history_applications');
        $this->db->join('type_of_vessel', 'type_of_vessel.id_tov = history_applications.id_tov', 'left');
        $this->db->join('users', 'users.id_user = history_applications.id_applicant', 'left');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->where('history_applications.status_applicant', 3);
        if($position) {
            $this->db->where('users.id_position', $position);
        }
        if($tov) {
            $this->db->where('history_applications.id_tov', $tov);
        }
        $sql = $this->db->get();

        return $sql;
    }

    function read_applications_trash($position = NULL, $tov = NULL) {
        $this->db->select('*');
        $this->db->from('history_applications');
        $this->db->join('type_of_vessel', 'type_of_vessel.id_tov = history_applications.id_tov', 'left');
        $this->db->join('users', 'users.id_user = history_applications.id_applicant', 'left');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->where('history_applications.status_applicant', 5);
        if($position) {
            $this->db->where('users.id_position', $position);
        }
        if($tov) {
            $this->db->where('history_applications.id_tov', $tov);
        }
        $sql = $this->db->get();

        return $sql;
    }

    function read_applications_approved() {
        $this->db->select('*');
        $this->db->from('history_applications');
        $this->db->join('type_of_vessel', 'type_of_vessel.id_tov = history_applications.id_tov', 'left');
        $this->db->join('users', 'users.id_user = history_applications.id_applicant', 'left');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->where('history_applications.status_applicant', 4);
        $sql = $this->db->get();

        return $sql;
    }

    function read_applications_approved2() {
        $this->db->select('*');
        $this->db->from('history_applications');
        $this->db->join('type_of_vessel', 'type_of_vessel.id_tov = history_applications.id_tov', 'left');
        $this->db->join('users', 'users.id_user = history_applications.id_applicant', 'left');
        $this->db->join('position', 'position.id_position = users.id_position', 'left');
        $this->db->where('history_applications.status_applicant', 4);
        $sql = $this->db->get();

        return $sql;
    }

    function read_pool() {
        $sql = $this->db->query('SELECT * FROM pool');
        return $sql;
    }

    function read_principal() {
        $sql = $this->db->query('SELECT * FROM company');
        return $sql;
    }

    function read_position() {
        $sql = $this->db->query('SELECT * FROM position');
        return $sql;
    }

    function read_sertifikat() {
        $sql = $this->db->query('SELECT * FROM certificate');
        return $sql;
    }

    function read_vessel() {
        $sql = $this->db->query('SELECT * FROM vessel JOIN engine USING(id_engine) JOIN type_of_vessel USING(id_tov) JOIN company USING(id_company)');
        return $sql;
    }

    function read_vessel2() {
        $sql = $this->db->query('SELECT * FROM vessel');
        return $sql;
    }

    function read_open_job() {
        $sql = $this->db->query('SELECT * FROM type_of_vessel');
        return $sql;
    }

    function read_kabupaten_kota() {
        $sql = $this->db->query('SELECT * FROM kabupaten_kota ORDER BY id ASC');
        return $sql;
    }

    function read_provinsi() {
        $sql = $this->db->query('SELECT * FROM provinsi ORDER BY id ASC');
        return $sql;
    }

    function read_documents_user() {
        $sql = $this->db->query('SELECT * FROM documents_user');
        return $sql;
    }

    function read_license_user() {
        $sql = $this->db->query('SELECT * FROM license_user');
        return $sql;
    }

    function read_type_of_vessel() {
        $sql = $this->db->query('SELECT * FROM type_of_vessel');
        return $sql;
    }

    function read_engine() {
        $sql = $this->db->query('SELECT * FROM engine');
        return $sql;
    }

    function read_negara() {
        $sql = $this->db->query('SELECT * FROM negara');
        return $sql;
    }

    function edit_pool($id) {
        $this->db->where("id_pool",$id);
        return $this->db->get('pool');
    }

    function edit_principal($id) {
        $this->db->where("id_company",$id);
        return $this->db->get('company');
    }

    function edit_vessel($id) {
        $this->db->where("id_vessel",$id);
        return $this->db->get('vessel');
    }

    function edit_position($id) {
        $this->db->where("id_position",$id);
        return $this->db->get('position');
    }

    function edit_certificate($id) {
        $this->db->where("id_certificate",$id);
        return $this->db->get('certificate');
    }

    function edit_applications($id) {
        $this->db->where("id_user",$id);
        return $this->db->get('users');
    }

    function edit_open_job($id) {
        $this->db->where("id_oj",$id);
        return $this->db->get('open_job');
    }

    function edit_license_user($id) {
        $this->db->where("id_license_user",$id);
        return $this->db->get('license_user');
    }

    function edit_documents_user($id) {
        $this->db->where("id_documents_user",$id);
        return $this->db->get('documents_user');
    }

    function update_pool($id,$data) {
        $this->db->where("id_pool",$id);
        $this->db->update('pool',$data);
    }

    function update_principal($id,$data) {
        $this->db->where("id_company",$id);
        $this->db->update('company',$data);
    }

    function update_vessel($id,$data) {
        $this->db->where("id_vessel",$id);
        $this->db->update('vessel',$data);
    }

    function update_position($id,$data) {
        $this->db->where("id_position",$id);
        $this->db->update('position',$data);
    }

    function update_certificate($id,$data) {
        $this->db->where("id_certificate",$id);
        $this->db->update('certificate',$data);
    }

    function update_applications($id,$data) {
        $this->db->where("id_user",$id);
        $this->db->update('users',$data);
    }

    function update_history_applications($id,$data) {
        $this->db->where("id_history",$id);
        $this->db->update('history_applications',$data);
    }

    function update_replacement($id,$data) {
        $this->db->where("id",$id);
        $this->db->update('replacement',$data);
    }

    function update_data_cv($id,$data) {
        $this->db->where("id_user",$id);
        $this->db->update('users',$data);
    }

    function update_open_job($id,$data) {
        $this->db->where("id_tov",$id);
        $this->db->update('type_of_vessel',$data);
    }

    function update_open_job2($id,$data) {
        $this->db->where("id_oj",$id);
        $this->db->update('open_job',$data);
    }

    function update_license_user($id,$data) {
        $this->db->where("id_license_user",$id);
        $this->db->update('license_user',$data);
    }

    function update_documents_user($id,$data) {
        $this->db->where("id_documents_user",$id);
        $this->db->update('documents_user',$data);
    }

    function delete_applications($id) {
        $this->db->where("id_user",$id);
        $this->db->delete('users');
    }

    function delete_pool($id) {
        $this->db->where("id_pool",$id);
        $this->db->delete('pool');
    }

    function delete_principal($id) {
        $this->db->where("id_company",$id);
        $this->db->delete('company');
    }

    function delete_vessel($id) {
        $this->db->where("id_vessel",$id);
        $this->db->delete('vessel');
    }

    function delete_position($id) {
        $this->db->where("id_position",$id);
        $this->db->delete('position');
    }

    function delete_certificate($id) {
        $this->db->where("id_certificate",$id);
        $this->db->delete('certificate');
    }

    function delete_documents($id) {
        $this->db->where("id_documents",$id);
        $this->db->delete('documents');
    }

    function delete_license($id) {
        $this->db->where("id_license",$id);
        $this->db->delete('license');
    }

    function delete_certification($id) {
        $this->db->where("id_certification",$id);
        $this->db->delete('certification');
    }

    function delete_sea_experience($id) {
        $this->db->where("id_sea_experience",$id);
        $this->db->delete('sea_experience');
    }

    function delete_open_job($id) {
        $this->db->where("id_tov",$id);
        $this->db->delete('type_of_vessel');
    }

    function delete_license_user($id) {
        $this->db->where("id_license_user",$id);
        $this->db->delete('license_user');
    }

    function delete_documents_user($id) {
        $this->db->where("id_documents_user",$id);
        $this->db->delete('documents_user');
    }
}
